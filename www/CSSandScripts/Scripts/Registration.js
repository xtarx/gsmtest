﻿var RegistrationModule = {

    CurrentUUID: '',
    CurrentSelectedEntityType: '-1',
    CurrentSelectedEntitySubType: '-1',
    CurrentSelectedEmirate: '-1',
    ISEmirateSGotten: false,
    ISGCCNationalityAvailable: false,
    GCCNationalityCount: 0,
    EmiratisCount: 0,
    EmiratisValidationsTrailas: 0,
    EmiratesDropDOwnHTML: '',
    RegistrationObj: null,
    EntityOwnerObj: null,
    EntityDirectorObj: null,
    ResponsiblePersonObj: null,
    OwnerDetailsCompleted: false,
    DirectorDetailsCompleted: false,
    PersonsCounter: 0,
    PersonsCountertoValidate: 0,
    PersonsArr: new Array(),

    LoadRegistration: function (isFromServices) {
        if (isFromServices == true) {
            LoadPageContent("Registration", 'RegistrationModule.HandleRegistrationDocReady()', true, true, true, false, 'ServicesList.LoadServicesPage()');
        }
        else {
            LoadPageContent("Registration", 'RegistrationModule.HandleRegistrationDocReady()', true, true);
        }
    },

    HandleRegistrationDocReady: function () {

        SetHeaderTitle("Register New Employer", "تسجيل صاحب عمل جديد");
        BindEvents('#RegisterNextBtn', 'click', 'RegistrationModule.HandleNextBtnClicked()');

    },

    HandleEntityTypeDropDownSelectChanged: function () {

        try {

            var val = $("#RegistrationEntityTypeDropDown").val();

            RegistrationModule.CurrentSelectedEntityType = val;
            RegistrationModule.CurrentSelectedEntitySubType = "-1";

            try {
                $("#RegistrationEntitySubTypeDropDown_Private").val("-1").trigger("change");
                $("#RegistrationEntitySubTypeDropDown_Local").val("-1").trigger("change");
                $("#RegistrationEntitySubTypeDropDown_Federal").val("-1").trigger("change");
            } catch (e) {

            }
            if (val == "-1") {
                $('.RegEntityDropDowns').hide();
                $('#RegistrationEntitySubTypeDropDown_Private').show();
                $("#RegistrationEntitySubTypeDropDown_Private").attr('disabled', 'disabled');
                $("#EmirateTR").hide();
            }
            else {
                $('.RegEntityDropDowns').hide();
                $('#RegistrationEntitySubTypeDropDown_' + val).show();
                $("#RegistrationEntitySubTypeDropDown_" + val).removeAttr('disabled');

                if (val == "Private" || val == "Local") {
                    if (ISEmirateSGotten) {
                        $("#EmirateDropDown").html(EmiratesHTML);
                        $("#EmirateTR").show();
                    }
                    else {
                        var URL = Globals.ServicesURI_Test + "view/lov/type/GPSSA_EMIRATE/language/#Lang#";
                        URL = URL.replace("#Lang#", Language);

                        CallWebServiceWithOauth(URL, 'RegistrationModule.HandleEmirateDropDownCallSuccess(response)', "RegistrationModule.HandleEmirateDropDownCallFailure()", "", "", true, true);
                    }
                }
                else {
                    $("#EmirateTR").hide();
                }
            }
        } catch (e) {
            //alert(e);
        }
    },

    HandleEntitySubTypeDropDownSelectChanged: function (id) {
        var val = $("#" + id).val();
        RegistrationModule.CurrentSelectedEntitySubType = val;
    },

    HandleEmirateDropDownCallFailure: function () {
        $("#RegistrationEntityTypeDropDown").val("-1").trigger('change');
    },

    HandleEmirateDropDownCallSuccess: function (response) {
        try {

            if (response != null) {
                var OptionTemp = "<option value='#Val#'>#text#</option>";
                var DefaultOption = "";
                var AllHTML = "";
                var RespObj = response;//JSON.parse(response);

                if (RespObj.LOVs != null && RespObj.LOVs.length > 0) {

                    if (Language == "en") {
                        AllHTML += "<option value='-1'>Select Emirate</option>";
                    }
                    else {
                        AllHTML += "<option value='-1'>اختر الإمارة</option>";
                    }

                    for (var i = 0; i < RespObj.LOVs.length; i++) {
                        OptionTemp = "<option value='#Val#'>#text#</option>";
                        AllHTML += OptionTemp.replace("#Val#", RespObj.LOVs[i]).replace("#text#", RespObj.LOVs[i]);;
                    }

                    RegistrationModule.ISEmirateSGotten = true;
                    RegistrationModule.EmiratesDropDOwnHTML = AllHTML;
                    ISEmirateSGotten = true;
                    EmiratesHTML = AllHTML;
                    $("#EmirateDropDown").html(AllHTML);
                }
                else {
                    $("#EmirateDropDown").attr('disabled', 'disabled');
                }
            }
            else {
                $("#EmirateDropDown").attr('disabled', 'disabled');
            }

            $("#EmirateTR").show();

        } catch (e) {

        }
    },

    HandleEmirateDropDownChanged: function () {
        RegistrationModule.CurrentSelectedEmirate = $("#EmirateDropDown").val();
    },

    HandleNextBtnClicked: function () {
        try {

            if (RegistrationModule.CurrentSelectedEntityType == "-1") {
                AlertFunction("Please select Entity Type", "من فضلك اختر نوع القطاع الحكومي", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if (RegistrationModule.CurrentSelectedEntitySubType == "-1") {
                AlertFunction("Please select Sub-Type", "من فضلك اختر نوع القطاع الحكومي الفرعى", "Error", "خطأ", "OK", "موافق");
                return;
            }

            if (RegistrationModule.CurrentSelectedEntityType == "Private" || RegistrationModule.CurrentSelectedEntityType == "Local") {
                if (RegistrationModule.CurrentSelectedEmirate == "-1") {
                    AlertFunction("Please select Emirate", "من فضلك اختر الإمارة", "Error", "خطأ", "OK", "موافق");
                    return;
                }
                else if (RegistrationModule.CurrentSelectedEmirate == "Abu Dhabi" || RegistrationModule.CurrentSelectedEmirate == "ابوظبي") {
                    LoadPageContent("RegistrationError", '', true, true, true, false, "LoadPageContent('Registration', 'RegistrationModule.HandleRegistrationDocReady()', true, true)");
                }
                else {
                    var GccCount = $.trim($("#RegistrationStep2_GCCCount").val());
                    RegistrationModule.GCCNationalityCount = GccCount;
                    RegistrationModule.EmiratisCount = 0;

                    if (GccCount > 0) {
                        LoadPageContent("RegistrationStep4", 'RegistrationModule.HandleRegistrationStep4DocReady()', true, true, true, false, 'RegistrationModule.LoadRegistration()');
                    }
                    else {
                        var EmiratisCount = $.trim($("#RegistrationStep2_EmiratisCount").val());
                        if (EmiratisCount == 0) {
                            LoadPageContent("RegistrationError", '', true, true, true, false, "LoadPageContent('Registration', 'RegistrationModule.HandleRegistrationDocReady()', true, true)");
                        }
                        else {
                            RegistrationModule.EmiratisCount = EmiratisCount;
                            RegistrationModule.GCCNationalityCount = 0;
                            LoadPageContent("RegistrationStep3", 'RegistrationModule.HandleRegistrationStep3DocReady()', true, true, true, false, 'LoadPageContent("Registration", "RegistrationModule.HandleRegistrationDocReady()", true, true)');
                        }
                    }
                    //                    RegistrationModule.LoadRegistrationStep2();
                }
            }
            else {
                //RegistrationModule.LoadRegistrationStep2();
                var GccCount = $.trim($("#RegistrationStep2_GCCCount").val());
                RegistrationModule.GCCNationalityCount = GccCount;
                RegistrationModule.EmiratisCount = 0;

                if (GccCount > 0) {
                    LoadPageContent("RegistrationStep4", 'RegistrationModule.HandleRegistrationStep4DocReady()', true, true, true, false, "LoadPageContent('Registration', 'RegistrationModule.HandleRegistrationDocReady()', true, true)");
                }
                else {
                    var EmiratisCount = $.trim($("#RegistrationStep2_EmiratisCount").val());
                    if (EmiratisCount == 0) {
                        LoadPageContent("RegistrationError", '', true, true, true, false, "LoadPageContent('Registration', 'RegistrationModule.HandleRegistrationDocReady()', true, true)");
                    }
                    else {
                        RegistrationModule.EmiratisCount = EmiratisCount;
                        RegistrationModule.GCCNationalityCount = 0;
                        LoadPageContent("RegistrationStep3", 'RegistrationModule.HandleRegistrationStep3DocReady()', true, true, true, false, "LoadPageContent('Registration', 'RegistrationModule.HandleRegistrationDocReady()', true, true)");
                    }
                }

            }
        } catch (e) {

        }
    },

    LoadRegistrationStep2: function () {
        LoadPageContent("RegistrationStep2", 'RegistrationModule.HandleRegistrationStep2DocRead()', true, true, true, false, "LoadPageContent('Registration', 'RegistrationModule.HandleRegistrationDocReady()', true, true)");
    },

    HandleRegistrationStep2DocRead: function () {
        BindEvents('#RegistrationStep2NextBtn', 'click', 'RegistrationModule.HandleRegistrationStep2NextBtnClicked()');
        BindEvents('#RegistrationStep2BackBtn', 'click', 'RegistrationModule.LoadRegistration()');
    },

    HandleGCCNationalityDropDownSelectChanged: function () {
        var val = $("#RegistrationStep2_GCCNationalityDropDown").val();

        if (val == "-1") {
            $("#GCCTR").hide();
            $("#RegistrationStep2_GCCTextTR").hide();
            $("#EmiratisCountTR").hide();
            $("#RegistrationStep2_EmiratisTextTR").hide();
            RegistrationModule.ISGCCNationalityAvailable = false;
        }
        else if (val == "Yes") {
            $("#EmiratisCountTR").hide();
            $("#RegistrationStep2_EmiratisTextTR").hide();
            $("#GCCTR").show();
            $("#RegistrationStep2_GCCTextTR").show();
            RegistrationModule.ISGCCNationalityAvailable = true;
        }
        else {
            $("#GCCTR").hide();
            $("#RegistrationStep2_GCCTextTR").hide();
            $("#EmiratisCountTR").show();
            $("#RegistrationStep2_EmiratisTextTR").show();
            RegistrationModule.ISGCCNationalityAvailable = false;
        }
    },

    HandleRegistrationStep2NextBtnClicked: function () {

        var val = $("#RegistrationStep2_GCCNationalityDropDown").val();

        if (val == "-1") {
            AlertFunction("Please select GCC Nationality Employee Available Option", "من فضلك اختر إتاحة موظفين من دول مجلس التعاون الخليجى", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if (val == "Yes") {
            var GccCount = $.trim($("#RegistrationStep2_GCCCount").val());
            if (GccCount == 0 || GccCount == "") {
                AlertFunction("Please enter GCC Nationality Employee Count", "من فضلك ادخل عدد الموظفين من دول مجلس التعاون الخليجى", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else {
                RegistrationModule.GCCNationalityCount = GccCount;
                RegistrationModule.EmiratisCount = 0;
                LoadPageContent("RegistrationStep4", 'RegistrationModule.HandleRegistrationStep4DocReady()', true, true, true, false, 'RegistrationModule.LoadRegistrationStep2()');
            }
        }
        else {
            var EmiratisCount = $.trim($("#RegistrationStep2_EmiratisCount").val());

            if (EmiratisCount == "") {
                AlertFunction("Please enter Emiratis Count", "من فضلك ادخل عدد الاماراتيين", "Error", "خطأ", "OK", "موافق");
                return;
            }

            else if (EmiratisCount == 0) {
                LoadPageContent("RegistrationError", '', true, true, true, false, 'RegistrationModule.LoadRegistrationStep2()');
            }
            else {
                RegistrationModule.EmiratisCount = EmiratisCount;
                RegistrationModule.GCCNationalityCount = 0;
                RegistrationModule.LoadRegistrationStep3Page();
            }
        }
    },

    LoadRegistrationStep3Page: function () {
        LoadPageContent("RegistrationStep3", 'RegistrationModule.HandleRegistrationStep3DocReady()', true, true, true, false, 'RegistrationModule.LoadRegistration()');
    },

    HandleRegistrationStep3DocReady: function () {
        BindEvents('#RegistrationStep3NextBtn', 'click', 'RegistrationModule.HandleRegistrationStep3NextBtnClicked()');
        BindEvents('#RegistrationStep3BackBtn', 'click', 'RegistrationModule.LoadRegistration()');
        RegistrationModule.EmiratisValidationsTrailas = RegistrationModule.EmiratisCount;
        $("#RegistrationStep3_EmiratesID").mask("784-XXXX-XXXXXXX-X");
    },

    HandleRegistrationStep3NextBtnClicked: function () {

        var EmiratesID = FormatChar($.trim($("#RegistrationStep3_EmiratesID").val()));
        if (EmiratesID == "0") {
            AlertFunction("Please enter Emirates ID", "من فضلك ادخل رقم الهوية", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else {
            if (RegistrationModule.EmiratisValidationsTrailas > 0) {

                var url = Globals.ServicesURI_Test + "validate/emiratesId/emiratesId/#emiratesId#/language/#Lang#";

                url = url.replace("#emiratesId#", EmiratesID).replace("#Lang#", Language);

                CallWebService(url, "RegistrationModule.HandleEmiratesIDValidationSuccess(response)", "", "", "", true, true);
            }
        }
    },

    HandleEmiratesIDValidationSuccess: function (response) {
        try {

            var RespObj = response;//JSON.parse(response);
            if (RespObj != null) {
                if (RespObj.Message != null) {

                    RegistrationModule.EmiratisValidationsTrailas = RegistrationModule.EmiratisValidationsTrailas - 1;

                    if (RespObj.Message.Code == 0) {
                        if (RespObj.IsValid == true) {
                            RegistrationModule.LoadRegistrationStep4Page();
                        }
                        else {
                            if (RegistrationModule.EmiratisValidationsTrailas == 0) {
                                CleanPage("RegistrationStep3");
                                LoadPageContent("RegistrationError", '', true, true, true, false, 'RegistrationModule.LoadRegistrationStep3Page()');
                            }
                            else {
                                AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
                            }
                        }
                    }
                    else {
                        AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
                    }
                }
                else {
                    AlertFunction("Error occurred", "حدث خطأ", "Error", "خطأ", "OK", "موافق");
                }
            }
            else {
                AlertFunction("Error occurred", "حدث خطأ", "Error", "خطأ", "OK", "موافق");
            }
        } catch (e) {

        }
    },

    LoadRegistrationStep4Page: function () {
        if (RegistrationModule.GCCNationalityCount > 0) {
            LoadPageContent("RegistrationStep4", 'RegistrationModule.HandleRegistrationStep4DocReady()', true, true, true, false, 'RegistrationModule.LoadRegistration()');
        }
        else {
            LoadPageContent("RegistrationStep4", 'RegistrationModule.HandleRegistrationStep4DocReady()', true, true, true, false, 'RegistrationModule.LoadRegistrationStep3Page()');
        }
    },

    HandleRegistrationStep4DocReady: function () {
        BindEvents('#RegistrationStep4_NextBtn', 'click', 'RegistrationModule.HandleRegistrationStep4_NextBtnClicked()');
        BindEvents('#RegistrationStep4_BackBtn', 'click', "RegistrationModule.LoadRegistration()");
        $('#RegistrationStep4_ArabicName').on('input', function (e) {
            isArabic(RegistrationStep4_ArabicName);
        });
    },

    HandleRegistrationStep4_NextBtnClicked: function () {

        try {
            if ($.trim($("#RegistrationStep4_EnglishName").val()) == "") {
                AlertFunction("Please enter Entity English Name", "من فضلك ادخل اسم المنشأة الانجليزى", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#RegistrationStep4_ArabicName").val()) == "") {
                AlertFunction("Please enter Entity Arabic Name", "من فضلك ادخل اسم المنشأة العربى", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($("#RegistrationStep4_EntityTypeDropDown").val() == "-1") {
                AlertFunction("Please select Entity Type", "من فضلك اختر نوع المنشأة", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#RegistrationStep4_LicenseType").val()) == "") {
                AlertFunction("Please enter Entity License Type", "من فضلك ادخل نوع الرخصة", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#RegistrationStep4_LicensingAuthority").val()) == "") {
                AlertFunction("Please enter Entity Licensing Authority", "من فضلك ادخل جهة الترخيص", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#RegistrationStep4_LicenseNo").val()) == "") {
                AlertFunction("Please enter Entity License Number", "من فضلك ادخل رقم الترخيص", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#RegistrationStep4_EntityID").val()) == "") {
                AlertFunction("please enter entity Entity ID", "من فضلك ادخل رقم الترخيص", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else {

                RegistrationModule.RegistrationObj = AddNewEmployerClass;
                var CurentDta = new Date();
                RegistrationModule.RegistrationObj.ExpirationDate = (CurentDta.getTime() / 1000) + 31536000;
                RegistrationModule.RegistrationObj.Header.UserName = Globals.UserName;
                RegistrationModule.RegistrationObj.Header.UserType = Globals.UserType;
                RegistrationModule.RegistrationObj.Header.ContactRowId = Globals.ContactRowId;
                RegistrationModule.RegistrationObj.Header.EmployerRowId = Globals.EmployerRowID;
                RegistrationModule.RegistrationObj.Header.FullName = Globals.FullName;
                RegistrationModule.RegistrationObj.EmployerType = RegistrationModule.GetTypesLookup(RegistrationModule.CurrentSelectedEntityType);
                RegistrationModule.RegistrationObj.EmployerSubType = RegistrationModule.GetTypesLookup(RegistrationModule.CurrentSelectedEntitySubType);
                RegistrationModule.RegistrationObj.EmployerName = $("#RegistrationStep4_EnglishName").val();
                RegistrationModule.RegistrationObj.EmployerArabicName = $("#RegistrationStep4_ArabicName").val();
                RegistrationModule.RegistrationObj.LicenseType = $("#RegistrationStep4_LicenseType").val();
                RegistrationModule.RegistrationObj.LicensingAuthority = $("#RegistrationStep4_LicensingAuthority").val();
                RegistrationModule.RegistrationObj.LicenseNo = $("#RegistrationStep4_LicenseNo").val();
                RegistrationModule.RegistrationObj.EntityId = $("#RegistrationStep4_EntityID").val();
                RegistrationModule.RegistrationObj.EntityType = $("#RegistrationStep4_EntityTypeDropDown").val();

                RegistrationModule.LoadRegistrationStep5Page();
            }
        } catch (e) {

        }
    },

    LoadRegistrationStep5Page: function () {
        LoadPageContent("RegistrationStep5", 'RegistrationModule.HandleRegistration5DocReady()', true, true, true, false, 'RegistrationModule.LoadRegistrationStep4Page()');
    },

    HandleRegistration5DocReady: function () {
        try {

            BindEvents('#RegistrationStep5_OwnerInfoTD', 'click', 'RegistrationModule.HandleRegistrationStep5_OwnerInfoTDClicked()');
            BindEvents('#RegistrationStep5_OwnerNameBtn', 'click', 'RegistrationModule.HandleRegistrationStep5_OwnerNameBtnClicked()');
            BindEvents('#RegistrationStep5_DirectorInfoTD', 'click', 'RegistrationModule.HandleRegistrationStep5_DirectorInfoTDClicked()');
            BindEvents('#RegistrationStep5_DirectorNameBtn', 'click', 'RegistrationModule.HandleRegistrationStep5_DirectorNameBtnClicked()');
            BindEvents('#RegistrationStep5NextBtn', 'click', 'RegistrationModule.HandleRegistrationStep5_NextBtnClicked()');
            BindEvents('#RegistrationStep5BackBtn', 'click', "RegistrationModule.LoadRegistrationStep4Page()");
            // Masking ...............

            // Masking Mobile   05X-XXXXXXX
            $('#RegistrationStep5_Phone').mask("+XXX-XX-XXXXXXX", { autoclear: false });
            $('#RegistrationStep5_Phone').val("+971");

            $('#RegistrationStep5_OwnerMobileNo').mask("+XXX-XX-XXXXXXX", { autoclear: false });
            $('#RegistrationStep5_OwnerMobileNo').val("+971");

            $('#RegistrationStep5_DirectorMobileNo').mask("+XXX-XX-XXXXXXX", { autoclear: false });
            $('#RegistrationStep5_DirectorMobileNo').val("+971");

            $('#RegistrationStep6_MobileNo').mask("+XXX-XX-XXXXXXX", { autoclear: false });
            $('#RegistrationStep6_MobileNo').val("+971");
            // Emirate ID  784-XXXX-XXXXXXX-X
            $("#RegistrationStep5_OwnerEmiratesID").mask("784-XXXX-XXXXXXX-X");
            $("#RegistrationStep6_EmiratesID").mask("784-XXXX-XXXXXXX-X");
            $("#RegistrationStep5_DirectorEmiratesID").mask("784-XXXX-XXXXXXX-X");
            //   Land Line and Fax    0X-XXXXXXX
            $("#RegistrationStep5_Fax").mask("+XXX-X-XXXXXXX", { autoclear: false }).val("+971");
            $("#RegistrationStep5_DirectorLandLine").mask("+XXX-XX-XXXXXXX", { autoclear: false }).val("+971");
            $("#RegistrationStep5_OwnerLandLine").mask("+XXX-XX-XXXXXXX", { autoclear: false }).val("+971");
            $("#RegistrationStep6_LandLine").mask("+XXX-XX-XXXXXXX", { autoclear: false }).val("+971");

            $('#Registrationstep5_Ownerfirstname').on('input', function (e) {
                isArabic(Registrationstep5_Ownerfirstname);
            });
            $('#RegistrationStep5_OwnerSecondName').on('input', function (e) {
                isArabic(RegistrationStep5_OwnerSecondName);
            });
            $('#RegistrationStep5_OwnerThirdName').on('input', function (e) {
                isArabic(RegistrationStep5_OwnerThirdName);
            });
            $('#RegistrationStep5_OwnerFourthName').on('input', function (e) {
                isArabic(RegistrationStep5_OwnerFourthName);
            });

            $('#Registrationstep5_Directorfirstname').on('input', function (e) {
                isArabic(Registrationstep5_Directorfirstname);
            });
            $('#RegistrationStep5_DirectorSecondName').on('input', function (e) {
                isArabic(RegistrationStep5_DirectorSecondName);
            });
            $('#RegistrationStep5_DirectorThirdName').on('input', function (e) {
                isArabic(RegistrationStep5_DirectorThirdName);
            });
            $('#RegistrationStep5_DirectorFourthName').on('input', function (e) {
                isArabic(RegistrationStep5_DirectorFourthName);
            });

            if (ISEmirateSGotten) {
                $("#RegistrationStep5_EmirateDropDown").html(EmiratesHTML);
            }
            else {
                var URL = Globals.ServicesURI_Test + "view/lov/type/GPSSA_EMIRATE/language/#Lang#";
                URL = URL.replace("#Lang#", Language);

                CallWebServiceWithOauth(URL, 'RegistrationModule.HandleEmirateCallSuccess(response)', "", "", "", true, true);
            }

        } catch (e) {

        }
    },

    HandleEmirateCallSuccess: function (response) {
        try {

            if (response != null) {
                var OptionTemp = "<option value='#Val#'>#text#</option>";
                var DefaultOption = "";
                var AllHTML = "";
                var RespObj = response;//JSON.parse(response);

                if (RespObj.LOVs != null && RespObj.LOVs.length > 0) {

                    if (Language == "en") {
                        AllHTML += "<option value='-1'>Select Emirate</option>";
                    }
                    else {
                        AllHTML += "<option value='-1'>اختر الإمارة</option>";
                    }

                    for (var i = 0; i < RespObj.LOVs.length; i++) {
                        OptionTemp = "<option value='#Val#'>#text#</option>";
                        AllHTML += OptionTemp.replace("#Val#", RespObj.LOVs[i]).replace("#text#", RespObj.LOVs[i]);;
                    }

                    ISEmirateSGotten = true;
                    EmiratesHTML = AllHTML;
                    $("#RegistrationStep5_EmirateDropDown").html(EmiratesHTML);
                }
            }
        } catch (e) {

        }
    },

    HandleRegistrationStep5_OwnerInfoTDClicked: function () {
        if ($("#RegistrationStep5_OwnerNameTR").is(":visible")) {
            $("#RegistrationStep5_OwnerNameTR").fadeOut('slow');
        }
        else {
            $("#RegistrationStep5_OwnerNameTR").fadeIn('slow');
        }
    },

    HandleRegistrationStep5_OwnerNameBtnClicked: function () {

        if ($.trim($("#Registrationstep5_Ownerfirstname").val()) == "") {
            AlertFunction("Please enter Entity Owner First Name", "من فضلك ادخل اسم صاحب المنشأة الأول", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_OwnerSecondName").val()) == "") {
            AlertFunction("Please enter Entity Owner Second Name", "من فضلك ادخل اسم صاحب المنشأة الثانى", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_OwnerThirdName").val()) == "") {
            AlertFunction("Please enter Entity Owner Third Name", "من فضلك ادخل اسم صاحب المنشأة الثالث", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_OwnerFourthName").val()) == "") {
            AlertFunction("Please enter Entity Owner Fourth Name", "من فضلك ادخل اسم صاحب المنشأة الرابع", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_OwnerBirthDate").val()) == "") {
            AlertFunction("Please enter Entity Owner Birth Date", "من فضلك ادخل تاريخ ميلاد صاحب المنشأة ", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_OwnerEmiratesID").val()) == "") {
            AlertFunction("Please enter Entity Owner Emirates ID", "من فضلك ادخل رقم هوية صاحب المنشأة ", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_OwnerMobileNo").val()) == "") {
            AlertFunction("Please enter Entity Owner Mobile No", "من فضلك ادخل رقم موبايل صاحب المنشأة ", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_OwnerLandLine").val()) == "") {
            AlertFunction("Please enter Entity Owner Land Line", "من فضلك ادخل رقم الهاتف الارضى صاحب المنشأة ", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_OwnerEmail").val()) == "") {
            AlertFunction("Please enter Entity Owner Email", "من فضلك ادخل البريد الإلكترونى لصاحب المنشأة ", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if (!ValidateEmail($("#RegistrationStep5_OwnerEmail").val())) {
            AlertFunction("Please enter valid E-mail", "من فضلك ادخل البريد الإلكترونى صالح", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if (!ValidateBirthDate($("#RegistrationStep5_OwnerBirthDate").val())) {
            AlertFunction("Please select valide BirthDate", "من فضلك اختر تاريخ صالح", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else {
            $("#RegistrationStep5_OwnerNameTR").fadeOut('slow');
            $("#RegistrationStep5_OwnerInfoTD").html($("#Registrationstep5_Ownerfirstname").val() + " " + $("#RegistrationStep5_OwnerSecondName").val() + " " + $("#RegistrationStep5_OwnerThirdName").val() + " " + $("#RegistrationStep5_OwnerFourthName").val());
            RegistrationModule.EntityOwnerObj = ContactClass;
            RegistrationModule.EntityOwnerObj.FirstName = $("#Registrationstep5_Ownerfirstname").val();
            RegistrationModule.EntityOwnerObj.SecondName = $("#RegistrationStep5_OwnerSecondName").val();
            RegistrationModule.EntityOwnerObj.ThirdName = $("#RegistrationStep5_OwnerThirdName").val();
            RegistrationModule.EntityOwnerObj.FourthName = $("#RegistrationStep5_OwnerFourthName").val();
            RegistrationModule.EntityOwnerObj.BirthDate = ConvertDateToUNIX($("#RegistrationStep5_OwnerBirthDate").val());
            RegistrationModule.EntityOwnerObj.EmiratesId = FormatChar($("#RegistrationStep5_OwnerEmiratesID").val());

            var clearRegistrationStep5_OwnerMobileNo = FormatChar($("#RegistrationStep5_OwnerMobileNo").val());
            RegistrationModule.EntityOwnerObj.Mobile = "+" + clearRegistrationStep5_OwnerMobileNo;
            //alert("RegistrationModule.EntityOwnerObj.Mobile >>" + RegistrationModule.EntityOwnerObj.Mobile);

            var ClearRegistrationStep5_OwnerLandLine = FormatChar($("#RegistrationStep5_OwnerLandLine").val());
            RegistrationModule.EntityOwnerObj.Landline = "+" + ClearRegistrationStep5_OwnerLandLine;
            //alert("RegistrationModule.EntityOwnerObj.Landline>>>" + RegistrationModule.EntityOwnerObj.Landline);

            RegistrationModule.EntityOwnerObj.Email = $("#RegistrationStep5_OwnerEmail").val();
            RegistrationModule.EntityOwnerObj.IsOwner = true;
            RegistrationModule.OwnerDetailsCompleted = true;
        }
    },

    HandleRegistrationStep5_DirectorInfoTDClicked: function () {
        if ($("#RegistrationStep5_DirectorNameTR").is(":visible")) {
            $("#RegistrationStep5_DirectorNameTR").fadeOut('slow');
        }
        else {
            $("#RegistrationStep5_DirectorNameTR").fadeIn('slow');
        }
    },

    HandleRegistrationStep5_DirectorNameBtnClicked: function () {

        if ($.trim($("#Registrationstep5_Directorfirstname").val()) == "") {
            AlertFunction("Please enter Entity Director First Name", "من فضلك ادخل اسم مدير المنشأة الأول", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_DirectorSecondName").val()) == "") {
            AlertFunction("Please enter Entity Director Second Name", "من فضلك ادخل اسم مدير المنشأة الثانى", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_DirectorThirdName").val()) == "") {
            AlertFunction("Please enter Entity Director Third Name", "من فضلك ادخل اسم مدير المنشأة الثالث", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_DirectorFourthName").val()) == "") {
            AlertFunction("Please enter Entity Director Fourth Name", "من فضلك ادخل اسم مدير المنشأة الرابع", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_DirectorBirthDate").val()) == "") {
            AlertFunction("Please enter Entity Director Birth Date", "من فضلك ادخل تاريخ ميلاد مدير المنشأة ", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_DirectorEmiratesID").val()) == "") {
            AlertFunction("Please enter Entity Director Emirates ID", "من فضلك ادخل رقم هوية مدير المنشأة ", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_DirectorMobileNo").val()) == "") {
            AlertFunction("Please enter Entity Director Mobile No", "من فضلك ادخل رقم موبايل مدير المنشأة ", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_DirectorLandLine").val()) == "") {
            AlertFunction("Please enter Entity Director Land Line", "من فضلك ادخل رقم الهاتف الارضى مدير المنشأة ", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_DirectorEmail").val()) == "") {
            AlertFunction("Please enter Entity Director Email", "من فضلك ادخل البريد الإلكترونى لمدير المنشأة ", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if (!ValidateEmail($("#RegistrationStep5_DirectorEmail").val())) {
            AlertFunction("Please enter valid E-mail", "من فضلك ادخل البريد الإلكترونى صالح", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if (!ValidateBirthDate($("#RegistrationStep5_DirectorBirthDate").val())) {
            AlertFunction("Please select valide BirthDate", "من فضلك اختر تاريخ صالح", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else {
            $("#RegistrationStep5_DirectorNameTR").fadeOut('slow');
            $("#RegistrationStep5_DirectorInfoTD").html($("#Registrationstep5_Directorfirstname").val() + " " + $("#RegistrationStep5_DirectorSecondName").val() + " " + $("#RegistrationStep5_DirectorThirdName").val() + " " + $("#RegistrationStep5_DirectorFourthName").val());
            RegistrationModule.EntityDirectorObj = ContactClass2;
            RegistrationModule.EntityDirectorObj.FirstName = $("#Registrationstep5_Directorfirstname").val();
            RegistrationModule.EntityDirectorObj.SecondName = $("#RegistrationStep5_DirectorSecondName").val();
            RegistrationModule.EntityDirectorObj.ThirdName = $("#RegistrationStep5_DirectorThirdName").val();
            RegistrationModule.EntityDirectorObj.FourthName = $("#RegistrationStep5_DirectorFourthName").val();
            RegistrationModule.EntityDirectorObj.BirthDate = ConvertDateToUNIX($("#RegistrationStep5_DirectorBirthDate").val());
            RegistrationModule.EntityDirectorObj.EmiratesId = FormatChar($("#RegistrationStep5_DirectorEmiratesID").val());

            var clearRegistrationStep5_DirectorMobileNo = FormatChar($("#RegistrationStep5_DirectorMobileNo").val());
            RegistrationModule.EntityDirectorObj.Mobile = "+" + clearRegistrationStep5_DirectorMobileNo;
            //alert("RegistrationModule.EntityDirectorObj.Mobile >>" + RegistrationModule.EntityDirectorObj.Mobile);

            var ClearRegistrationStep5_DirectorLandLine = FormatChar($("#RegistrationStep5_DirectorLandLine").val());
            RegistrationModule.EntityDirectorObj.Landline = ClearRegistrationStep5_DirectorLandLine;
            //alert("RegistrationModule.EntityDirectorObj.Landline>>" + RegistrationModule.EntityDirectorObj.Landline);

            RegistrationModule.EntityDirectorObj.Email = $("#RegistrationStep5_DirectorEmail").val();
            RegistrationModule.EntityDirectorObj.IsManager = true;
            RegistrationModule.DirectorDetailsCompleted = true;
        }
    },

    HandleRegistrationStep5_NextBtnClicked: function () {
        if (RegistrationModule.OwnerDetailsCompleted == false) {
            AlertFunction("Please enter Entity Owner Information", "من فضلك ادخل بيانات صاحب المنشأة", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if (RegistrationModule.DirectorDetailsCompleted == false) {
            AlertFunction("Please enter Entity Director Information", "من فضلك ادخل بيانات مدير المنشأة", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_DateOfEstablishment").val()) == "") {
            AlertFunction("Please enter Entity Date Of Establishment", "من فضلك ادخل تاريخ تأسيس المنشأة", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_EmirateDropDown").val()) == "-1") {
            AlertFunction("Please select Entity Emirate", "من فضلك اختر إمارة المنشأة", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_Phone").val()) == "") {
            AlertFunction("Please enter Entity Phone", "من فضلك ادخل رقم هاتف المنشأة", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_Fax").val()) == "") {
            AlertFunction("Please enter Entity Fax", "من فضلك ادخل فاكس المنشأة", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#RegistrationStep5_POBox").val()) == "") {
            AlertFunction("Please enter Entity P.O Box", "من فضلك ادخل صندوق بريد المنشأة", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else {
            RegistrationModule.RegistrationObj.DateOfEstablishment = ConvertDateToUNIX($("#RegistrationStep5_DateOfEstablishment").val());
            RegistrationModule.RegistrationObj.EmirateOfLicense = $("#RegistrationStep5_EmirateDropDown").val();

            var ClearRegistrationStep5_Phone = FormatChar($("#RegistrationStep5_Phone").val());
            RegistrationModule.RegistrationObj.Phone = "+" + ClearRegistrationStep5_Phone;
            //alert("RegistrationModule.RegistrationObj.Phone >>" + RegistrationModule.RegistrationObj.Phone);

            var ClearRegistrationStep5_Fax = FormatChar($("#RegistrationStep5_Fax").val());
            RegistrationModule.RegistrationObj.Fax = "+" + ClearRegistrationStep5_Fax;
            //alert("ClearRegistrationStep5_Fax >> " + RegistrationModule.RegistrationObj.Fax);

            RegistrationModule.RegistrationObj.POBox = $("#RegistrationStep5_POBox").val();
            RegistrationModule.RegistrationObj.Contacts = new Array();
            RegistrationModule.RegistrationObj.Contacts[0] = RegistrationModule.EntityOwnerObj;
            RegistrationModule.RegistrationObj.Contacts[1] = RegistrationModule.EntityDirectorObj;

            RegistrationModule.LoadRegistrationStep6Page();
        }
    },

    LoadRegistrationStep6Page: function () {
        LoadPageContent("RegistrationStep6", 'RegistrationModule.HandleRegistrationStep6DocReady()', true, true, true, false, 'RegistrationModule.LoadRegistrationStep5Page()');
    },

    HandleRegistrationStep6DocReady: function () {
        try {
            
            RegistrationModule.PersonsArr = new Array();
            //$("#RegistrationStep6_FieldsDiv").height(DeviceHeight / 2);
            //$("#RegistrationStep6_PersonsDiv").height(DeviceHeight / 5);
            BindEvents('#RegistrationStep6_AddBtn', 'click', 'RegistrationModule.HandleRegistrationStep6_AddBtnClicked()');
            BindEvents('#RegistrationStep6NextBtn', 'click', 'RegistrationModule.HandleRegistrationStep6NextBtnClicked()');
            BindEvents('#RegistrationStep6BackBtn', 'click', 'RegistrationModule.LoadRegistrationStep5Page()');
            $("#RegistrationStep6_EmiratesID").mask("784-XXXX-XXXXXXX-X");
            $('#RegistrationStep6_MobileNo').mask("+XXX-XX-XXXXXXX", { autoclear: false }).val("+971");
            $("#RegistrationStep6_LandLine").mask("+XXX-XX-XXXXXXX", { autoclear: false }).val("+971");

            $('#RegistrationStep6_firstname').on('input', function (e) {
                isArabic(RegistrationStep6_firstname);
            });
            $('#RegistrationStep6_SecondName').on('input', function (e) {
                isArabic(RegistrationStep6_SecondName);
            });
            $('#RegistrationStep6_ThirdName').on('input', function (e) {
                isArabic(RegistrationStep6_ThirdName);
            });
            $('#RegistrationStep6_FourthName').on('input', function (e) {
                isArabic(RegistrationStep6_FourthName);
            });

            try {
                var uploader = document.getElementsByName('RegistrationStep6_File');
                for (item in uploader) {
                    // Detect changes
                    uploader[item].onchange = function () {
                        alert("upload");
                        $("#RegistrationCheckMark").css("display", "block");
                    }
                }
            } catch (e) {

            }
        } catch (e) {

        }
    },

    HandleRegistrationStep6_AddBtnClicked: function () {
        try {

            if ($.trim($("#RegistrationStep6_firstname").val()) == "") {
                AlertFunction("Please enter First Name", "من فضلك ادخل الأسم الأول", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#RegistrationStep6_SecondName").val()) == "") {
                AlertFunction("Please enter Second Name", "من فضلك ادخل الأسم الثانى", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#RegistrationStep6_ThirdName").val()) == "") {
                AlertFunction("Please enter Third Name", "من فضلك ادخل الأسم الثالث", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#RegistrationStep6_FourthName").val()) == "") {
                AlertFunction("Please enter Fourth Name", "من فضلك ادخل الأسم الرابع", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#RegistrationStep6_BirthDate").val()) == "") {
                AlertFunction("Please enter Birth Date", "من فضلك ادخل تاريخ الميلاد", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#RegistrationStep6_EmiratesID").val()) == "") {
                AlertFunction("Please enter Emirates ID", "من فضلك ادخل رقم الإقامة", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#RegistrationStep6_MobileNo").val()) == "") {
                AlertFunction("Please enter Mobile No", "من فضلك ادخل رقم الموبايل", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#RegistrationStep6_LandLine").val()) == "") {
                AlertFunction("Please enter Land Line", "من فضلك ادخل رقم الخط الأرضى", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#RegistrationStep6_Email").val()) == "") {
                AlertFunction("Please enter Email", "من فضلك ادخل البريد الإلكترونى", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if (!ValidateEmail($("#RegistrationStep6_Email").val())) {
                AlertFunction("Please enter valid E-mail", "من فضلك ادخل البريد الإلكترونى صالح", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#RegistrationStep6_JobTitle").val()) == "") {
                AlertFunction("Please enter Job Title", "من فضلك ادخل وظيفة الأشخاص", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else {

                try {
                    if (!GetUploadedDocsSize("RegistrationStep6_File")) {
                        AlertFunction("Max Size for documents is 7 MB", "الحد الاقصى للملفات 7 م ب", "Error", "خطأ", "OK", "موافق");
                        return;
                    }
                } catch (e) {

                }
                var uuid = guid();
                RegistrationModule.CurrentUUID = uuid;
                UploadFiles("RegistrationStep6_File", uuid, "RegistrationModule.HandleUploadDocSuccess(name)");
                //RegistrationModule.HandleUploadDocSuccess('test');
            }
        } catch (e) {
            //alert(e);
        }
    },

    HandleUploadDocSuccess: function (name) {
        RegistrationModule.RegistrationObj.Header.AttachedFiles[RegistrationModule.PersonsCounter] = name;
        RegistrationModule.ResponsiblePersonObj = ContactClass3;
        RegistrationModule.ResponsiblePersonObj.FirstName = $("#RegistrationStep6_firstname").val();
        RegistrationModule.ResponsiblePersonObj.SecondName = $("#RegistrationStep6_SecondName").val();
        RegistrationModule.ResponsiblePersonObj.ThirdName = $("#RegistrationStep6_ThirdName").val();
        RegistrationModule.ResponsiblePersonObj.FourthName = $("#RegistrationStep6_FourthName").val();
        RegistrationModule.ResponsiblePersonObj.BirthDate = ConvertDateToUNIX($("#RegistrationStep6_BirthDate").val());
        RegistrationModule.ResponsiblePersonObj.EmiratesId = FormatChar($("#RegistrationStep6_EmiratesID").val());

        var clearRegistrationStep6_MobileNo = FormatChar($("#RegistrationStep6_MobileNo").val());
        RegistrationModule.ResponsiblePersonObj.Mobile = "+" + clearRegistrationStep6_MobileNo;
        //alert("RegistrationModule.ResponsiblePersonObj.Mobile >>" + RegistrationModule.ResponsiblePersonObj.Mobile);

        var ClearRegistrationStep6_LandLine = FormatChar($("#RegistrationStep6_LandLine").val());
        RegistrationModule.ResponsiblePersonObj.Landline = "+" + ClearRegistrationStep6_LandLine;
        //alert("RegistrationModule.ResponsiblePersonObj.Landline >>" + RegistrationModule.ResponsiblePersonObj.Landline);

        RegistrationModule.ResponsiblePersonObj.Email = $("#RegistrationStep6_Email").val();
        RegistrationModule.ResponsiblePersonObj.JobTitle = $("#RegistrationStep6_JobTitle").val();
        RegistrationModule.ResponsiblePersonObj.IsAuthorizedPerson = true;

        var Temp = $("#RegistrationStep6_PersonTemplate").html();
        var HTML = Temp.replace("#Name#", $("#RegistrationStep6_firstname").val() + " " + $("#RegistrationStep6_SecondName").val() + " " + $("#RegistrationStep6_ThirdName").val() + " " + $("#RegistrationStep6_FourthName").val()).replace("#ID#", RegistrationModule.PersonsCounter).replace("#ID#", RegistrationModule.PersonsCounter);
        $("#RegistrationStep6_PersonsDiv").append(HTML);

        RegistrationModule.PersonsArr[RegistrationModule.PersonsCounter] = RegistrationModule.ResponsiblePersonObj;
        RegistrationModule.PersonsCounter += 1;
        RegistrationModule.PersonsCountertoValidate += 1;

        $("#RegistrationStep6_firstname").val("");
        $("#RegistrationStep6_SecondName").val("");
        $("#RegistrationStep6_ThirdName").val("");
        $("#RegistrationStep6_FourthName").val("");
        $("#RegistrationStep6_BirthDate").val("");
        $("#RegistrationStep6_EmiratesID").val("");
        $("#RegistrationStep6_MobileNo").val("");
        $("#RegistrationStep6_MobileNo").val("+971");
        $("#RegistrationStep6_LandLine").val("");
        $("#RegistrationStep6_LandLine").val("+971");
        $("#RegistrationStep6_Email").val("");
        $("#RegistrationStep6_JobTitle").val("");
        $("#RegistrationStep6_File").val("");
    },

    DeletePerson: function (ID) {
        $("#RegistrationStep6_" + ID).remove();
        RegistrationModule.PersonsArr[ID] = null;
        RegistrationModule.PersonsCountertoValidate -= 1;
        RegistrationModule.RegistrationObj.Header.AttachedFiles[ID] = null;
    },

    HandleRegistrationStep6NextBtnClicked: function () {
        try {

            var counter = 0;
            if (RegistrationModule.PersonsCountertoValidate > 0) {
                for (var i = 0; i < RegistrationModule.PersonsArr.length; i++) {
                    if (RegistrationModule.PersonsArr[i] != null) {
                        counter += 1;
                        RegistrationModule.RegistrationObj.Contacts[counter + 2] = RegistrationModule.PersonsArr[i];
                    }
                }

                var url = Globals.ServicesURI_Test + "add/employer/language/#Lang#";
                url = url.replace("#Lang#", Language);
                var data = JSON.stringify(RegistrationModule.RegistrationObj);
                data = data.replace(",null,", ",");
                Log(data, "RegistrationModule.RegistrationObj");
                CallWebService(url, "RegistrationModule.HandleRegistrationCallSuccess(response)", "", data, "POST", true, true);
            }
            else {
                AlertFunction("you must enter at least one Authorized Person ", "يجب ادخال شخص مفوض واحد على الأقل", "Error", "خطأ", "OK", "موافق");
                RegistrationModule.PersonsCountertoValidate = 0;
                return;
            }
        } catch (e) {

        }
    },

    HandleRegistrationCallSuccess: function (response) {
        if (response != null) {
            if (response.Message.Code == 0) {
                AlertFunction("Case Number : " + response.CaseNumber, "رقم الحالة : " + response.CaseNumber, "Success", "نجاح", "OK", "موافق");
                RegistrationModule.CleanRegisterationModule();
                RegistrationModule.LoadRegistration();
            }
            else {
                AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        }
    },

    CleanRegisterationModule: function () {
        CleanPage('RegistrationStep6,RegistrationStep5,RegistrationStep4,RegistrationStep3,RegistrationStep2,Registration');
        RegistrationModule.CurrentUUID = "";
        RegistrationModule.CurrentSelectedEntityType = '-1';
        RegistrationModule.CurrentSelectedEntitySubType = '-1';
        RegistrationModule.CurrentSelectedEmirate = '-1';
        RegistrationModule.ISEmirateSGotten = false;
        RegistrationModule.ISGCCNationalityAvailable = false;
        RegistrationModule.GCCNationalityCount = 0;
        RegistrationModule.EmiratisCount = 0;
        RegistrationModule.EmiratisValidationsTrailas = 0;
        RegistrationModule.EmiratesDropDOwnHTML = '';
        RegistrationModule.RegistrationObj = null;
        RegistrationModule.EntityOwnerObj = null;
        RegistrationModule.EntityDirectorObj = null;
        RegistrationModule.ResponsiblePersonObj = null;
        RegistrationModule.OwnerDetailsCompleted = false;
        RegistrationModule.DirectorDetailsCompleted = false;
        RegistrationModule.PersonsCounter = 0;
        RegistrationModule.PersonsCountertoValidate = 0;
        RegistrationModule.PersonsArr = new Array();
        $("#RegistrationStep6_PersonsDiv").html("");
    },

    GetTypesLookup: function (val) {
        if (Language == "en") {
            return val;
        }
        else {
            if (val == "Private") {
                return "خاص";
            }
            else if (val == "Federal") {
                return "حكومة اتحادية";
            }
            else if (val == "Institutions") {
                return "منشأة";
            }
            else if (val == "Ministries") {
                return "وزارات";
            }
            else if (val == "Local") {
                return "حكومة محلية";
            }
            else if (val == "Government") {
                return "حكومي";
            }
            else if (val == "Independent") {
                return "مستقل";
            }
            else {
                return val;
            }
        }
    }
};
