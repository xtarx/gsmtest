﻿var ComplaintsModule = {

    LoadComplaints: function (isFromServices) {
        if (isFromServices == true) {
            LoadPageContent("Complaints", 'ComplaintsModule.HandleComplaintsDocReady()', true, true, true, false, 'ServicesList.LoadServicesPage()');
        }
        else {
            LoadPageContent("Complaints", 'ComplaintsModule.HandleComplaintsDocReady()', true, true, true, true);
        }
    },

    HandleComplaintsDocReady: function () {
        //alert("HandleComplaintsDocReady");
        SetHeaderTitle("Complaints & Feedback", "الشكاوى و الأراء");
        BindEvents('#SendComplaintsBtn', 'click', 'ComplaintsModule.HandleComplaintsSendBtnClicked()');
        BindEvents('#ComplaintsTypeDropDown', 'change', 'ComplaintsModule.HandleComplainTypesDropDownChanged()');
        $("#ComplaintsSubTypeDropDown_Complaint").show();
        $("#ComplaintsSubTypeDropDown_Feedback").hide();
        $('#Complaints_Mobile').mask("+XXX-XX-XXXXXXX", { autoclear: false });
        $('#Complaints_Mobile').val("+971");
        if (Globals.UserLoggedIn) {
            var FullName = Globals.LoginRspObj.GPSSAUser.FullName;
            var Mobile = Globals.LoginRspObj.GPSSAUser.Mobile;
            var Email = Globals.LoginRspObj.GPSSAUser.Email;
            $('#Complaints_FullName').val(FullName).prop('disabled', true);
            $('#Complaints_Mobile').val(Mobile).prop('disabled', true);
            $('#Complaints_Mobile_Perfix').val(Mobile).prop('disabled', true);
            $('#Complaints_Email').val(Email).prop('disabled', true);
        }
        else {
            $('#Complaints_FullName').val('').prop('disabled', false);
            $('#Complaints_Mobile').val('').prop('disabled', false);
            $('#Complaints_Mobile').val("+971");
            $('#Complaints_Email').val('').prop('disabled', false);
        }

        // Detect changes
        var uploader = document.getElementsByName('Complaints_upload');
        for (item in uploader) {
            // Detect changes
            uploader[item].onchange = function () {
                $("#Complaints_CheckMark").css("display", "block");
            }
        }
        $('#Complaints_CamUpload').click(function () {
            getPhoto();
            return false;
        });

    },

    HandleComplainTypesDropDownChanged: function () {

        var val = $("#ComplaintsTypeDropDown").val();

        if (val == -1) {
            $("#ComplaintsSubTypeDropDown_Complaint").show();
            $("#ComplaintsSubTypeDropDown_Feedback").hide();
        }
        else if (val == "Complaint") {
            $("#ComplaintsSubTypeDropDown_Complaint").show();
            $("#ComplaintsSubTypeDropDown_Feedback").hide();

            $('#ComplaintsSubTypeDropDown_Complaint').val('Complaint').trigger('change');
        }
        else {
            $("#ComplaintsSubTypeDropDown_Complaint").hide();
            $("#ComplaintsSubTypeDropDown_Feedback").show();
            $('#ComplaintsSubTypeDropDown_Feedback').val('Feedback').trigger('change');
        }
    },

    HandleComplaintsSendBtnClicked: function () {

        if ($("#ComplaintsTypeDropDown").val() == "-1") {
            AlertFunction("Please Select Type", "من فضلك اختر النوع", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($("#ComplaintsSubTypeDropDown_Complaint").val() == "-1"
            && $("#ComplaintsSubTypeDropDown_Feedback").val() == "-1") {
            AlertFunction("Please Select Sub-Type", "من فضلك اختر النوع الفرعى", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#Complaints_FullName").val()) == "") {
            AlertFunction("Please enter FullName", "من فضلك ادخل الاسم بالكامل", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#Complaints_Mobile").val()) == "") {
            AlertFunction("Please enter Mobile Number", "من فضلك ادخل الهاتف المحمول", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#Complaints_Email").val()) == "") {
            AlertFunction("Please enter E-mail", "من فضلك ادخل البريد الإلكترونى", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if (!ValidateEmail($("#Complaints_Email").val())) {
            AlertFunction("Please enter valid E-mail", "من فضلك ادخل البريد الإلكترونى صالح", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#ComplaintsContent").val()) == "") {
            AlertFunction("Please enter Content", "من فضلك ادخل المحتوى", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else {
            // POST Method
            var URL = Globals.ServicesURI_Test + "add/complainsuggestionfeedback/language/#Lang#";
            URL = URL.replace("#Lang#", Language);
            var ComplaintsObj = {};
            ComplaintsObj.RequestType = $("#ComplaintsTypeDropDown").val();
            if ($("#ComplaintsSubTypeDropDown_Complaint").is(":visible")) {
                ComplaintsObj.RequestSubType = $("#ComplaintsSubTypeDropDown_Complaint").val();
            }
            else {
                ComplaintsObj.RequestSubType = $("#ComplaintsSubTypeDropDown_Feedback").val();
            }
            ComplaintsObj.Comment = $("#ComplaintsContent").val();
            if (Globals.UserLoggedIn == true) {
                ComplaintsObj.ContactRowId = Globals.ContactRowId;
            }
            else {
                ComplaintsObj.ContactRowId = "@@";
            }
            ComplaintsObj.FullName = $("#Complaints_FullName").val();
            //
            var ClearMobNum = FormatChar($("#Complaints_Mobile").val())
            ComplaintsObj.Mobile = "+" + ClearMobNum;
            //
            ComplaintsObj.Email = $("#Complaints_Email").val();

            var data = JSON.stringify(ComplaintsObj);
            Log(data, "DATA");
            CallWebService(URL, 'ComplaintsModule.HandleSendComplaintsCallSuccess(response)', "", data, "POST", true, true);
        }
    },

    HandleSendComplaintsCallSuccess: function (response) {
        try {
            if (response.Message.Code == 0) {
                var msg;
                if (Language == 'en')
                    msg = "Your request has been registered successfully, in order to track the status of your application, use this case number: ";
                else
                    msg = "لقد تم تسجيل طلبك بنجاح, من أجل متابعة حالة طلبك، استخدم هذا الرقم :";

                AlertFunction(msg + response.CaseNumber, msg + response.CaseNumber, "Success", "نجاح", "OK", "موافق");

                if (Globals.UserLoggedIn == false) {
                    CleanPage('Complaints');
                }
                else {
                    $("#ComplaintsTypeDropDown").val("-1").trigger("change");
                    $("#ComplaintsSubTypeDropDown").val("-1").trigger("change");
                    $("#ComplaintsContent").val("");
                }
            }
            else {
                HideLoadingSpinner();
                AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
            }

        } catch (e) {
        }
    }

};