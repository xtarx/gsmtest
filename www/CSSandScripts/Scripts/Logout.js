﻿var Logout = {

    CallLogout: function () {
        ConfirmationAlert("Are you sure you want to logout?", "هل انت متأكد من انك تريد الخروج؟", "Attention", "تنبيه", "OK", "موافق", "Cancel", "إلغاء", "Logout.HandleLogout()");
    },

    HandleLogout: function () {
        Globals.UserLoggedIn = false;
        $("#LoginTR").show();
        $("#LogOutTR").hide();
        StartUp.LoadStartuUp();
        Globals.UserServicesArr = null;
        localStorage.removeItem("UserServicesArr");
        localStorage.removeItem("Username");
        localStorage.removeItem("Password");
        localStorage.removeItem("LoginObj");
    }
};