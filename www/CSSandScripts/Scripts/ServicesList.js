﻿var ServicesList = {

    ContentLoaded: false,
    ServiceDetailsResp: '',
    CurrentServiceID: '',
    ServicesResp: null,
    MostRecentOffice: 0,

    LoadServicesPage: function () {

        LoadPageContent("ServicesList", 'ServicesList.HandleServicesListDocReady()', true, false, true);
    },

    HandleServicesListDocReady: function () {
        if (Globals.UserLoggedIn == true ||
            (localStorage.getItem("Username") != undefined && localStorage.getItem("Username") != null)) {
            $("#ServicesList_Tabs_PreLogin").hide();
            $("#ServicesList_Tabs_PostLogin").show();
            ServicesList.HandleServicesListTabClicked('ServicesList_MyServicesTab_PostLogin');
        }
        else {

            $("#ServicesList_Tabs_PostLogin").hide();
            $("#ServicesList_Tabs_PreLogin").show();
            ServicesList.HandleServicesListTabClicked('ServicesList_UserTypeTab');
        }

        SetHeaderTitle("Services", "الخدمات");

        if (ServicesList.ContentLoaded == false) {
            if (localStorage.getItem("GPSSAServices" + Language) == null || localStorage.getItem("GPSSAServices" + Language) == undefined) {
                var URL = Globals.ServicesURI_Test + "get/page/protalservices/language/#Lang#";
                URL = URL.replace("#Lang#", Language);
                CallWebService(URL, "ServicesList.HandleGetServicesCallSuccess(response)", "", "", "", false, true);
            }
            else {
                
                var response = localStorage.getItem("GPSSAServices" + Language);
                ServicesList.ServicesResp = JSON.parse(response);

                ServicesList.MostRecentOffice = Number(ServicesList.ServicesResp.GPSSAService[0].Modified);

                for (var i = 0; i < ServicesList.ServicesResp.GPSSAService.length; i++) {
                    var Temp = Number(ServicesList.ServicesResp.GPSSAService[i].Modified);

                    if (ServicesList.MostRecentOffice > Temp) {
                        ServicesList.MostRecentOffice = Temp;
                    }
                }
                var URL = Globals.ServicesURI_Test + "validate/list/module/services/lastupdate/#LastUpdate#/language/#Lang#";
                URL = URL.replace("#LastUpdate#", ServicesList.MostRecentOffice).replace("#Lang#", Language);
                CallWebService(URL, "ServicesList.HandleValidateServicesCallSuccess(response)", "ServicesList.HandleValidateServicesCallFailure()", "", "", false, true, false);
            }
        }
        else {
            
            if (Globals.UserLoggedIn == true ||
            (localStorage.getItem("Username") != undefined && localStorage.getItem("Username") != null)
            ) {

                var MyServicesArr = new Array();
                if (Globals.UserServicesArr == undefined)
                    Globals.UserServicesArr = JSON.parse(localStorage.getItem("UserServicesArr"));
                for (var i = 0; i < Globals.UserServicesArr.GPSSAServices.length; i++) {
                    if (Globals.UserServicesArr.GPSSAServices[i].Id != 7) {
                        MyServicesArr[i] = Globals.UserServicesArr.GPSSAServices[i];
                    }
                }
                $("#ServicesList_MyServicesTab_Content").html("");
                $("#MyServicesList_Temp").tmpl(MyServicesArr).appendTo("#ServicesList_MyServicesTab_Content");
                $("#ServicesList_MyServicesTab_Content tr:last").remove();
                $("#ServicesList_MyServicesTab_Content").height(((DeviceHeight / 3) * 2) - 30);
                HideLoadingSpinner();
            }
        }
    },

    HandleValidateServicesCallSuccess: function (response) {
        if (response != null) {
            var RespObj = response;//JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                if (RespObj.IsValid == true) {
                    ServicesList.HandleGetServicesCallSuccess(response);
                }
                else {
                    ServicesList.ServicesResp = null;
                    localStorage.removeItem("GPSSAServices" + Language);
                    var URL = Globals.ServicesURI_Test + "get/page/protalservices/language/#Lang#";
                    URL = URL.replace("#Lang#", Language);
                    CallWebService(URL, "ServicesList.HandleGetServicesCallSuccess(response)", "", "", "", true, true);
                }
            }
        }
    },

    HandleValidateServicesCallFailure: function () {
        var Obj = localStorage.getItem("GPSSAServices" + Language);
        var Resp = JSON.parse(Obj);
        ServicesList.HandleGetServicesCallSuccess(Resp);
    },

    HandleGetServicesCallSuccess: function (response) {
        try {

            if (response != null) {

                var RespObj = response;// JSON.parse(response);

                if (RespObj.Message.Code == 0) {
                    localStorage.setItem("GPSSAServices" + Language, JSON.stringify(response));
                    ServicesList.ContentLoaded = true;
                    var AlphabeticallyServices = new Array();
                    var UserServices = new Array();
                    var BeneficiaryArray = new Array();
                    var EmployerServices = new Array();
                    var GCCServices = new Array();
                    var PensionerServices = new Array();
                    var InsuredServices = new Array();
                    try {

                        var ServicesListArr = RespObj.GPSSAService;
                        var BeneficiaryServicesCounter = 0;
                        var InsuredServicesCounter = 0;
                        var EmployerServicesCounter = 0;
                        var GCCServicesCounter = 0;
                        var PensionerServicesCounter = 0;

                        try {

                            for (var i = 0; i < ServicesListArr.length; i++) {

                                for (var j = 0; j < ServicesListArr[i].UserTypes.length; j++) {

                                    if (Language == "en") {
                                        if (ServicesListArr[i].UserTypes[j] == "Beneficiary") {
                                            if (!BeneficiaryArray.indexOf(ServicesListArr[i].Name) > -1) {
                                                BeneficiaryArray[BeneficiaryServicesCounter] = ServicesListArr[i].Name + "#" + ServicesListArr[i].Id;
                                                BeneficiaryServicesCounter += 1;
                                            }
                                        }
                                        else if (ServicesListArr[i].UserTypes[j] == "Employer") {
                                            if (!EmployerServices.indexOf(ServicesListArr[i].Name) > -1) {
                                                EmployerServices[EmployerServicesCounter] = ServicesListArr[i].Name + "#" + ServicesListArr[i].Id;
                                                EmployerServicesCounter += 1;
                                            }
                                        }
                                        else if (ServicesListArr[i].UserTypes[j] == "Insured") {
                                            if (!InsuredServices.indexOf(ServicesListArr[i].Name) > -1) {
                                                InsuredServices[InsuredServicesCounter] = ServicesListArr[i].Name + "#" + ServicesListArr[i].Id;
                                                InsuredServicesCounter += 1;
                                            }
                                        }
                                        else if (ServicesListArr[i].UserTypes[j] == "GCC") {
                                            if (!GCCServices.indexOf(ServicesListArr[i].Name) > -1) {
                                                GCCServices[GCCServicesCounter] = ServicesListArr[i].Name + "#" + ServicesListArr[i].Id;
                                                GCCServicesCounter += 1;
                                            }
                                        }
                                        else if (ServicesListArr[i].UserTypes[j] == "Pensioner") {
                                            if (!PensionerServices.indexOf(ServicesListArr[i].Name) > -1) {
                                                PensionerServices[PensionerServicesCounter] = ServicesListArr[i].Name + "#" + ServicesListArr[i].Id;
                                                PensionerServicesCounter += 1;
                                            }
                                        }
                                    }
                                    else {
                                        if (ServicesListArr[i].UserTypes[j] == "المستفيد") {
                                            if (!BeneficiaryArray.indexOf(ServicesListArr[i].Name) > -1) {
                                                BeneficiaryArray[BeneficiaryServicesCounter] = ServicesListArr[i].Name + "#" + ServicesListArr[i].Id;
                                                BeneficiaryServicesCounter += 1;
                                            }
                                        }
                                        else if (ServicesListArr[i].UserTypes[j] == "أصحاب الأعمال") {
                                            if (!EmployerServices.indexOf(ServicesListArr[i].Name) > -1) {
                                                EmployerServices[EmployerServicesCounter] = ServicesListArr[i].Name + "#" + ServicesListArr[i].Id;
                                                EmployerServicesCounter += 1;
                                            }
                                        }
                                        else if (ServicesListArr[i].UserTypes[j] == "المؤمن عليه") {
                                            if (!InsuredServices.indexOf(ServicesListArr[i].Name) > -1) {
                                                InsuredServices[InsuredServicesCounter] = ServicesListArr[i].Name + "#" + ServicesListArr[i].Id;
                                                InsuredServicesCounter += 1;
                                            }
                                        }
                                        else if (ServicesListArr[i].UserTypes[j] == "مواطن مجلس التعاون الخليجى") {
                                            if (!GCCServices.indexOf(ServicesListArr[i].Name) > -1) {
                                                GCCServices[GCCServicesCounter] = ServicesListArr[i].Name + "#" + ServicesListArr[i].Id;
                                                GCCServicesCounter += 1;
                                            }
                                        }
                                        else if (ServicesListArr[i].UserTypes[j] == "المستحق") {
                                            if (!PensionerServices.indexOf(ServicesListArr[i].Name) > -1) {
                                                PensionerServices[PensionerServicesCounter] = ServicesListArr[i].Name + "#" + ServicesListArr[i].Id;
                                                PensionerServicesCounter += 1;
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (e) {
                        }
                        var AlphabeticallyServices = ServicesListArr.sort(function (a, b) {
                            if (a.Name > b.Name) {
                                return 1;
                            }
                            if (a.Name < b.Name) {
                                return -1;
                            }
                            return 0;
                        });

                    } catch (e) {
                    }

                    $("#ServicesList_Temp_Alpha").tmpl(AlphabeticallyServices).appendTo("#ServicesList_AlphabeticallyTab_Content");
                    $("#ServicesList_AlphabeticallyTab_Content tr:last").remove();
                    $("#ServicesList_AlphabeticallyTab_Content").height(((DeviceHeight / 3) * 2) - 30);

                    if (Globals.UserLoggedIn == true ||
            (localStorage.getItem("Username") != undefined && localStorage.getItem("Username") != null)
            ) {
                        var MyServicesArr = new Array();
                        if (Globals.UserServicesArr == undefined)
                            Globals.UserServicesArr = JSON.parse(localStorage.getItem("UserServicesArr"));

                        for (var i = 0; i < Globals.UserServicesArr.GPSSAServices.length; i++) {
                            if (ServicesList.IsServiceAvailableOnMobile(Globals.UserServicesArr.GPSSAServices[i].Id)) {
                                MyServicesArr[i] = Globals.UserServicesArr.GPSSAServices[i];
                            }
                        }
                        $("#ServicesList_MyServicesTab_Content").html("");
                        $("#MyServicesList_Temp").tmpl(MyServicesArr).appendTo("#ServicesList_MyServicesTab_Content");
                        $("#ServicesList_MyServicesTab_Content tr:last").remove();
                        $("#ServicesList_MyServicesTab_Content").height(((DeviceHeight / 3) * 2) - 30);
                    }
                    var listitemheight = 42.5;
                    $("#ServicesList_UserTypeTab_Beneficiary_Content").html(ServicesList.CreateServiceHTMl(BeneficiaryArray));
                    $("#ServicesList_UserTypeTab_Beneficiary_Content tr:last").remove();
                    $("#ServicesList_UserTypeTab_Beneficiary_Content").height(listitemheight * BeneficiaryArray.length);

                    $("#ServicesList_UserTypeTab_Employer_Content").html(ServicesList.CreateServiceHTMl(EmployerServices));
                    $("#ServicesList_UserTypeTab_Employer_Content tr:last").remove();
                    $("#ServicesList_UserTypeTab_Employer_Content").height(listitemheight * EmployerServices.length);

                    $("#ServicesList_UserTypeTab_GCC_Content").html(ServicesList.CreateServiceHTMl(GCCServices));
                    $("#ServicesList_UserTypeTab_GCC_Content tr:last").remove();
                    $("#ServicesList_UserTypeTab_GCC_Content").height(listitemheight * GCCServices.length);

                    $("#ServicesList_UserTypeTab_Insured_Content").html(ServicesList.CreateServiceHTMl(InsuredServices));
                    $("#ServicesList_UserTypeTab_Insured_Content tr:last").remove();
                    $("#ServicesList_UserTypeTab_Insured_Content").height(listitemheight * InsuredServices.length);

                    $("#ServicesList_UserTypeTab_Pensioner_Content").html(ServicesList.CreateServiceHTMl(PensionerServices));
                    $("#ServicesList_UserTypeTab_Pensioner_Content tr:last").remove();
                    $("#ServicesList_UserTypeTab_Pensioner_Content").height(listitemheight * PensionerServices.length);
                }
                else {
                    AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
                }
            }
            
        } catch (e) {
            HideLoadingSpinner();
        }
    },

    IsServiceAvailableOnMobile: function (id) {
        if (id == 7)
            return false;
        return true;
    },

    CreateServiceHTMl: function (arr) {
        try {
            var Temp = $("#ServicesList_Temp").html();
            var AllHtml = "";
            for (var i = 0; i < arr.length; i++) {
                var TempArr = arr[i].split('#');
                AllHtml += Temp.replace("#Name#", TempArr[0]).replace("#ID#", TempArr[1]);
                Temp = $("#ServicesList_Temp").html();
            }
            return AllHtml;
        } catch (e) {
        }
    },

    HandleServicesListTabClicked: function (id) {
        $('.ServiceListContent').hide();
        $(".ServicesTabs").removeClass("ActiveTabClass");
        $(".ServicesTabs").addClass("TabClass");
        $("#" + id).addClass("ActiveTabClass");
        if (id == "ServicesList_UserTypeTab" || id == "ServicesList_UserTypeTab_PostLogin") {
            $("#ServicesList_UserTypeTab_Content").show();
            $("#ServicesList_AlphabeticallyTab_Content").hide();
            $("#ServicesList_MyServicesTab_Content").hide();
        }
        else if (id == "ServicesList_AlphabeticallyTab" || id == "ServicesList_AlphabeticallyTab_PostLogin") {
            $("#ServicesList_AlphabeticallyTab_Content").show();
            $("#ServicesList_MyServicesTab_Content").hide();
            $("#ServicesList_UserTypeTab_Content").hide();
        }
        else {
            $("#ServicesList_MyServicesTab_Content").show();
            $("#ServicesList_AlphabeticallyTab_Content").hide();
            $("#ServicesList_UserTypeTab_Content").hide();
        }
    },

    OpenMyService: function (id) {
        if (Globals.UserLoggedIn == true) {

            if (id == 1) {
                RegistrationModule.LoadRegistration(true);
            }
            else if (id == 2) {
                AutoRegisteration.LoadAutoRegistrationContent(true);
            }
            else if (id == 3) {
                PensionCalculation.LoadPensionCalculation(true)
            }
            else if (id == 4) {
                UpdateEmployer.LoadUpdateEmployer(true);
            }
            else if (id == 5) {
                InsuredEmployees.LoadInsuredEmployees(true);
            }
            else if (id == 6) {
                UpdateResponsiblePerson.LoadUpdateResponsiblePersonPage(true);
            }
            else if (id == 8) {
                AddResponsiblePerson.LoadResponsiblePerson(true);
            }
            else if (id == 9) {
                ComplaintsModule.LoadComplaints(true);
            }
            return;
        }

        if (localStorage.getItem("Username") != undefined && localStorage.getItem("Username") != null) {

            if (id == 1) {
                Login.Businessfunction = RegistrationModule.LoadRegistration;
            }
            else if (id == 2) {
                Login.Businessfunction = AutoRegisteration.LoadAutoRegistrationContent;
            }
            else if (id == 3) {
                Login.Businessfunction = PensionCalculation.LoadPensionCalculation;
            }
            else if (id == 4) {
                Login.Businessfunction = UpdateEmployer.LoadUpdateEmployer;
            }
            else if (id == 5) {
                Login.Businessfunction = InsuredEmployees.LoadInsuredEmployees;
            }
            else if (id == 6) {
                Login.Businessfunction = UpdateResponsiblePerson.LoadUpdateResponsiblePersonPage;
            }
            else if (id == 8) {
                Login.Businessfunction = AddResponsiblePerson.LoadResponsiblePerson;
            }
            else if (id == 9) {
                Login.Businessfunction = ComplaintsModule.LoadComplaints;
            }
            Login.NeedBusinessAfterLogin = true;
            Login.LoadLoginPage();
        }
    },

    ShowUserTypeContent: function (id) {
        if (!$("#" + id).is(':visible')) {
            $(".UserTypeContent").hide();
            $("#" + id).fadeIn("slow");
        }
        else {
            $(".UserTypeContent").hide();
        }
    },

    OpenServiceDetails: function (ServiceID) {
        try {

            ServicesList.CurrentServiceID = ServiceID;

            if (localStorage.getItem("ServiceItem" + Language + ServicesList.CurrentServiceID) == null || localStorage.getItem("ServiceItem" + Language + ServicesList.CurrentServiceID) == undefined) {

                var URL = Globals.ServicesURI_Test + "get/page/protalservicebyid/serviceid/#ID#/language/#Lang#";
                URL = URL.replace("#ID#", ServiceID).replace("#Lang#", Language);

                CallWebService(URL, "ServicesList.HandleGetServiceDetailsCallSucess(response)", "", "", "", true, false);
            }
            else {
                var response = localStorage.getItem("ServiceItem" + Language + ServicesList.CurrentServiceID);
                var obj = JSON.parse(response);
                ServicesList.ServiceDetailsResp = obj.PortalService;

                var URL = Globals.ServicesURI_Test + "validate/item/module/services/itemid/1/lastupdate/#LastUpdate#/language/#Lang#";
                URL = URL.replace("#LastUpdate#", ServicesList.ServiceDetailsResp.Modified).replace("#Lang#", Language);
                CallWebService(URL, "ServicesList.HandleValidateServiceItemCallSuccess(response)", "ServicesList.HandleValidateServiceItemCallFailure()", "", "", true, true, false);
            }

        } catch (e) {

        }
    },

    HandleValidateServiceItemCallSuccess: function (response) {
        if (response != null) {
            var RespObj = response;//JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                if (RespObj.IsValid == true) {
                    ServicesList.HandleGetServiceDetailsCallSucess(response);
                }
                else {
                    ServicesList.ServiceDetailsResp = null;
                    localStorage.removeItem("ServiceItem" + Language + ServicesList.CurrentServiceID);
                    var URL = Globals.ServicesURI_Test + "get/page/protalservicebyid/serviceid/#ID#/language/#Lang#";
                    URL = URL.replace("#ID#", ServicesList.CurrentServiceID).replace("#Lang#", Language);

                    CallWebService(URL, "ServicesList.HandleGetServiceDetailsCallSucess(response)", "", "", "", true, false);
                }
            }
        }
    },

    HandleValidateServiceItemCallFailure: function () {
        var Resp = localStorage.getItem("ServiceItem" + Language + ServicesList.CurrentServiceID);
        var obj = JSON.parse(Resp);
        ServicesList.ServiceDetailsResp = obj.PortalService;
        ServicesList.HandleGetServiceDetailsCallSucess(Resp);
    },

    HandleGetServiceDetailsCallSucess: function (response) {
        try {
            if (response != null) {
                var RespObj = response;//JSON.parse(response);

                if (RespObj.Message.Code == 0) {
                    localStorage.setItem("ServiceItem" + Language + ServicesList.CurrentServiceID, JSON.stringify(response));
                    ServicesList.ServiceDetailsResp = RespObj.PortalService;
                    LoadPageContent("ServiceDetails", 'ServicesList.HandleServiceDetailsDocReady()', false, true, true, false, "ServicesList.LoadServicesPage()");
                }
                else {
                    AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
                }
            }
        } catch (e) {

        }
    },

    HandleServiceDetailsDocReady: function () {
        try {
            SetHeaderTitle("Service Description", "وصف الخدمة");
            $("#ServiceName").html('');
            $("#ServiceDescription").html('');
            $("#CustomerServiceContact").html('');
            $("#Department").html('');
            $("#EstimatedServiceDuration").html('');
            $("#GPSSAOffices").html('');
            $("#OfficerInCharge").html('');
            $("#Required").html('');
            $("#RequiredDocuments").html('');
            $("#ServiceFeeDetails").html('');
            $("#ServiceProcedure").html('');
            $("#ServiceRecipient").html('');
            $("#ServiceUsers").html('');
            BindEvents('#ServiceDetails_BackSingleBtn', 'click', 'ServicesList.HandleBackBtnClicked()');
            BindEvents('#ServiceDetails_BackBtn', 'click', 'ServicesList.HandleBackBtnClicked()');
            BindEvents('#GoToService', 'click', 'ServicesList.GoToService()');

            if (isUndefinedOrNullOrBlank(ServicesList.ServiceDetailsResp.ServiceDescription)) {
                $("#ServiceDescriptionDiv").hide();
            }
            else {
                $("#ServiceDescriptionDiv").show();
                $("#ServiceDescription").html(ServicesList.ServiceDetailsResp.ServiceDescription);
            }

            if (isUndefinedOrNullOrBlank(ServicesList.ServiceDetailsResp.CustomerServiceContact))
                $("#CustomerServiceContactDiv").hide();
            else {
                $("#CustomerServiceContactDiv").show();
                $("#CustomerServiceContact").html(ServicesList.ServiceDetailsResp.CustomerServiceContact);
            }

            if (isUndefinedOrNullOrBlank(ServicesList.ServiceDetailsResp.Department))
                $("#DepartmentDiv").hide();
            else {
                $("#DepartmentDiv").show();
                $("#Department").html(ServicesList.ServiceDetailsResp.Department);
            }

            if (isUndefinedOrNullOrBlank(ServicesList.ServiceDetailsResp.EstimatedServiceDuration))
                $("#EstimatedServiceDurationDiv").hide();
            else {
                $("#EstimatedServiceDurationDiv").show();
                $("#EstimatedServiceDuration").html(ServicesList.ServiceDetailsResp.EstimatedServiceDuration);
            }

            if (isUndefinedOrNullOrBlank(ServicesList.ServiceDetailsResp.GPSSAOffices))
                $("#GPSSAOfficesDiv").hide();
            else {
                $("#GPSSAOfficesDiv").show();
                for (var i = 0; i < ServicesList.ServiceDetailsResp.GPSSAOffices.length; i++) {
                    $("#GPSSAOffices").append(ServicesList.ServiceDetailsResp.GPSSAOffices[i] + "<br />");
                }
            }

            if (isUndefinedOrNullOrBlank(ServicesList.ServiceDetailsResp.OfficerInCharge))
                $("#OfficerInChargeDiv").hide();
            else {
                $("#OfficerInChargeDiv").show();
                $("#OfficerInCharge").html(ServicesList.ServiceDetailsResp.OfficerInCharge);
            }

            if (isUndefinedOrNullOrBlank(ServicesList.ServiceDetailsResp.Required))
                $("#RequiredDiv").hide();
            else {
                $("#RequiredDiv").show();
                $("#Required").html(ServicesList.ServiceDetailsResp.Required);
            }

            if (isUndefinedOrNullOrBlank(ServicesList.ServiceDetailsResp.RequiredDocuments))
                $("#RequiredDocumentsDiv").hide();
            else {
                $("#RequiredDocumentsDiv").show();
                $("#RequiredDocuments").html(ServicesList.ServiceDetailsResp.RequiredDocuments);
            }

            if (isUndefinedOrNullOrBlank(ServicesList.ServiceDetailsResp.ServiceFeeDetails))
                $("#ServiceFeeDetailsDiv").hide();
            else {
                $("#ServiceFeeDetailsDiv").show();
                $("#ServiceFeeDetails").html(ServicesList.ServiceDetailsResp.ServiceFeeDetails);
            }
            if (isUndefinedOrNullOrBlank(ServicesList.ServiceDetailsResp.ServiceName))
                $("#ServiceNameDiv").hide();
            else {
                $("#ServiceNameDiv").show();
                $("#ServiceName").html(ServicesList.ServiceDetailsResp.ServiceName);
            }

            if (isUndefinedOrNullOrBlank(ServicesList.ServiceDetailsResp.ServiceProcedure))
                $("#ServiceProcedureDiv").hide();
            else {
                $("#ServiceProcedureDiv").show();
                $("#ServiceProcedure").html(ServicesList.ServiceDetailsResp.ServiceProcedure);
            }
            if (isUndefinedOrNullOrBlank(ServicesList.ServiceDetailsResp.ServiceRecipient))
                $("#ServiceRecipientDiv").hide();
            else {
                $("#ServiceRecipientDiv").show();
                for (var i = 0; i < ServicesList.ServiceDetailsResp.ServiceRecipient.length; i++) {
                    $("#ServiceRecipient").append(ServicesList.ServiceDetailsResp.ServiceRecipient[i] + "<br />");
                }
            }

            if (isUndefinedOrNullOrBlank(ServicesList.ServiceDetailsResp.ServiceUsers))
                $("#ServiceUsersDiv").hide();
            else {
                $("#ServiceUsersDiv").show();
                for (var i = 0; i < ServicesList.ServiceDetailsResp.ServiceUsers.length; i++) {
                    $("#ServiceUsers").append(ServicesList.ServiceDetailsResp.ServiceUsers[i] + "<br />");
                }
                var Continue = false;

                var MyServicesArr = new Array();
                if (Globals.UserServicesArr == undefined)
                    Globals.UserServicesArr = JSON.parse(localStorage.getItem("UserServicesArr"));
                if (!isUndefinedOrNullOrBlank(Globals.UserServicesArr)) {
                    for (var i = 0; i < Globals.UserServicesArr.GPSSAServices.length; i++) {
                        if (Globals.UserServicesArr.GPSSAServices[i].Id == ServicesList.CurrentServiceID
                            && ServicesList.IsServiceAvailableOnMobile(Globals.UserServicesArr.GPSSAServices[i].Id)) {
                            Continue = true;
                            continue;
                        }
                    }
                }
                if (!Continue) {
                    $('#ServiceDetails_BackSingleBtn').show();
                    $('#ServiceDetails_BackMultiBtn').hide();
                }
                else {
                    $('#ServiceDetails_BackMultiBtn').show();
                    $('#ServiceDetails_BackSingleBtn').hide();
                }
                //if (localStorage.getItem("Username") == undefined || localStorage.getItem("Username") == null)
                //{
                //    $('#ServiceDetails_BackSingleBtn').show();
                //    $('#ServiceDetails_BackMultiBtn').hide();
                //}
            }
        } catch (e) {

        }
    },

    GoToService: function () {
        ServicesList.OpenMyService(ServicesList.CurrentServiceID);

    },

    HandleBackBtnClicked: function () {
        try {
            LoadPageContent("ServicesList", '', true, false, false);
        } catch (e) {

        }
    }
};