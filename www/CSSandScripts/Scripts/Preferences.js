﻿var Preferences = {

    LoadPreferencesContent: function () {
        LoadPageContent("Preferences", 'Preferences.HandlePreferencesDocReady()', true, true);
    },

    HandlePreferencesDocReady: function () {
        SetHeaderTitle("preference", "تفضيلات");
        BindEvents('#PreferencesSaveBtn', 'click', 'Preferences.HandleSaveBtnClicked()');

        $("#Preference_Language").val(Globals.LoginRspObj.GPSSAUser.PreferredLanguage).trigger("change");
        $("#Preference_Contact").val(Globals.LoginRspObj.GPSSAUser.PreferredContactMethod).trigger("change");
    },

    HandleSaveBtnClicked: function () {

        var LanguageVal = $("#Preference_Language").val();
        var Contact = $("#Preference_Contact").val();

        if (LanguageVal == -1 && Contact == -1) {
            AlertFunction("Please Select Preferences to update", "من فضلك اختر التفضيلات لتحديثها", "Error", "خطأ", "OK", "موافق");
            return;
        }

        var ReqObj = Globals.LoginRspObj.GPSSAUser;

        if (LanguageVal != -1) {
            ReqObj.PreferredLanguage = LanguageVal;
        }

        if (Contact != -1) {
            ReqObj.PreferredContactMethod = Contact;
        }

        var URL = Globals.ServicesURI_Test + "update/gpssauser/language/#Lang#";
        URL = URL.replace("#Lang#", Language);
        var data = JSON.stringify(ReqObj);

        CallWebService(URL, 'Preferences.HandleUpdatePreferencesSuccess(response)', "", data, "POST", true, true);

        //var val = $("#LangDropDown").val();

        //if ((Language == "en" && val == "English") || Language == "ar" && val == "Arabic") {
        //    return;
        //}
        //else {
        //    ConfirmationAlert("Are you sure that you want change the application language", "هل انت متأكد من انك تريد تغيير لغة البرنامج", "Attention", "تنبيه", "OK", "موافق", "Cancel", "إلغاء", "Preferences.HandlePreferencesChangeLanguage()");
        //}
    },

    HandleUpdatePreferencesSuccess: function (response) {
        try {

            if (response != null) {
                if (response.Message.Code == 0) {
                    AlertFunction("Preferences updated successfully", "تم تحديث التفضيلات بنجاح", "Success", "نجاح", "OK", "موافق");
                    //           $("#Preference_Language").val(-1).trigger("change");
                    //        $("#Preference_Contact").val(-1).trigger("change");
                }
                else {
                    AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
                }
            }
        } catch (e) {
        }
    },


    HandlePreferencesChangeLanguage: function () {
        HandleBtnLangClicked();
    }
};