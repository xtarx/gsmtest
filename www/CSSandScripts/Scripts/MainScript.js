﻿var mobileOS;
var Language;
var DeviceHeight;
var DeviceWidth;
var CurrentActivePage = "";
var ISEmirateSGotten = false;
var EmiratesHTML = "";
var CurrentActiveFooterTab = "";
var GPSSAOfficeResponseGotten = false;
var GPSSAOfficeResponse = null;
var WidgetsArr = new Array();
var CurrentView = null;
var NoofShownSpinner = 0;
var CallTimeOut;

$.ajaxPrefilter(function (options, originalOptions, jqXHR) { });

$(window).on("orientationchange", function (event) {
    HandleOrientationChanged();
});

$(document).ready(function () {
    try {

        DeviceHeight = window.innerHeight;
        DeviceWidth = window.innerWidth;
        $(".PageClass").height(DeviceHeight - 100);
        $(".ContentClass").height(DeviceHeight - 100);
        $(".DivContainerClass").height(DeviceHeight - 100);
        $(".DivContainerClass").css("overflow", "scroll");
        $("#SpinnerContentDiv").css('margin-top', (DeviceHeight / 2) - (DeviceHeight * .1));
        $("#ALLPageContent").width(DeviceWidth - 20);
        $("#FooterDiv").show();
        DisableWindowScroll();
        Language = localStorage.getItem("Lang");
        if (Language != "en") {
            Language = "ar"
        }
        LoadPageContent("StartUp", 'StartUp.HandleStartUpDocReady()', true, true);
        BindEvents("#HomeTR", "click", "StartUp.LoadStartuUp();ClearFooterTab()");
        BindEvents("#ComplaintsTR", "click", "ComplaintsModule.LoadComplaints();ClearFooterTab()");
        BindEvents("#RegisterationTR", "click", "AutoRegisteration.LoadAutoRegistrationContent();ClearFooterTab()");
        BindEvents("#RegisterNewEmployerTR", "click", "RegistrationModule.LoadRegistration()");
        BindEvents('#ServicesTR', 'click', 'ServicesList.LoadServicesPage(); ClearFooterTab()');
        BindEvents('#AboutGAPSSATR', 'click', 'AboutGPSSA.LoadAboutGPSSAPage(); ClearFooterTab()');
        BindEvents('#NewsTR', 'click', 'News.LoadNewsPage(); ClearFooterTab()');
        BindEvents('#LoginTR', 'click', 'Login.LoadLoginPage(); ClearFooterTab()');
        BindEvents('#NearestGAPSSATR', 'click', 'NearestOffice.LoadNearestOfficeContent()');
        BindEvents('#LogOutTR', 'click', 'Logout.CallLogout(); ClearFooterTab()');
        BindEvents('#termsTR', 'click', 'Terms.LoadTermsContent();ClearFooterTab()');
        BindEvents('#privacypolicyTR', 'click', 'Privacy.LoadPrivacyContent();ClearFooterTab()');
        BindEvents('#PensionsGuideTR', 'click', 'PensionsGuide.LoadPensionsGuideContent();ClearFooterTab()');
        BindEvents('#preferenceTR', 'click', 'Preferences.LoadPreferencesContent();ClearFooterTab()');
        BindEvents('#HelpTR', 'click', 'Help.LoadHelpContent();ClearFooterTab()');
        BindEvents('#ShareTR', 'click', 'Share();ClearFooterTab()');
        HandleInputsFn();
        var URL = Globals.ServicesURI_Test + "view/lov/type/GPSSA_EMIRATE/language/#Lang#";
        URL = URL.replace("#Lang#", Language);
        CallWebServiceWithOauth(URL, 'GetEmiratesCallSuccess(response)', "", "", "", false, false, false);
        try {
            var PageHeight;
            $("#MenuPanel").panel({
                beforeopen: function (event, ui) {
                    PageHeight = $(".PageClass").height();
                    //$(".PageClass").height(DeviceHeight * 1.3)
                    $(".PageClass").height(750);
                    //setTimeout(function () {
                    //    $(".ui-panel-wrapper").css('position', 'fixed');
                    //}, 1000);
        }
    });
            $("#MenuPanel").panel({
                beforeclose: function (event, ui) {
                    $(".PageClass").height(PageHeight)
                    //setTimeout(function () {
                    //    $(".ui-panel-wrapper").css('position', 'relative');
                    //}, 500);
        }
    })
        } catch (e) { }

    } catch (e) {
        Log(e, "Doc Ready")
    }
});

function Share() {
    try {
        //jsinterface.Share();
        window.plugins.socialsharing.share('GPSSA Smart App', null, null, 'https://play.google.com/store/apps/details?id=com.phonegap.gpssaMGov&hl=en');
    } catch (e) {
        alert("Share" + e.toString());
    }
}

function GetEmiratesCallSuccess(response) {
    try {
        if (response != null) {
            var OptionTemp = "<option value='#Val#'>#text#</option>";
            var DefaultOption = "";
            var AllHTML = "";
            if (response.LOVs != null && response.LOVs.length > 0) {
                if (Language == "en") {
                    AllHTML += "<option value='-1'>Select Emirate</option>"
                } else {
                    AllHTML += "<option value='-1'>اختر الإمارة</option>"
                }
                for (var i = 0; i < response.LOVs.length; i++) {
                    OptionTemp = "<option value='#Val#'>#text#</option>";
                    AllHTML += OptionTemp.replace("#Val#", response.LOVs[i]).replace("#text#", response.LOVs[i])
                }
                ISEmirateSGotten = true;
                EmiratesHTML = AllHTML
            }
        }
    } catch (e) { }
}

function GetEmiratesCallFailure() {
    AlertFunction("Communication error, please make sure of internet connection", "حدث خطأ اثناء الاتصال من فضلك تأكد من الاتصال بالانترنت", "Error", "خطأ", "OK", "موافق")
}

function Log(msg, caller) {
    if (caller == undefined) {
        caller = ""
    }
    console.log("------------------ Start Logs at " + caller + " ---------------");
    console.log(msg);
    console.log("------------------ end ---------------")
}

function SetHeaderTitle(Header_EN, Header_AR) {
    if (Language == "en") {
        $("#HeaderTitle").html(Header_EN)
    } else {
        $("#HeaderTitle").html(Header_AR)
    }
}

function OpenMenu() {
    $("#MenuPanel").panel("open")
}

function CloseMenu() {
    $("#MenuPanel").panel("close")
}

function ClearFooterTab() {
    CurrentActiveFooterTab = ""
}

function HandleFooterTabsClicked(id) {
    $('.FooterTabs').removeClass("FooterActiveTabClass");
    $('.FooterTabs').addClass("FooterTabClass");
    $('#' + id).addClass("FooterActiveTabClass");
    if (id == CurrentActiveFooterTab) {
        return
    }
    CurrentActiveFooterTab = id;
    if (id == "Footer_ContactUs") {
        ContactUs.LoadContactUsPage()
    } else if (id == "Footer_NearestOffice") {
        NearestOffice.LoadNearestOfficeContent()
    } else if (id == "Footer_FeedBacK") {
        ComplaintsModule.LoadComplaints()
    }
}

function HandleBtnLangClicked() {

    if (localStorage.getItem("Lang") == "en") {
        localStorage.setItem("Lang", "ar");
        Language = "ar";
        window.location = "index_AR.html"
    } else {
        Language = "en";
        localStorage.setItem("Lang", "en");
        window.location = "index_EN.html"
    }

    try {
        //window.plugins.spinnerDialog.show(null, null, true);
    } catch (e) {

    }
}

function HandleOrientationChanged() {
    if (Language == "ar") {
        localStorage.setItem("Lang", "ar");
        window.location = "index_AR.html"
    } else {
        Language = "en";
        localStorage.setItem("Lang", "en");
        window.location = "index_EN.html"
    }
}

function LoadPageContent(PageID, successFunction, ShowSpinner, HideSpinner, CallSuccessAfterGettingFromCache, RootPage, FunctionStr) {
    try {

        var url = "";

        if (CurrentActivePage == PageID) {
            CloseMenu();
            return
        }

        if (ShowSpinner == undefined || ShowSpinner == null) {
            ShowSpinner = false
        }
        if (HideSpinner == undefined || HideSpinner == null) {
            HideSpinner = false
        }
        if (CallSuccessAfterGettingFromCache == undefined || CallSuccessAfterGettingFromCache == null) {
            CallSuccessAfterGettingFromCache = true
        }

        if (RootPage == undefined || RootPage == null) {
            RootPage = true;
        }

        if (FunctionStr == undefined) {
            FunctionStr = "";
        }

        if (RootPage == true) {
            WidgetsArr = new Array();
            if (PageID == "StartUp") {
                WidgetsArr.push("");
            }
            else {
                WidgetsArr.push("StartUp.LoadStartuUp()");
            }
        }
        else {
            WidgetsArr.push(FunctionStr);
        }

        $('.FooterTabs').removeClass("FooterActiveTabClass");
        $('.FooterTabs').addClass("FooterTabClass");

        if (PageID == "ContactUS") {
            $('#Footer_ContactUs').addClass("FooterActiveTabClass")
        } else if (PageID == "Complaints") {
            $('#Footer_FeedBacK').addClass("FooterActiveTabClass")
        } else if (PageID == "NearestOffice") {
            $('#Footer_NearestOffice').addClass("FooterActiveTabClass")
        } else if (PageID == "ContactusMap") {
            $('#Footer_ContactUs').addClass("FooterActiveTabClass")
            PageID = "NearestOffice";
        }

        if ($("#" + PageID).length > 0) {
            setTimeout(function () {
                CloseMenu()
            }, 100);
            $(".DivContainerClass").hide();
            $("#" + PageID).show();
            $("#" + PageID).scrollTop(0);
            if (CallSuccessAfterGettingFromCache) {
                eval(successFunction)
            }
            if (HideSpinner) {
                HideLoadingSpinner();
            }
            CurrentActivePage = PageID;
            Log("Load content from cache", "")
        } else {
            Log("Load new content", "");
            if (ShowSpinner) {
                ShowLoadingSpinner()
            }
            CloseMenu();
            CurrentActivePage = PageID;
            if (Language == "en") {
                url = PageID + "_EN.html"
            } else {
                url = PageID + "_AR.html"
            }
            $.ajax({
                type: "GET",
                url: 'Templates/' + url,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                dataType: "html",
                data: '',
                success: function (response, textStatus, XMLHttpResponse) {
                    $("#ALLPageContent").append(response);
                    $(".DivContainerClass").hide();
                    $("#" + PageID).show();
                    $("#" + PageID).scrollTop(0);
                    EnbleWindowScroll();
                    BindEvents('#MenuIconTD', 'click', 'OpenMenu()');
                    eval(successFunction);
                    if (HideSpinner) {
                        HideLoadingSpinner()
                    }
                },
                error: function (xhr, status, error) {
                    EnbleWindowScroll();
                    AlertFunction("Communication error, please make sure of internet connection", "حدث خطأ اثناء الاتصال من فضلك تأكد من الاتصال بالانترنت", "Error", "خطأ", "OK", "موافق");
                    HideLoadingSpinner()
                }
            })
}
} catch (e) {
    Log(e, "LoadPageContent")
}
}

function BindEvents(ID, event, functionName) {
    try {
        $(ID).unbind(event);
        $(ID).on(event, function () {
            eval(functionName)
        })
    } catch (e) {
        Log(e, "BindEvents")
    }
}

function CallWebServiceWithOauth(url, SuccessFunction, FailureFunction, Data, Type, ShowSpinner, HideSpinner, showDefaultErrorAlert, timeout) {
    try {

        var request_data;
        var ProcessData = true;
        var DataType = "json";
        if (ShowSpinner == undefined || ShowSpinner == null) {
            ShowSpinner = false
        }
        if (HideSpinner == undefined || HideSpinner == null) {
            HideSpinner = false
        }
        if (showDefaultErrorAlert == undefined || showDefaultErrorAlert == null) {
            showDefaultErrorAlert = true
        }
        if (ShowSpinner) {
            ShowLoadingSpinner()
        }
        if (Data == undefined || Data == "") {
            Data = '{}'
        }
        if (Type == undefined || Type == "") {
            Type = 'GET';
            ProcessData = false;
            DataType = "TEXT";
        }
        try {

            if (!timeout) {
                timeout = 60000;
            }

            CallTimeOut = setTimeout(function () {
                AlertFunction("Connection timeout", "Connection timeout", "Error", "خطأ", "OK", "موافق");
                HideLoadingSpinner();
            }, timeout + timeout / 5);

            var ServicURL = GetOAuthURL(url);
            Log(ServicURL);
            $.ajax({
                url: ServicURL,
                type: Type,
                data: Data,
                timeout: timeout,
                contentType: "application/json",
                success: function (response, textStatus, xhr) {
                    clearTimeout(CallTimeOut);
                    Log(response, "ajax Response");
                    eval(SuccessFunction);
                    if (HideSpinner) {
                        HideLoadingSpinner()
                    }
                },
                error: function (xhr, status, error) {
                    clearTimeout(CallTimeOut);
                    if (showDefaultErrorAlert) {
                        if (status == "timeout" || error == "timeout") {
                            AlertFunction("Connection timeout", "Connection timeout", "Error", "خطأ", "OK", "موافق");
                        }
                        else {
                            AlertFunction("please make sure that the device is connected to the internet", "من فضلك تأكد ان الجهاز متصل بالانترنت", "Error", "خطأ", "OK", "موافق")
                        }
                    }
                    eval(FailureFunction);
                    //if (HideSpinner) {
                        HideLoadingSpinner();
                    //}
                    Log(error, "error");
                    Log(status, "status");
                    Log(xhr, "xhr")
                }
            })
} catch (e) { }
} catch (e) {
    Log(e, "CallWebService")
}
}

function GetOAuthURL(I_url) {
    try {
        var mConsumerKey = "6eGR8BzQ0hgjDJDhUuiGBOCLzb";// jsinterface.GetLookup("0");
        var mConsumerSecret = "7phquMfsANaNvUUqHF6w";//jsinterface.GetLookup("1");
        var mOAuthSignatureMethod = "HMAC-SHA1";//jsinterface.GetLookup("2");
        var mOAuthVersion = "1.0";//jsinterface.GetLookup("3");
        var mNormalizedUrl = I_url;

        var accessor =
        {
          consumerSecret: mConsumerSecret,
          tokenSecret: null,
          serviceProvider:
          {
              accessTokenURL: mNormalizedUrl
          },
          consumerKey: mConsumerKey
      };

      var message =
      {
        action: accessor.serviceProvider.accessTokenURL,
        method: "GET",
        parameters: []
    };
    message.parameters.push(["oauth_consumer_key", mConsumerKey]);
    message.parameters.push(["oauth_signature_method", mOAuthSignatureMethod]);
    message.parameters.push(["oauth_version", mOAuthVersion]);
    message.parameters.push(["oauth_timestamp", ""]);
    message.parameters.push(["oauth_nonce", ""]);
    message.parameters.push(["oauth_signature", ""]);

    OAuth.setTimestampAndNonce(message);
    OAuth.SignatureMethod.sign(message, accessor);

    var url = mNormalizedUrl + "?oauth_consumer_key={0}&oauth_signature_method={1}&oauth_timestamp={2}&oauth_nonce={3}&oauth_version={4}&oauth_signature={5}";
    url = url.replace("{0}", mConsumerKey);
    url = url.replace("{1}", mOAuthSignatureMethod);
    url = url.replace("{2}", message.parameters[3][1]);
    url = url.replace("{3}", message.parameters[4][1]);
    url = url.replace("{4}", mOAuthVersion);
    url = url.replace("{5}", message.parameters[5][1]);

    return url;
} catch (e) {
    Log(e);
}
}

function CallWebService(url, SuccessFunction, FailureFunction, Data, Type, ShowSpinner, HideSpinner, showDefaultErrorAlert, timeout) {
    try {

        CallWebServiceWithOauth(url, SuccessFunction, FailureFunction, Data, Type, ShowSpinner, HideSpinner, showDefaultErrorAlert, timeout);

        //var ProcessData = true;
        //var DataType = "json";

        //if (ShowSpinner == undefined || ShowSpinner == null) {
        //    ShowSpinner = false
        //}
        //if (HideSpinner == undefined || HideSpinner == null) {
        //    HideSpinner = false
        //}
        //if (showDefaultErrorAlert == undefined || showDefaultErrorAlert == null) {
        //    showDefaultErrorAlert = true
        //}
        //if (ShowSpinner) {
        //    ShowLoadingSpinner()
        //}
        //if (Data == undefined || Data == "") {
        //    Data = '{}'
        //}
        //if (Type == undefined || Type == "") {
        //    Type = 'GET';
        //    ProcessData = false;
        //    DataType = "TEXT"
        //}

        //if (!timeout) {
        //    timeout = 60000;
        //}

        //$.ajax({
        //    type: Type,
        //    url: url,
        //    contentType: "application/json",
        //    dataType: DataType,
        //    processData: ProcessData,
        //    data: Data,
        //    timeout: timeout,
        //    success: function (response) {
        //        Log(response, "ajax Response");
        //        eval(SuccessFunction);
        //        if (HideSpinner) {
        //            HideLoadingSpinner()
        //        }
        //    },
        //    error: function (xhr, status, error) {
        //        if (showDefaultErrorAlert) {
        //            HideLoadingSpinner();
        //            AlertFunction("please make sure that the device is connected to the internet", "من فضلك تأكد ان الجهاز متصل بالانترنت", "Error", "خطأ", "OK", "موافق")
        //        }
        //        eval(FailureFunction);
        //        //if (HideSpinner) {
        //        HideLoadingSpinner()
        //        //}
        //        Log(error, "error");
        //        Log(status, "status");
        //        Log(xhr, "xhr")
        //    }
        //})

} catch (e) {
    Log(e, "CallWebService")
}
}

function getOS() {
    try {
        var ua = navigator.userAgent;
        var uaindex;
        if (ua.match(/iPad/i) || ua.match(/iPhone/i)) {
            mobileOS = 'iOS';
            uaindex = ua.indexOf('OS ')
        } else if (ua.match(/Android/i)) {
            mobileOS = 'Android';
            uaindex = ua.indexOf('Android ')
        } else {
            mobileOS = 'BlackBerry'
        }
        if (mobileOS === 'iOS' && uaindex > -1) {
            mobileOSver = ua.substr(uaindex + 3, 3).replace('_', '.')
        } else if (mobileOS === 'Android' && uaindex > -1) {
            mobileOSver = ua.substr(uaindex + 8, 3)
        } else {
            mobileOSver = 'BlackBerry'
        }
    } catch (e) {
        Log(e, "getOS")
    }
}

function EnbleWindowScroll() {
    $('html').css('overflow', 'auto');
    $(document).unbind("touchmove")
}

function DisableWindowScroll() {
    $('html').css('overflow', 'hidden');
    $(document).bind("touchmove", function (event) {
        event.preventDefault()
    })
}

function AlertFunction(textEN, textAR, titelEN, titelAR, OK_Tetx_EN, OK_Text_AR, CallBAckFunction) {
    try {
        jAlert(Language == "en" ? textEN : textAR, Language == "en" ? titelEN : titelAR, "", Language == "en" ? OK_Tetx_EN : OK_Text_AR, CallBAckFunction);
        $('#popup_overlay').bind('click', function () {
            return
        })
    } catch (e) { }
}

function ConfirmationAlert(textEN, textAR, titelEN, titelAR, OK_Tetx_EN, OK_Text_AR, Cancel_Text_EN, Cancel_Text_AR, CallBAckFunction) {
    try {
        jConfirm(Language == "en" ? textEN : textAR, Language == "en" ? titelEN : titelAR, true, Language == "en" ? OK_Tetx_EN : OK_Text_AR, Language == "en" ? Cancel_Text_EN : Cancel_Text_AR, CallBAckFunction);
        $('#popup_overlay').bind('click', function () {
            return
        })
    } catch (e) { }
}

function ShowLoadingSpinner() {
    try {
        $("#LoadingSpinner").show();
        NoofShownSpinner += 1;
        //window.plugins.spinnerDialog.show(null, null, true);
    } catch (e) {

    }
}

function HideLoadingSpinner() {
    try {

        $("#LoadingSpinner").hide();

        //if (localStorage.getItem("Lang") == "en" || localStorage.getItem("Lang") == "ar") {
        //    NoofShownSpinner += 1;
        //}

        //if (NoofShownSpinner > 0) {
        //    //  window.plugins.spinnerDialog.hide();
        //    NoofShownSpinner -= 1;
        //    HideLoadingSpinner();
        //}
    } catch (e) {
        //alert(e);
    }
}

function isNumberWithDot(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        return false
    }
    return true
}

function isNumberWithDash(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 45) {
        return false
    }
    return true
}

function isNumber(evt) {
    Log(evt);
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false
    }
    return true
}

function isNumeric(inputText) {
    try {
        var numberformat = /^\-{0,1}(?:[0-9-]+){0,1}(?:\.[0-9-]+){0,1}$/i;
        if (inputText.value.length > 0) {
            if (!inputText.value.match(numberformat)) {
                AlertFunction("Numbers only!", "أرقام فقط!", "Error", "خطأ ", "Ok", "موافق", "");
                inputText.value = ""
            }
        }
    } catch (e) { }
}

function CleanPage(PagesIDs) {
    try {
        var temp = PagesIDs.split(',');
        for (var i = 0; i < temp.length; i++) {
            try {
                $("#" + temp[i] + " input:not(:button)").val("")
            } catch (e) { }
            try {
                $("#" + temp[i] + " textarea").val("")
            } catch (e) { }
            try {
                $("#" + temp[i] + " select").val("-1").trigger("change")
            } catch (e) { }
        }
    } catch (e) { }
}

function HandleInputsFn() {
    $(document).on('focus', 'input:not(:checkbox):not(:button),select,textarea', function () {
        $('.HeaderClass').css('position', 'absolute')
    });
    $(document).on('blur', 'input:not(:checkbox):not(:button),select,textarea', function () {
        window.scrollTo($.mobile.window.scrollLeft(), $.mobile.window.scrollTop());
        $('.HeaderClass').css('position', 'fixed')
    })
}

function ConvertDateToUNIX(date) {
    var dateObj = new Date(date);
    return dateObj.getTime() / 1000
}

function GetUploadedDocsSize(InputID) {
    var fileUpload = $("#" + InputID).get(0);
    var files = fileUpload.files;
    var Filessize = 0;
    for (var i = 0; i < files.length; i++) {
        if (files[i].size / 1024 / 1024 > 7) {
            return false
        }
    }
    return true
}

var guid = (function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
    }
    return function () {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4()
    }
})();

function UploadFiles(InputID, FileUDID, SuccessFunction, FailureFunction, OneFile) {
    try {
        ShowLoadingSpinner();
        if (OneFile == undefined || OneFile == null) {
            OneFile = true
        }
        var name;
        var fileUpload;
        var imagefile;
        var formData;
        if (OneFile) {
            fileUpload = $("#" + InputID).get(0);
            imagefile = fileUpload.files[0];
            formData = new FormData();
            var Tempname = imagefile.name.split('.');
            name = FileUDID + "." + Tempname[Tempname.length - 1];
            formData.append("fileUploaded", imagefile);
            formData.append("fileName", name);
            formData.append("language", Language)
        } else {
            try {
                fileUpload = $("#" + InputID).get(0);
                imagefile = fileUpload.files;
                name = new Array();
                formData = new FormData();
                for (var i = 0; i < imagefile.length; i++) {
                    var Tempname = imagefile[i].name.split('.');
                    name[i] = FileUDID[i] + "." + Tempname[Tempname.length - 1];
                    formData.append("fileUploaded", imagefile[i]);
                    formData.append("fileName", name[i]);
                    formData.append("language", Language)
                }
            } catch (e) { }
        }
        $.ajax({
            url: GetOAuthURL(Globals.UploadServiceURI),
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (data, textStatus, jqXHR) {
                AlertFunction(data.Message.Body, data.Message.Body, "Success", "نجاح", "OK", "موافق");
                eval(SuccessFunction);
                HideLoadingSpinner()
            },
            error: function (jqXHR, textStatus, errorThrown) {
                AlertFunction("Error: " + textStatus, "Error: " + textStatus, "Error", "خطأ", "OK", "موافق");
                eval(FailureFunction);
                HideLoadingSpinner()
            }
        })
    } catch (e) {
        alert(e);
        HideLoadingSpinner();
    }
}

function ValidateEmail(email) {
    var check = /^[\w\.\+-]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,6}$/;
    if (!check.test(email)) {
        return false
    }
    return true
}

function ReturnImageSrc(String) {
    var m, urls = [],
    str = String,
    rex = /<img.*?src="([^">]*\/([^">]*?))".*?>/g;
    while (m = rex.exec(str)) {
        urls.push(m[1])
    }
    return urls
}

function replaceAll(txt, replace, with_this) {
    if (txt == null) {
        txt = ""
    }
    return txt.replace(new RegExp(replace, 'g'), with_this)
}

function ValidateBirthDate(date) {
    var CurentDate = new Date();
    var CurentUnixDate = (CurentDate.getTime() / 1000);
    var BirthDateUnix = ConvertDateToUNIX(date);
    if (BirthDateUnix >= CurentUnixDate) {
        return false
    } else {
        return true
    }
}

function ValidatePassLength(Password) {
    if (Password.length < 7) {
        return true
    } else {
        return false
    }
}

function isArabic(inputText) {
    try {
        var Arabicformat = /[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FF]/;
        if (inputText.value.length > 0) {
            if (!inputText.value.match(Arabicformat)) {
                AlertFunction("Arabic only!", "عربى فقط!", "Error", "خطأ ", "Ok", "موافق", "");
                inputText.value = ""
            }
        }
    } catch (e) { }
}

function FormatChar(value) {
    value = value.replace(/\D/g, '');
    return value
}

function ConvertToHtml(content, id) {
    $('#' + id).html(content);
}

function changeToPassword(id) {
    document.getElementById(id).setAttribute("type", "number");
    setTimeout(function () {
        document.getElementById(id).setAttribute("type", "password")
    }, 0);
}

function changeToNumber(id) {
    setTimeout(function () {
        document.getElementById(id).setAttribute("type", "number")
    }, 500);
}

function ConvertUNIXToDate(unix_timestamp) {
    var date = new Date(unix_timestamp * 1000);
    return formatDateToddDDMMYYYY(date, Language);
}

function formatDateToddDDMMYYYY(inputFormat, lang) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat);
    var year = d.getFullYear();
    var month = pad(d.getMonth() + 1);
    var day = pad(d.getDate());
    if (lang == 'ar') {
        var weekdays = ["الأحد", "الاثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت"];
        var Months = ["يناير", "فبراير", "مارس", "إبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "يوليو"];
    } else if (lang == 'en') {

        var weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var Months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    }
    var weekday = weekdays[d.getDay()];
    inputFormat = weekday + ' ' + day + '-' + Months[month - 1] + '-' + year;
    return inputFormat;
}

function SortArray(Array, SortAttr) {
    Array.sort(function (Obj1, Obj2) {
        if (Obj1[SortAttr] > Obj2[SortAttr])
            return 1;
        else if (Obj1[SortAttr] < Obj2[SortAttr])
            return -1;
        else
            return 0;
    });
    return Array;
}

function isUndefinedOrNullOrBlank(v) {
    try {
        if (v != null)
            v = v.trim();

        if (v == undefined || v == "undefined" || v == null || v == "")
            result = true;
        else
            result = false;
        return result;
    } catch (e) {
        return false;
    }
}

function HandleBackButton() {

    var functionStr = WidgetsArr.pop();
    if (functionStr != undefined && functionStr != "") {
        eval(functionStr);
    }
    else {
        CloseMenu();
        ConfirmationAlert("Are you sure you want to quit the application ?", "هل انت متأكد من انك تريد الخروج من البرنامج؟", "Confirm", "تأكيد", "Quit", "خروج", "Cancel", "إلغاء", "navigator.app.exitApp();");
    }
    HideLoadingSpinner();
}

// Since the '+' symbol's decimal ASCII code is 43, you can add it to your condition
function isNumberWithPlus(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}



var DeviceHeight = $(window).height();
var is_keyboardOpen = false;
window.addEventListener("resize", function () {
    is_keyboardOpen = (window.innerHeight < DeviceHeight);
    if (is_keyboardOpen) {
        $('#FooterDiv').css('display', 'none');
        //setTimeout(function () {
        //    $('#footer').css('display', 'none');
        //}, 105);
} else {
    setTimeout(function () {
        window.scrollTo($.mobile.window.scrollLeft(), $.mobile.window.scrollTop());
        $('#FooterDiv').css('display', 'block');
        setTimeout(function () {
                //$('#FooterDiv').css('position', 'fixed');
            }, 20);
    }, 20);
}
}, false);

function getPhoto() {
    try {
        navigator.camera.getPicture(onPhotoDataSuccess, ongetPhotoFail,
        {
            quality: 30,
                //destinationType: Camera.DestinationType.DATA_URL,
                destinationType: Camera.DestinationType.FILE_URI,
                //sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 100,
                targetHeight: 100
            });
    } catch (e) {
        alert("Func: getPhoto Excep, " + e);
    }
}

function onPhotoDataSuccess(imageData) {
    try {
        //var image = document.getElementById('ImgTemp');
        //image.src = "data:image/jpeg;base64," + imageData;
        //alert("image.src >: " + image.src);
        // alert(imageData)
        var uuid = guid();
        //UploadImage(imageData, uuid, "", "");
        onCapturePhoto(imageData);
    } catch (e) {
        alert("Func: onPhotoDataSuccess Excep, " + e);
    }
}

function ongetPhotoFail(message) {
    alert('Func. ongetPhotoFail Excep: ' + message);
}

function UploadImage(imagefile, FileUDID, SuccessFunction, FailureFunction) {
    try {

        ShowLoadingSpinner();

        var name;
        var fileUpload;
        var formData;


        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = imagefile.substr(imagefile.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";
        options.params = {}; // if we need to send parameters to the server request
        var URL = GetOAuthURL(Globals.UploadServiceURI);
        var ft = new FileTransfer();
        ft.upload(imagefile, encodeURI(URL), win, fail, options);



        //formData = new FormData();

        //name = FileUDID + ".jpg";
        ////alert("name " + name);
        ////alert(imagefile);
        //formData.append("fileUploaded", imagefile);
        //formData.append("fileName", name);
        //formData.append("language", Language)

        //$.ajax({
        //    url: GetOAuthURL(Globals.UploadServiceURI),
        //    type: 'POST',
        //    data: formData,
        //    cache: false,
        //    dataType: 'json',
        //    processData: false,
        //    contentType: false,
        //    success: function (data, textStatus, jqXHR) {
        //        AlertFunction(data.Message.Body, data.Message.Body, "Success", "نجاح", "OK", "موافق");
        //        eval(SuccessFunction);
        //        HideLoadingSpinner()
        //    },
        //    error: function (jqXHR, textStatus, errorThrown) {
        //        AlertFunction("Error: " + textStatus + " errorThrown - " + errorThrown, "Error: " + textStatus, "Error", "خطأ", "OK", "موافق");
        //        eval(FailureFunction);
        //        HideLoadingSpinner()
        //    }
        //})
} catch (e) {
    HideLoadingSpinner()
}
}



function clearCache() {
    navigator.camera.cleanup();
}

var retries = 0;
function onCapturePhoto(fileURI) {

    ShowLoadingSpinner();

    var win = function (r) {
        clearCache();
        retries = 0;
        //image uploaded succsfuly
        //alert & hide spinner
        alert('Image uploaded');
        // AlertFunction(r.response.Message.Body, r.response.Message.Body, "Success", "نجاح", "OK", "موافق");
        // eval(SuccessFunction);
        HideLoadingSpinner();
    }

    var fail = function (error) {
        if (retries == 0) {
            retries++
            setTimeout(function () {
                onCapturePhoto(fileURI)
            }, 1000)
        } else {
            retries = 0;
            clearCache();
            //image failed 
             //alert & hide spinner
            alert('Ups. Something wrong happens!');
            HideLoadingSpinner();

        }
    }
    var uuid = guid();
    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.fileName = uuid + ".jpg";
    options.mimeType = "image/jpeg";
    options.params = {
        type: 'POST',
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        fileName : uuid + ".jpg"
    }; // if we need to send parameters to the server request
    var ft = new FileTransfer();
    var URL = GetOAuthURL(Globals.UploadServiceURI);
    ft.upload(fileURI, encodeURI(URL), win, fail, options);
}

function capturePhoto() {
    try {
        var pictureSource = navigator.Camera.PictureSourceType.CAMERA; // Cannot read PictureSourceType of undef

        var _destinationType = navigator.camera.DestinationType.FILE_URI
        //alert(navigator.camera)

        console.log(navigator.camera);
        navigator.camera.getPicture(onCapturePhoto, onFail, {
            quality: 100,
            destinationType: _destinationType
        });
    } catch (e) {
        //alert('capturePhoto' + e);
    }
}

function onFail(message) {
    //alert('Failed because: ' + message);
}

function CatchError(e, caller) {
    Log(e, caller)
    HideLoadingSpinner();
}
