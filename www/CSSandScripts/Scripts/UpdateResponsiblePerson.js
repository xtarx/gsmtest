﻿var UpdateResponsiblePerson = {
    GPSSAUSersResp: null,
    GPSSADetailsUserResponse: null,
    GPSAADeletedUser: null,
    isFromServices: false,

    LoadUpdateResponsiblePersonPage: function (isFromServices) {
        UpdateResponsiblePerson.isFromServices = isFromServices;
        var URL = Globals.ServicesURI_Test + "get/gpssausers/employerrowid/#EmployerRowID#/language/#Lang#";
        URL = URL.replace("#EmployerRowID#", Globals.EmployerRowID).replace("#Lang#", Language);
        CallWebService(URL, 'UpdateResponsiblePerson.HandleGetGPSSAUsersCallSuccess(response)', "", "", "", true, false, true, 300000);
        //UpdateResponsiblePerson.HandleGetGPSSAUsersCallSuccess();
    },

    HandleGetGPSSAUsersCallSuccess: function (response) {

        //    var response = '{ "CaseId": null, "CaseNumber": null, "Message": { "Body": null, "Code": 0 }, "GPSSAUsers": [{ "AllowVarianceSheetUpload": false, "BirthDate": "1382040000", "ContactRowId": "1-13BOGV", "DefaultSearchCriteria": "", "DeviceToken": "", "Email": "a.shihata@hotmail.com", "EmailVerified": false, "EmiratesId": "784765754096932", "EmployerRowId": "1-1RW-100", "Fax": null, "FirstName": "أحمد", "FourthName": "سالم", "FullName": "أحمد على صلاح الدين سالم", "GPSSAServices": [{ "Header": { "AttachedFiles": [], "ContactRowId": null, "EmployerRowId": null, "FullName": null, "UserName": null, "UserType": null }, "Id": "1", "Name": "Register New Employer", "NameAr": "تسجيل صاحب عمل جديد", "ServiceUri": "RegisterEmployer.aspx", "SubType": null, "Type": null, "UserTypes": null }, { "Header": { "AttachedFiles": [], "ContactRowId": null, "EmployerRowId": null, "FullName": null, "UserName": null, "UserType": null }, "Id": "2", "Name": "New user registration", "NameAr": "تسجيل مستخدم جديد", "ServiceUri": "AutoRegisterExistingEmployer.aspx", "SubType": null, "Type": null, "UserTypes": null }, { "Header": { "AttachedFiles": [], "ContactRowId": null, "EmployerRowId": null, "FullName": null, "UserName": null, "UserType": null }, "Id": "3", "Name": "Pension Calculator", "NameAr": "حسبة المعاش التقاعدي اليدوي", "ServiceUri": "PensionCalculator.aspx", "SubType": null, "Type": null, "UserTypes": null }, { "Header": { "AttachedFiles": [], "ContactRowId": null, "EmployerRowId": null, "FullName": null, "UserName": null, "UserType": null }, "Id": "4", "Name": "Update Employer’s Details", "NameAr": "تحديث بيانات صاحب العمل", "ServiceUri": "UpdateEmployerDetails.aspx", "SubType": null, "Type": null, "UserTypes": null }, { "Header": { "AttachedFiles": [], "ContactRowId": null, "EmployerRowId": null, "FullName": null, "UserName": null, "UserType": null }, "Id": "5", "Name": "View Insured Employees", "NameAr": "عرض الموظفين المؤمن عليهم", "ServiceUri": "InsuredEmployees.aspx", "SubType": null, "Type": null, "UserTypes": null }, { "Header": { "AttachedFiles": [], "ContactRowId": null, "EmployerRowId": null, "FullName": null, "UserName": null, "UserType": null }, "Id": "6", "Name": "Manage Responsible Persons", "NameAr": "إدارة الأشخاص المسئولين", "ServiceUri": "ManageResponsible.aspx", "SubType": null, "Type": null, "UserTypes": null }, { "Header": { "AttachedFiles": [], "ContactRowId": null, "EmployerRowId": null, "FullName": null, "UserName": null, "UserType": null }, "Id": "7", "Name": "View Contribution Sheet", "NameAr": "عرض كشف المستحقات", "ServiceUri": "ContributionSheet.aspx", "SubType": null, "Type": null, "UserTypes": null }, { "Header": { "AttachedFiles": [], "ContactRowId": null, "EmployerRowId": null, "FullName": null, "UserName": null, "UserType": null }, "Id": "8", "Name": "Add Responsible Person", "NameAr": "إضافة شخص مسئول", "ServiceUri": "ResponsiblePerson.aspx ", "SubType": null, "Type": null, "UserTypes": null }, { "Header": { "AttachedFiles": [], "ContactRowId": null, "EmployerRowId": null, "FullName": null, "UserName": null, "UserType": null }, "Id": "9", "Name": "Complaints, Suggestions", "NameAr": "الشكاوى والمقترحات", "ServiceUri": "Feedback.aspx ", "SubType": null, "Type": null, "UserTypes": null }], "IsPrimaryResponsible": true, "IsResponsible": false, "LandLine": "", "Mobile": "0526467393", "Password": null, "PinCode": "1234", "PreferredContactMethod": "Mobile", "PreferredLanguage": "English", "Roles": ["Employer"], "SecondName": "على", "ThirdName": "صلاح الدين", "UserId": "1f641a1b-4efc-4dac-8be6-e18b1ef87423", "UserName": "newgpssauser4" },{ "AllowVarianceSheetUpload": false, "BirthDate": "1098216000", "ContactRowId": "1-13DHJD", "DefaultSearchCriteria": "", "DeviceToken": "", "Email": "test1000@990.com", "EmailVerified": false, "EmiratesId": "784333333332568", "EmployerRowId": "1-1RW-100", "Fax": null, "FirstName": "يس", "FourthName": "يب", "FullName": "يس بي بي يب", "GPSSAServices": [{ "Header": { "AttachedFiles": [], "ContactRowId": null, "EmployerRowId": null, "FullName": null, "UserName": null, "UserType": null }, "Id": "1", "Name": "Register New Employer", "NameAr": "تسجيل صاحب عمل جديد", "ServiceUri": "RegisterEmployer.aspx", "SubType": null, "Type": null, "UserTypes": null }, { "Header": { "AttachedFiles": [], "ContactRowId": null, "EmployerRowId": null, "FullName": null, "UserName": null, "UserType": null }, "Id": "2", "Name": "New user registration", "NameAr": "تسجيل مستخدم جديد", "ServiceUri": "AutoRegisterExistingEmployer.aspx", "SubType": null, "Type": null, "UserTypes": null }, { "Header": { "AttachedFiles": [], "ContactRowId": null, "EmployerRowId": null, "FullName": null, "UserName": null, "UserType": null }, "Id": "3", "Name": "Pension Calculator", "NameAr": "حسبة المعاش التقاعدي اليدوي", "ServiceUri": "PensionCalculator.aspx", "SubType": null, "Type": null, "UserTypes": null }, { "Header": { "AttachedFiles": [], "ContactRowId": null, "EmployerRowId": null, "FullName": null, "UserName": null, "UserType": null }, "Id": "4", "Name": "Update Employer’s Details", "NameAr": "تحديث بيانات صاحب العمل", "ServiceUri": "UpdateEmployerDetails.aspx", "SubType": null, "Type": null, "UserTypes": null }, { "Header": { "AttachedFiles": [], "ContactRowId": null, "EmployerRowId": null, "FullName": null, "UserName": null, "UserType": null }, "Id": "8", "Name": "Add Responsible Person", "NameAr": "إضافة شخص مسئول", "ServiceUri": "ResponsiblePerson.aspx ", "SubType": null, "Type": null, "UserTypes": null }], "IsPrimaryResponsible": false, "IsResponsible": true, "LandLine": "", "Mobile": "0564654564", "Password": null, "PinCode": "", "PreferredContactMethod": "Email", "PreferredLanguage": "en", "Roles": ["Employer"], "SecondName": "بي", "ThirdName": "بي", "UserId": "7c8e092c-9ff2-4add-9d0b-b462134c890a", "UserName": "sania.ahm" }], "RecordSetTotalCount": 20 }';

        if (response != null) {
            Log(response);
            var respObj = response;//JSON.parse(response);
            if (respObj.Message.Code == 0) {
                UpdateResponsiblePerson.GPSSAUSersResp = respObj.GPSSAUsers;
                if (UpdateResponsiblePerson.isFromServices == true)
                {
                    LoadPageContent("UpdateResponsiblePerson", 'UpdateResponsiblePerson.HandleUpdateResponsiblePersonDocReady()', false, true, true, false, 'ServicesList.LoadServicesPage()');
                }
                else
                {
                    LoadPageContent("UpdateResponsiblePerson", 'UpdateResponsiblePerson.HandleUpdateResponsiblePersonDocReady()', false, true);
                }
            }
            else {
                HideLoadingSpinner();
                AlertFunction(respObj.Message.Body, respObj.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        }
    },

    HandleUpdateResponsiblePersonDocReady: function () {
        SetHeaderTitle("Update Responsible Person", "تحديث شخص مسئول");
        $("#ManageResponsiblePersonTemplate").tmpl(UpdateResponsiblePerson.GPSSAUSersResp).appendTo("#ManageResponsibleContent")

        //BindEvents('#UpdateResponsiblePerson_Persons', 'change', 'UpdateResponsiblePerson.HandlePersonsDropDownChanged()');
        //BindEvents('#UpdateResponsiblePerson_UpdateBtn', 'click', 'UpdateResponsiblePerson.HandleBtnUpdateClicked()');
        //BindEvents('#UpdateResponsiblePerson_DeleteBtn', 'click', 'UpdateResponsiblePerson.HandleBtnDeleteClicked()');

        //if (Language == "en") {
        //    $("#UpdateResponsiblePerson_Persons").html("<option value='' disabled selected>Select Person</option>");
        //}
        //else {
        //    $("#UpdateResponsiblePerson_Persons").html("<option value='' disabled selected>اختر شخص</option>");
        //}
        //$("#UpdateResponsiblePerson_UsersDropDownTemp").tmpl(UpdateResponsiblePerson.GPSSAUSersResp).appendTo("#UpdateResponsiblePerson_Persons");
        //$("#UpdateResponsiblePerson_EmiratesID").mask("784-9999-9999999-9");
        //$("#UpdateResponsiblePerson_MobileNumber").mask("059-9999999");
        //$("#UpdateResponsiblePerson_LandLine").mask("09-9999999");
        //$("#UpdateResponsiblePerson_Fax").mask("09-9999999");
    },

    OpenPersonDetails: function (id) {
        $(".UpdateResTable").hide();
        $(".ServiceNameClasses").hide();
        $(".ServicesTable").hide();
        $("#" + id + " .UpdateResTable").show();
        $("#" + id + " .ServiceNameClasses").show();
        $("#" + id + " .ServicesTable").show();
       // $("#" + id + " .ServicesTable tr:last").remove();
    },

    HandlePersonsDropDownChanged: function () {
        var val = $("#UpdateResponsiblePerson_Persons").val();
        if (val == -1) {
            CleanPage("UpdateResponsiblePerson");
        }
        else {
            var URL = Globals.ServicesURI_Test + "get/gpssauser/contactrowid/#RowID#/language/#Lang#";
            URL = URL.replace("#RowID#", val).replace("#Lang#", Language);
            CallWebService(URL, 'UpdateResponsiblePerson.HandleGetGPSSAUsersDetailsCallSuccess(response)', "", "", "", true, true);
            //for (var i = 0; i < UpdateResponsiblePerson.GPSSAUSersResp.length; i++) {
            //    if (UpdateResponsiblePerson.GPSSAUSersResp[i].UserName == val) {
            //        UpdateResponsiblePerson.GPSSADetailsUserResponse = UpdateResponsiblePerson.GPSSAUSersResp[i];
            //        break;
            //    }
            //}


        }
    },

    HandleGetGPSSAUsersDetailsCallSuccess: function (response) {
        if (response != null) {
            var DetailsRespObj = response;//JSON.parse(response);
            if (DetailsRespObj.Message.Code == 0) {
                var Rsp = DetailsRespObj.GPSSAUser;
                $("#UpdateResponsiblePerson_EmiratesID").val(Rsp.EmiratesId);
                $("#UpdateResponsiblePerson_FirstName").val(Rsp.FirstName);
                $("#UpdateResponsiblePerson_SecondName").val(Rsp.SecondName);
                $("#UpdateResponsiblePerson_ThirdName").val(Rsp.ThirdName);
                $("#UpdateResponsiblePerson_ForthName").val(Rsp.FourthName);
                $("#UpdateResponsiblePerson_MobileNumber").val(Rsp.Mobile);
                $("#UpdateResponsiblePerson_Email").val(Rsp.Email);
                $("#UpdateResponsiblePerson_Username").val(Rsp.UserName);
                $("#UpdateResponsiblePerson_LandLine").val(Rsp.Landline);
                $("#UpdateResponsiblePerson_Fax").val(Rsp.Fax);
                $("#UpdateResponsiblePerson_Language").val(Rsp.PreferredLanguage).trigger('change');

                $("#UpdateResponsiblePerson_ValuesTemp").tmpl(Rsp.GPSSAServices).appendTo("#UpdateResponsiblePerson_ListOfValues");
                //$("#UpdateResponsiblePerson_ValuesTemp").tmpl(Globals.UserServicesArr.GPSSAServices).appendTo("#UpdateResponsiblePerson_ListOfValues");

                //for (var i = 0; i < Rsp.GPSSAServices.length; i++) {
                //    $("#UpdateResponsiblePerson_ListOfValues").val(Rsp.GPSSAServices[i].Id).attr('selected', 'selected');
                //}
            }
            else {
                AlertFunction(DetailsRespObj.Message.Body, DetailsRespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        }
    },

    HandleBtnUpdateClicked: function () {

        var val = $("#UpdateResponsiblePerson_Persons").val();

        if (val == -1) {
            AlertFunction("Please select Responsible Person", "من فضلك اختر شخص مسئول", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#UpdateResponsiblePerson_EmiratesID").val()) == "") {
            AlertFunction("Please enter Emirates ID", "من فضلك ادخل رقم الهوية", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#UpdateResponsiblePerson_FirstName").val()) == "") {
            AlertFunction("Please enter First Name", "من فضلك ادخل الاسم الاول", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#UpdateResponsiblePerson_SecondName").val()) == "") {
            AlertFunction("Please enter Second Name", "من فضلك ادخل الاسم الثاني", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#UpdateResponsiblePerson_ThirdName").val()) == "") {
            AlertFunction("Please enter Third Name", "من فضلك ادخل الاسم الثالث", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#UpdateResponsiblePerson_ForthName").val()) == "") {
            AlertFunction("Please enter Third Name", "من فضلك ادخل الاسم الرابع", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#UpdateResponsiblePerson_MobileNumber").val()) == "") {
            AlertFunction("Please enter Mobile Number", "من فضلك ادخل رقم الهاتف المتحرك", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#UpdateResponsiblePerson_Email").val()) == "") {
            AlertFunction("Please enter E-mail", "من فضلك ادخل رقم البريد الالكتروني", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#UpdateResponsiblePerson_Username").val()) == "") {
            AlertFunction("Please enter Username", "من فضلك ادخل رقم اسم المستخدم", "Error", "خطأ", "OK", "موافق");
            return;
        }

        // Update Call

        var GPSAAUserObj = GPSSAUser2;
        GPSAAUserObj.FirstName = $("#UpdateResponsiblePerson_FirstName").val();
        GPSAAUserObj.SecondName = $("#UpdateResponsiblePerson_SecondName").val();
        GPSAAUserObj.ThirdName = $("#UpdateResponsiblePerson_ThirdName").val();
        GPSAAUserObj.FourthName = $("#UpdateResponsiblePerson_ForthName").val();
        GPSAAUserObj.EmiratesId = FormatChar($("#UpdateResponsiblePerson_EmiratesID").val());
        GPSAAUserObj.Mobile = FormatChar($("#UpdateResponsiblePerson_MobileNumber").val());
        GPSAAUserObj.Email = $("#UpdateResponsiblePerson_Email").val();
        GPSAAUserObj.Landline = FormatChar($("#UpdateResponsiblePerson_LandLine").val());
        GPSAAUserObj.UserName = $("#UpdateResponsiblePerson_Username").val();
        GPSAAUserObj.Fax = FormatChar($("#UpdateResponsiblePerson_Fax").val());

        GPSAAUserObj.GPSSAServices = new Array();

        try {
            var ServicesIDArr = $("#UpdateResponsiblePerson_ListOfValues").val();
            for (var i = 0; i < ServicesIDArr.length; i++) {
                for (var j = 0; j < Globals.UserServicesArr.GPSSAServices.length; j++) {
                    if (ServicesIDArr[i] == Globals.UserServicesArr.GPSSAServices[j].Id) {
                        GPSAAUserObj.GPSSAServices[i] = Globals.UserServicesArr.GPSSAServices[j];
                        break;
                    }
                }
            }
        } catch (e) {
        }

        var data = JSON.stringify(GPSAAUserObj);
    },

    HandleBtnDeleteClicked: function () {

        var val = $("#UpdateResponsiblePerson_Persons").val();

        if (val == -1) {
            AlertFunction("Please select Responsible Person", "من فضلك اختر شخص مسئول", "Error", "خطأ", "OK", "موافق");
            return;
        }

        for (var i = 0; i < UpdateResponsiblePerson.GPSSAUSersResp.length; i++) {
            if (UpdateResponsiblePerson.GPSSAUSersResp[i].Id == val) {
                UpdateResponsiblePerson.GPSAADeletedUser = UpdateResponsiblePerson.GPSSAUSersResp[i];
                break;
            }
        }
        ConfirmationAlert("Are you sure, you want to delete Responsble Person ?", "هل انت متأكد,انك تريد حذف الشخص المسئول؟", "Confirm", "تأكيد", "OK", "موافق", "Cancel", "إلغاء", "UpdateResponsiblePerson.ConfirmDeletePerson");
    },

    ConfirmDeletePerson: function () {
        // Delete
        var URL = Globals.ServicesURI_Test + "delete/gpssauser/username/#UserName#/language/#Lang#";
        URL = URL.replace("#UserName#", UpdateResponsiblePerson.GPSAADeletedUser.UserName).replace("#Lang#", Language);
        CallWebService(URL, 'UpdateResponsiblePerson.HandleDeleteGPSSAUsersCallSuccess(response)', "", "", "", true, true);
    },

    HandleDeleteGPSSAUsersCallSuccess: function (response) {
        if (response != null) {
            var RespObj = response;//JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                AlertFunction("The Responsible Person has been deleted", "تم حذف الشخص المسؤل", "Success", "نجاح", "OK", "موافق");
            }
            else {
                AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        }
    }
};