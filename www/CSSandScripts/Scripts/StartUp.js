﻿var StartUp = {

    LoadStartuUp: function () {
        SetHeaderTitle("GPSSA smart App", "التطبيق الذكي لهيئة المعاشات");
        LoadPageContent("StartUp", 'StartUp.HandleStartUpDocReady()', false, false, false, true);
    },

    HandleStartUpDocReady: function () {

        try {

            $("#CaseFollowUpNumber").height(DeviceHeight / 12.6);
            $("#CaseFollowUpNumber").mask("1-XXXXXXXX");
            $(".HomeIcons").height((DeviceHeight - 150 - DeviceHeight / 12.6) / 3);
            $(".HomeIcons").width((DeviceWidth - 20) / 2);
            SetHeaderTitle("GPSSA smart App", "التطبيق الذكي لهيئة المعاشات");
            BindEvents('#LangBtn', 'click', 'HandleBtnLangClicked()');
            BindEvents('#MenuIconTD', 'click', 'OpenMenu()');
            BindEvents('#SearchCaseFollowUp', 'click', 'CaseFollowUp.LoadCaseFollowUp()');
            BindEvents('#CalculationTD', 'click', 'PensionCalculation.LoadPensionCalculation()');
            BindEvents('#ServiceTD', 'click', 'ServicesList.LoadServicesPage()');
            BindEvents('#ContactUsTD', 'click', 'ContactUs.LoadContactUsPage()');
            BindEvents('#AboutGPSSATD', 'click', 'AboutGPSSA.LoadAboutGPSSAPage()');
            BindEvents('#NewsTD', 'click', 'News.LoadNewsPage()');
            BindEvents('#PensionRaulationsTD', 'click', 'PensionsGuide.LoadPensionsGuideContent()');

            CloseMenu();
            getOS();

            if (Globals.UserLoggedIn == true) {
                $("#preferenceTR").show();
                $("#preferenceTR_Space").show();
            }
            else {
                $("#preferenceTR").hide();
                $("#preferenceTR_Space").hide();
            }
        } catch (e) {
            Log(e, "StartUp.HandleStartUpDocReady()");
        }
    }
};