﻿var AddResponsiblePerson = {

    CaseNumber: 0,
    IsValideUsername: false,
    IsValidEmail:false,

    LoadResponsiblePerson: function (isFromServices) {

        if (isFromServices == true) {
            LoadPageContent("AddResponsiblePerson", 'AddResponsiblePerson.HandleAddResposiblePersonDocReady()', true, true, true, false, 'ServicesList.LoadServicesPage()');
        }
        else {
            LoadPageContent("AddResponsiblePerson", 'AddResponsiblePerson.HandleAddResposiblePersonDocReady()', true, true, true);
        }
    },

    HandleAddResposiblePersonDocReady: function () {
        try {

            SetHeaderTitle("Add Responsible Person", "إضافة شخص مسئول");

            BindEvents('#AddResponsiblePerson_AddBtn', 'click', 'AddResponsiblePerson.HandleAddResponsiblePersonBtnClicked()');

            if (Language == "en") {
                $("#AddResponsiblePerson_ListOfValues").html("<option value='' disabled selected>Authorized Services</option>");
            }
            else {
                $("#AddResponsiblePerson_ListOfValues").html("<option value='' disabled selected>الخدمات المرخصة</option>");
            }

            $("#AddResponsiblePerson_ValuesTemp").tmpl(Globals.UserServicesArr.GPSSAServices).appendTo("#AddResponsiblePerson_ListOfValues");

            $('#AddResponsiblePerson_MobileNumber').mask("+XXX-XX-XXXXXXX", { autoclear: false });
            $('#AddResponsiblePerson_MobileNumber').val("+971");

            $("#AddResponsiblePerson_EmiratesID").mask("784-XXXX-XXXXXXX-X");

            $("#AddResponsiblePerson_LandLine").mask("+XXX-X-XXXXXXX", { autoclear: false });
            $('#AddResponsiblePerson_LandLine').val("+971");

            $("#AddResponsiblePerson_Fax").mask("+XXX-X-XXXXXXX", { autoclear: false });
            $("#AddResponsiblePerson_Fax").val("+971");

            $('#AddResponsiblePerson_FirstName').on('input', function (e) {
                isArabic(AddResponsiblePerson_FirstName);
            });
            $('#AddResponsiblePerson_SecondName').on('input', function (e) {
                isArabic(AddResponsiblePerson_SecondName);
            });
            $('#AddResponsiblePerson_ThirdName').on('input', function (e) {
                isArabic(AddResponsiblePerson_ThirdName);
            });
            $('#AddResponsiblePerson_ForthName').on('input', function (e) {
                isArabic(AddResponsiblePerson_ForthName);
            });
        } catch (e) {
        }
    },

    HandleAddResponsiblePersonBtnClicked: function () {
        try {

            if ($.trim($("#AddResponsiblePerson_EmiratesID").val()) == "") {
                AlertFunction("Please enter Emirates ID", "من فضلك ادخل رقم الهوية", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#AddResponsiblePerson_FirstName").val()) == "") {
                AlertFunction("Please enter First Name", "من فضلك ادخل الاسم الاول", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#AddResponsiblePerson_SecondName").val()) == "") {
                AlertFunction("Please enter Second Name", "من فضلك ادخل الاسم الثاني", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#AddResponsiblePerson_ThirdName").val()) == "") {
                AlertFunction("Please enter Third Name", "من فضلك ادخل الاسم الثالث", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#AddResponsiblePerson_ForthName").val()) == "") {
                AlertFunction("Please enter Third Name", "من فضلك ادخل الاسم الرابع", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#AddResponsiblePerson_MobileNumber").val()) == "") {
                AlertFunction("Please enter Mobile Number", "من فضلك ادخل رقم الهاتف المتحرك", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#AddResponsiblePerson_Email").val()) == "") {
                AlertFunction("Please enter E-mail", "من فضلك ادخل رقم البريد الالكتروني", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if (!ValidateEmail($("#AddResponsiblePerson_Email").val())) {
                AlertFunction("Please enter valid E-mail", "من فضلك ادخل البريد الإلكترونى صالح", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if (AddResponsiblePerson.IsValidEmail == false) {
                AlertFunction("Email already exists", "البريد الإلكترونى موجود مسبقا", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#AddResponsiblePerson_Username").val()) == "") {
                AlertFunction("Please enter Username", "من فضلك ادخل اسم المستخدم", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if (AddResponsiblePerson.IsValideUsername == false) {
                AlertFunction("Username already exists", "اسم المستخدم موجود مسبقا", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else {

                var AddResponsibleObj = GPSSAUser;
                AddResponsibleObj.EmployerRowId = Globals.EmployerRowID;
                AddResponsibleObj.IsResponsible = true;
                AddResponsibleObj.UserName = $("#AddResponsiblePerson_Username").val()
                AddResponsibleObj.Email = $("#AddResponsiblePerson_Email").val();
                AddResponsibleObj.FirstName = $("#AddResponsiblePerson_FirstName").val();
                AddResponsibleObj.SecondName = $("#AddResponsiblePerson_SecondName").val();
                AddResponsibleObj.ThirdName = $("#AddResponsiblePerson_ThirdName").val();
                AddResponsibleObj.FourthName = $("#AddResponsiblePerson_ForthName").val();
                AddResponsibleObj.EmiratesId = FormatChar($("#AddResponsiblePerson_EmiratesID").val());
                AddResponsibleObj.PreferredLanguage = $("#AddResponsiblePerson_Language").val();
                var ClearMobNum = FormatChar($("#AddResponsiblePerson_MobileNumber").val());
                AddResponsibleObj.Mobile = "+" + ClearMobNum;
                //alert("AddResponsibleObj.Mobile >>" + AddResponsibleObj.Mobile);

                var ClearLandNum = FormatChar($("#AddResponsiblePerson_LandLine").val());
                AddResponsibleObj.Landline = "+" + ClearLandNum;
                //alert("AddResponsibleObj.Landline >>" + AddResponsibleObj.Landline);

                var ClearFaxNum = FormatChar($("#AddResponsiblePerson_Fax").val());
                AddResponsibleObj.Fax = FormatChar($("#AddResponsiblePerson_Fax").val());
                //alert("AddResponsibleObj.Fax >>" + AddResponsibleObj.Fax);

                AddResponsibleObj.PreferredContactMethod = $("#AddResponsiblePerson_Contact").val();
                AddResponsibleObj.BirthDate = ConvertDateToUNIX($("#AddResponsiblePerson_BirthDate").val());
                AddResponsibleObj.GPSSAServices = new Array();

                try {
                    var ServicesIDArr = $("#AddResponsiblePerson_ListOfValues").val();
                    for (var i = 0; i < ServicesIDArr.length; i++) {
                        for (var j = 0; j < Globals.UserServicesArr.GPSSAServices.length; j++) {
                            if (ServicesIDArr[i] == Globals.UserServicesArr.GPSSAServices[j].Id) {
                                AddResponsibleObj.GPSSAServices[i] = Globals.UserServicesArr.GPSSAServices[j];
                                break;
                            }
                        }
                    }
                } catch (e) {
                }

                var URL = Globals.ServicesURI_Test + "add/gpssauser/language/#Lang#";
                URL = URL.replace("#Lang#", Language);

                var data = JSON.stringify(AddResponsibleObj);

                //var data = '{"EmployerRowId": "1-1RW-100","IsPrimaryResponsible": true,"UserName": "newgpssauser2432","Email": "a.shihata2812@hotmail.com","Password": "P@ssw0rd","FirstName": "ausdygh","SecondName": "asdw","ThirdName": "aiushd","FourthName": "www","BirthDate": 1382054411,"EmiratesId": "784315754018905"}';
                Log(data);
                CallWebServiceWithOauth(URL, "AddResponsiblePerson.HandleAddResponsiblePersonCallSuccess(response)", "", data, "POST", true, true);
            }
        } catch (e) {
        }
    },

    HandleAddResponsiblePersonCallSuccess: function (response) {
        if (response != null) {
            if (response.Message.Code == 0) {
                AddResponsiblePerson.CaseNumber = response.CaseNumber;
                LoadPageContent("AddResponsiblePersonResult", 'AddResponsiblePerson.HandleOpenAddResponsiblePersonSuccessDocReady()', true, true, true, false, "AddResponsiblePerson.LoadResponsiblePerson()");
            }
            else {
                AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        }
    },

    HandleOpenAddResponsiblePersonSuccessDocReady: function () {
        BindEvents('#ConfirmAddResponsiblePerson_BackBtn', 'click', 'ServicesList.LoadServicesPage()');
        var Temp = $("#AddResponsiblePersonResultTxt").html();
        Temp = Temp.replace("#Case#", AddResponsiblePerson.CaseNumber);
        $("#AddResponsiblePersonResultTxt").html(Temp);
    },

    HandleValidateUsername: function () {
        var URI = "http://mgovws.gpssa.ae:8080/SPREST.svc/validate/username/language/#Lang#";
        var I_username = $("#AddResponsiblePerson_Username").val();

        URI = URI.replace("#Lang#", Language);

        var data = {
            UserName: I_username
        };

        var ReqObj = JSON.stringify(data);
        CallWebService(URI, "AddResponsiblePerson.HandleUsernameValidationCallSuccess(response)", "AddResponsiblePerson.HandleUsernameValidationCallFailure()", ReqObj, "POST", true, true);
    },

    HandleUsernameValidationCallSuccess: function (response) {
        if (response != null) {
            if (response.Message.Code == 0) {
                AddResponsiblePerson.IsValideUsername = response.IsValid;
                if (response.IsValid == false) {
                    AlertFunction("Username already exists", "اسم المستخدم موجود مسبقا", "Error", "خطأ", "OK", "موافق");
                }
            }
            else {
                AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
                AddResponsiblePerson.IsValideUsername = false;
            }
        }
        else {
            AddResponsiblePerson.IsValideUsername = false;
        }
    },

    HandleUsernameValidationCallFailure: function () {
        AddResponsiblePerson.IsValideUsername = false;
    },

    ValidateEmailInDB: function (id) {

        var email = $("#" + id).val();
        var URI = "http://mgovws.gpssa.ae:8080/SPREST.svc/validate/email/language/#Lang#";
        URI = URI.replace("#Lang#", Language);

        var data = {
            Email: email
        };

        var ReqObj = JSON.stringify(data);
        CallWebService(URI, "AddResponsiblePerson.HandleValidateEmailCallSuccess(response)", "AddResponsiblePerson.HandleValidateEmailCallFailure()", ReqObj, "POST", true, true);
    },

    HandleValidateEmailCallSuccess: function (response) {
        if (response != null) {
            if (response.Message.Code == 0) {
                AddResponsiblePerson.IsValidEmail = response.IsValid;
                if (response.IsValid == false) {
                    AlertFunction("Email already exists", "البريد الإلكترونى موجود مسبقا", "Error", "خطأ", "OK", "موافق");
                }
            }
            else {
                AddResponsiblePerson.IsValidEmail = false;
                AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        }
        else {
            AddResponsiblePerson.IsValidEmail = false;
        }
    },

    HandleValidateEmailCallFailure: function () {
        AddResponsiblePerson.IsValidEmail = false;
    }
};