﻿var PensionCalculation =
{
    PensionCalResultObj: null,

    LoadPensionCalculation: function (isFromServices) {

        if (isFromServices == true) {
            LoadPageContent("PensionCalculation", 'PensionCalculation.HandleCalculatorSuccessDocReady()', true, true, true, false, 'ServicesList.LoadServicesPage()');
        }
        else {
            LoadPageContent("PensionCalculation", 'PensionCalculation.HandleCalculatorSuccessDocReady()');
        }
    },

    HandleCalculatorSuccessDocReady: function () {
        SetHeaderTitle("Manual Pension Calculation", "حساب المعاش التقاعدي");
        BindEvents('#CalculatePensionBtn', 'click', 'PensionCalculation.HandleCalculatePensionBtnClicked()');
    },

    HandleSectorChanged: function (SectorID) {

        var Val = $("#PensionCalculation_Sector").val();

        if (Val == "-1") {
            $("#PensionCalcText").html("");
        }
        else if (Val == "prv") {
            if (Language == "en") {
                $("#PensionCalcText").html("Salary Median Of the past 5 service years");
            }
            else {
                $("#PensionCalcText").html("من فضلك ادخل متوسط راتب الاشتراك لأخر 5 سنوات");
            }
        }
        else {
            if (Language == "en") {
                $("#PensionCalcText").html("Salary Median Of the past 3 service years");
            }
            else {
                $("#PensionCalcText").html("من فضلك ادخل متوسط راتب الاشتراك لأخر 3 سنوات");
            }
        }
    },

    HandleCalculatePensionBtnClicked: function () {

        if ($.trim($("#PensionCalculationAge").val()) == "") {
            AlertFunction("Please enter Age", "من فضلك ادخل السن", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($("#PensionCalculation_Sector").val() == "-1") {
            AlertFunction("Please select Sector", "من فضلك اختر قطاع الأعمال", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#PensionCalculationSalaryMedian").val()) == "") {
            AlertFunction("Please enter Salary Median", "من فضلك ادخل متوسط الراتب", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#PensionCalculationServiceYears").val()) == "") {
            AlertFunction("Please enter Service Years", "من فضلك ادخل عدد السنين", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#PensionCalculationServiceMonths").val()) == "") {
            AlertFunction("Please enter Service Months", "من فضلك ادخل عدد الشهور", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else {
            var Day = "";
            if ($.trim($("#PensionCalculationServiceDays").val()) == "") {
                Day = "0";
            }
            else {
                Day = $("#PensionCalculationServiceDays").val();
            }
            var data = {
                Age: $("#PensionCalculationAge").val(),
                Sector: $("#PensionCalculation_Sector").val(),
                SalaryMedian: $("#PensionCalculationSalaryMedian").val(),
                ServiceYears: $("#PensionCalculationServiceYears").val(),
                ServiceMonths: $("#PensionCalculationServiceMonths").val(),
                ServiceDays: Day
            };

            PensionCalculationResult.LoadPensionResult(data);
        }
    }
};

var PensionCalculationResult = {

    PensionResultData: null,

    LoadPensionResult: function (data) {
        try {
            CleanPage('PensionCalcResult');
        } catch (e) {

        }
        PensionCalculationResult.PensionResultData = data;
        var URL = Globals.ServicesURI_Test + "get/pension/age/#Age#/sector/#Sector#/salaryMedian/#salaryMedian#/serviceyears/#serviceyears#/servicemonths/#servicemonths#/servicedays/#servicedays#/language/#Lang#";
        URL = URL.replace("#Age#", PensionCalculationResult.PensionResultData.Age).replace("#Sector#", PensionCalculationResult.PensionResultData.Sector).replace("#salaryMedian#", PensionCalculationResult.PensionResultData.SalaryMedian).replace("#serviceyears#", PensionCalculationResult.PensionResultData.ServiceYears).replace("#servicemonths#", PensionCalculationResult.PensionResultData.ServiceMonths).replace("#servicedays#", PensionCalculationResult.PensionResultData.ServiceDays).replace("#Lang#", Language);

        CallWebService(URL, 'PensionCalculationResult.HandleCallPensionCalcResult(response)', "", "", "", true, true);
    },

    HandlePensionResultDocReady: function () {

        try {

            $("#AgeResult").html(PensionCalculationResult.PensionResultData.Age);

            if (Language == "en") {
                if (PensionCalculationResult.PensionResultData.Sector == "prv") {
                    $("#SectorResult").html("Private");
                }
                else {
                    $("#SectorResult").html("Government");
                }
            }
            else {
                if (PensionCalculationResult.PensionResultData.Sector == "prv") {
                    $("#SectorResult").html("خاص");
                }
                else {
                    $("#SectorResult").html("حكومى");
                }
            }

            $("#SalaryMedianResult").html(PensionCalculationResult.PensionResultData.SalaryMedian);
            $("#ServiceYearsResult").html(PensionCalculationResult.PensionResultData.ServiceYears);
            $("#ServiceMonthsResult").html(PensionCalculationResult.PensionResultData.ServiceMonths);
            $("#ServiceDaysResult").html(PensionCalculationResult.PensionResultData.ServiceDays);

            var RespObj = PensionCalculation.PensionCalResultObj;//JSON.parse(PensionCalculation.PensionCalResultObj);
            $("#ExpectedPensionSettlementResult").html(RespObj.CalculationResult);
            BindEvents('#BackPensionResultBtn', 'click', 'PensionCalculation.LoadPensionCalculation()');

        } catch (e) {
            HideLoadingSpinner();
            //alert(e);
        }
    },

    HandleCallPensionCalcResult: function (response) {
        try {

            if (response != null) {
                PensionCalculation.PensionCalResultObj = response;
                LoadPageContent("PensionCalcResult", "PensionCalculationResult.HandlePensionResultDocReady();", false, true, true, false, "PensionCalculation.LoadPensionCalculation()");
                try {
                    CleanPage('PensionCalculation');
                } catch (e) {

                }
            }
            else {
                HideLoadingSpinner();
            }
        } catch (e) {
            HideLoadingSpinner();
        }
    }
};