﻿var Help = {

    LoadHelpContent: function () {
        LoadPageContent("Help", 'Help.HandleHelpDocReady()', true, true);
    },

    HandleHelpDocReady: function () {
        SetHeaderTitle("Help", "مساعدة");
        $(".HelpImages").height($(".ContentClass").height() - 70);
        $('.flexslider').flexslider({
            startAt: 0,
            touch: true,
            slideshow: true,
            slideshowSpeed: 6000,
            after: function (slider) {
                window.curSlide = slider.currentSlide;
            }
        });
    }
};