﻿var CaseFollowUp = {

    CaseNumber: '',
    FollowUpResp: null,

    LoadCaseFollowUp: function () {

        CaseFollowUp.CaseNumber = $("#CaseFollowUpNumber").val();

        if (CaseFollowUp.CaseNumber == "") {
            AlertFunction("Please enter the case number", "من فضلك ادخل رقم الحالة", "Error", "خطأ", "OK", "موافق");
            return;
        }

        var URL = Globals.ServicesURI_Test + "view/case/casenumber/#casenumber#/language/#Lang#";
        URL = URL.replace("#casenumber#", CaseFollowUp.CaseNumber).replace("#Lang#", Language);
        CallWebService(URL, 'CaseFollowUp.HandleCaseFolloupCallSuccess(response)', "CaseFollowUp.HandleCaseFollowUpCallFailure(error)", "", "", true, false);
    },

    HandleCaseSuccessDocReady: function () {
        SetHeaderTitle("Case Follow-Up", "متابعة معاملة");
        BindEvents('#CaseFollowUp_BackBtn', 'click', 'CaseFollowUp.HandleCaseFollowUpBackBtn()');
        $(".CaseFollowUpResultClass").css('color', 'black');
        $("#CaseFollowUp_lblCaseNo").html(replaceAll(CaseFollowUp.FollowUpResp.Case.CaseNumber, "u000a", ""));
        $("#CaseFollowUp_lblServiceName").html(replaceAll(CaseFollowUp.FollowUpResp.Case.ServiceName, "u000a", ""));
        $("#CaseFollowUp_lblCaseStatus").html(replaceAll(CaseFollowUp.FollowUpResp.Case.CaseStatus, "u000a", ""));
        $("#CaseFollowUp_lblLastUpdated").html(replaceAll(CaseFollowUp.FollowUpResp.Case.LastUpdate, "u000a", ""));
        $("#CaseFollowUp_lblNotes").html(replaceAll(CaseFollowUp.FollowUpResp.Case.Notes, "u000a", ""));
        $("#CaseFollowUpNumber").val("");
    },

    HandleCaseFolloupCallSuccess: function (response) {
        try {
            if (response != null) {
                var respObj = response;//JSON.parse(response);
                if (respObj.Message.Code == "0" && respObj.Case != null) {
                    CaseFollowUp.FollowUpResp = respObj;
                    LoadPageContent("CaseFollowUP", 'CaseFollowUp.HandleCaseSuccessDocReady()', false, true, true, true);
                }
                else {
                    if (respObj.Message.Body != null) {
                        HideLoadingSpinner();
                        var msg;
                        if (Language == 'en')
                            msg = "Case Number entered is wrong. Please enter correct Case Number";
                            else 
                            msg = "رقم المعاملة خطأ. من فضلك ادخل رقم المعاملة الصحيح ";

                        AlertFunction(msg, msg, "Error", "خطأ", "OK", "موافق");

                        $("#CaseFollowUpNumber").val("");
                    }
                    else {
                        HideLoadingSpinner();
                        AlertFunction("There was an error in our system , please try again later", "حدث خطأ في النظام من فضلك اعد المحاولة لاحقا", "Error", "خطأ", "OK", "موافق");
                        $("#CaseFollowUpNumber").val("");
                    }
                }
            }
            else {
                HideLoadingSpinner();
                AlertFunction("There was an error in our system , please try again later", "حدث خطاء في النظام من فضلك اعد المحا,لة لاحقا", "Error", "خطأ", "OK", "موافق");
            }
        } catch (e) {
            HideLoadingSpinner();
        }
    },


    HandleCaseFollowUpCallFailure: function () {
        AlertFunction("There was an error in our system , please try again later", "حدث خطأ في النظام من فضلك اعد المحاولة لاحقا", "Error", "خطأ", "OK", "موافق");
    },

    HandleCaseFollowUpBackBtn: function () {
        try {
            LoadPageContent("StartUp", 'StartUp.HandleStartUpDocReady()');
        } catch (e) {

        }
    }
};