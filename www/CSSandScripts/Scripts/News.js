﻿var News = {

    NewsRes: null,
    NewsDetailsResp: null,
    CurrentNewsDetailsID: 0,
    MostRecentNews: 0,
    ConnectionLost: false,

    LoadNewsPage: function () {

        try {
            HideLoadingSpinner();
            if (localStorage.getItem("News" + Language) == null || localStorage.getItem("News" + Language) == undefined) {
                var URL = Globals.ServicesURI_Test + "get/news/newsCount/10/language/#Lang#";
                URL = URL.replace("#Lang#", Language);
                CallWebService(URL, 'News.HandleNewsCallSuccess(response)', "", "", "", true, false);
            }
            else {

                var response = localStorage.getItem("News" + Language);
                News.NewsRes = JSON.parse(response);

                News.MostRecentNews = Number(News.NewsRes.News[0].Modified);

                for (var i = 0; i < News.NewsRes.News.length; i++) {
                    var Temp = Number(News.NewsRes.News[i].Modified);

                    if (News.MostRecentNews > Temp) {
                        News.MostRecentNews = Temp;
                    }
                }
                var URL = Globals.ServicesURI_Test + "validate/list/module/news/lastupdate/#LastUpdate#/language/#Lang#";
                URL = URL.replace("#LastUpdate#", News.MostRecentNews).replace("#Lang#", Language);
                CallWebService(URL, "News.HandleValidateNewsCallSuccess(response)", "News.HandleValidateNewsCallFailure()", "", "", true, false, false);
            }


        } catch (e) {

        }
    },

    HandleValidateNewsCallSuccess: function (response) {
        if (response != null) {
            var RespObj = response;//JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                if (RespObj.IsValid == true) {
                    LoadPageContent("News", 'News.HandleNewsDocReady()', true, true);
                }
                else {
                    News.NewsRes = null;
                    localStorage.removeItem("News" + Language);
                    var URL = Globals.ServicesURI_Test + "get/news/newsCount/10/language/#Lang#";
                    URL = URL.replace("#Lang#", Language);
                    CallWebService(URL, 'News.HandleNewsCallSuccess(response)', "", "", "", true, false);
                }
            }
        }

         //img cache

              /* Note: this is using version 2.x of the imagesloaded library, use of current version might differ */
              $('body').imagesLoaded(function($images, $proper, $broken ) {

                // see console output for debug info
                ImgCache.options.debug = true;
                ImgCache.options.usePersistentCache = true;
                
                ImgCache.init(function() {
                    // 1. cache images
                    for (var i = 0; i < $proper.length; i++) {
                        ImgCache.cacheFile($($proper[i]).attr('src'));
                    }
                    // 2. broken images get replaced
                    for (var i = 0; i < $broken.length; i++) {
                        ImgCache.useCachedFile($($broken[i]));
                    }

                });
            });
                //img cache
    },

    HandleValidateNewsCallFailure: function () {
        var Resp = localStorage.getItem("News" + Language);
        News.NewsRes = JSON.parse(Resp);
        LoadPageContent("News", 'News.HandleNewsDocReady()', false, true);
        News.ConnectionLost = true;
         //img cache

              /* Note: this is using version 2.x of the imagesloaded library, use of current version might differ */
              $('body').imagesLoaded(function($images, $proper, $broken ) {

                // see console output for debug info
                ImgCache.options.debug = true;
                ImgCache.options.usePersistentCache = true;
                
                ImgCache.init(function() {
                    // 1. cache images
                    for (var i = 0; i < $proper.length; i++) {
                        ImgCache.cacheFile($($proper[i]).attr('src'));
                    }
                    // 2. broken images get replaced
                    for (var i = 0; i < $broken.length; i++) {
                        ImgCache.useCachedFile($($broken[i]));
                    }

                });
            });
                //img cache
    },

    HandleNewsCallSuccess: function (response) {
        if (response != null) {
            localStorage.setItem("News" + Language, JSON.stringify(response));
            News.NewsRes = response;//JSON.parse(response);
        }
        LoadPageContent("News", 'News.HandleNewsDocReady()', true, true);
    },

    HandleNewsDocReady: function () {

        SetHeaderTitle("News", "الأخبار");

        $("#NewsListContent").html("");

        // if (News.ConnectionLost == true) {
        //     for (var i = 0; i < News.NewsRes.News.length; i++) {
        //         News.NewsRes.News[i].src = "Images/logo.png";
        //     }
        // }
        // else {
            for (var i = 0; i < News.NewsRes.News.length; i++) {
                News.NewsRes.News[i].src = ReturnImageSrc(News.NewsRes.News[i].NewsImage);
                // News.NewsRes.News[i].DownloadedSrc = downloadFiles(ReturnImageSrc(News.NewsRes.News[i].NewsImage));
                
                

            }
        // }

               



        $("#NewsTemplate").tmpl(News.NewsRes.News).appendTo("#NewsListContent");
    },

    OpenNewsDetails: function (id) {
        News.CurrentNewsDetailsID = id;
        // check news at cache.
        if (localStorage.getItem("NewsDetails" + Language + id) == null || localStorage.getItem("NewsDetails" + Language + id) == undefined) {
            var URL = Globals.ServicesURI_Test + "get/news/newsid/#id#/language/#Lang#";
            URL = URL.replace("#id#", id).replace("#Lang#", Language);
            CallWebService(URL, "News.HandleNewsDetailsSuccess(response)", "", "", "", true, true);
        }
        else {
            var response = localStorage.getItem("NewsDetails" + Language + News.CurrentNewsDetailsID);
            News.NewsDetailsResp = JSON.parse(response);

            var URL = Globals.ServicesURI_Test + "validate/item/module/news/itemid/#ID#/lastupdate/#LastUpdate#/language/#Lang#";
            URL = URL.replace("#LastUpdate#", News.NewsDetailsResp.News.Modified).replace("#ID#", News.CurrentNewsDetailsID).replace("#Lang#", Language);
            CallWebService(URL, "News.HandleValidateNewsItemCallSuccess(response)", "News.HandleValidateNewsItemCallFailure()", "", "", true, false, false);
        }
    },

    HandleValidateNewsItemCallSuccess: function (response) {
        if (response != null) {
            var RespObj = response;//JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                if (RespObj.IsValid == true) {
                    LoadPageContent("NewsDetails", 'News.HandleNewsDetailsDocReady()', true, true, true, false, "LoadPageContent('News', 'News.HandleNewsDocReady()', true, true);");
                }
                else {
                    News.NewsDetailsResp = null;
                    localStorage.removeItem("NewsDetails" + Language + News.CurrentNewsDetailsID);
                    var URL = Globals.ServicesURI_Test + "get/news/newsid/#id#/language/#Lang#";
                    URL = URL.replace("#id#", id).replace("#Lang#", Language);
                    CallWebService(URL, "News.HandleNewsDetailsSuccess(response)", "", "", "", true, true);
                }
            }
        }
    },

    HandleValidateNewsItemCallFailure: function () {
        var Resp = localStorage.getItem("NewsDetails" + Language + News.CurrentNewsDetailsID);
        News.NewsDetailsResp = JSON.parse(Resp);
        LoadPageContent("NewsDetails", 'News.HandleNewsDetailsDocReady()', false, true, true, false, "LoadPageContent('News', 'News.HandleNewsDocReady()', true, true);");
    },

    HandleNewsDetailsSuccess: function (response) {
        if (response != null) {
            News.NewsDetailsResp = response;//JSON.parse(response);
            localStorage.setItem("NewsDetails" + Language + News.CurrentNewsDetailsID, JSON.stringify(response));
        }
        LoadPageContent("NewsDetails", 'News.HandleNewsDetailsDocReady()', true, true, true, false, "LoadPageContent('News', 'News.HandleNewsDocReady()', true, true);");
    },

    HandleNewsDetailsDocReady: function () {
        // $("#NewsDetails_Content").html("");
        SetHeaderTitle("News Details", "تفاصيل الأخبار");
        $("#NewsDetailsImg").attr("src", "");
        $("#NewsDetailsImg").width(DeviceWidth - 50);
        $("#NewsDetailsImg").height(DeviceHeight * .3);
        console.log(News.NewsDetailsResp.News.Details);
        $("#NewsTitle").html(News.NewsDetailsResp.News.Title);
        $("#NewsDate").html(ConvertUNIXToDate(News.NewsDetailsResp.News.NewsDate));

        BindEvents('#NewsDetails_BackBtn', 'click', 'News.HandleNewsDetailsBackBtn()');
        $("#NewsDetails_Content").html(News.NewsDetailsResp.News.Details);

        var src = "";

        // if (this.ConnectionLost == true) {
        //     src = "Images/logo.png";
        // }
        // else {
            //use the image in the localstorage by default
            //revert to the default if the localstorage version is null
            src = ReturnImageSrc(News.NewsDetailsResp.News.NewsImage);

            if (src == "" || src == null || src == "undefined") {
                src = "Images/logo.png";
            }


        // }

        $("#NewsDetailsImg").attr("src", src)
    },

    HandleNewsDetailsBackBtn: function () {
        LoadPageContent("News", 'News.HandleNewsDocReady()', true, true);
    }
};
var assetURL = "";
var fileName = "";
var store;

function downloadFiles(ImagePath) {
    try {

        assetURL = ImagePath;
        var temparr = assetURL.toString().split('/');
        fileName = temparr[temparr.length - 1];
        var uri = encodeURI(assetURL);
        store = cordova.file.dataDirectory;
        //alert(window.rootFS.fullPath);
        var ImgLocation = window.rootFS.fullPath + "";//(store + fileName).toString();
        // var ImgLocation =fileSystem.root.fullPath;
        //   window.resolveLocalFileSystemURL(store + fileName, ImageDownLoaded, downloadAsset);
        var fileTransfer = new FileTransfer();
        fileTransfer.download(
          uri,
          ImgLocation,
          function (entry) {
              alert("complete" + entry.toURL());
              console.log("download complete: " + entry.toURL());
          },
          function (error) {
              alert("error " + error.source + " dd  " + error.target + " rr " + error.code);
              console.log("download error source " + error.source);
              console.log("download error target " + error.target);
              console.log("upload error code" + error.code);
          },
          false,
          {
              headers: {
                  "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
              }
          }
          );
    } catch (e) {
        alert(e);
    }
}

function downloadAsset() {
    try {
        var fileTransfer = new FileTransfer();
        console.log("About to start transfer");
        fileTransfer.download(assetURL, store + fileName,
            function (entry) {
                //alert("success");
                console.log("Success!");
            },
            function (err) {
                //alert("fail" + err);
                console.log("Error");
                console.dir(err);
            });
    } catch (e) {
        alert(e);
    }
}

function ImageDownLoaded() {
   // alert("ImageDownLoaded");
}