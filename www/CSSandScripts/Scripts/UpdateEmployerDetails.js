﻿var UpdateEmployer = {

    EmployerDetailsObj: null,
    SelectedVal: 0,
    RequiredDocLength: 0,
    UploadedFilesName: new Array(),
    UploadedFilesCount: 0,
    isFromServices: false,

    LoadUpdateEmployer: function (isFromServices) {
        UpdateEmployer.isFromServices = isFromServices;
        var URL = Globals.ServicesURI_Test + "view/employer/employerrowid/#EmpRowID#/language/#Lang#";
        URL = URL.replace("#EmpRowID#", Globals.EmployerRowID).replace("#Lang#", Language);
        CallWebService(URL, "UpdateEmployer.HandleGetEmployerDetailsCallSuucess(response)", "UpdateEmployer.HandleGetEmployerDetailsCallFailure()", "", "", true, false);
    },

    HandleGetEmployerDetailsCallFailure: function () {

    },

    HandleGetEmployerDetailsCallSuucess: function (response) {
        if (response != null) {
            var RespObj = response;// JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                UpdateEmployer.EmployerDetailsObj = RespObj.Employer;
                if (UpdateEmployer.EmployerDetailsObj.Header == null) {
                    UpdateEmployer.EmployerDetailsObj.Header = new Object();
                }
                UpdateEmployer.EmployerDetailsObj.Header.AttachedFiles = new Array();
                if (UpdateEmployer.isFromServices == true) {
                    LoadPageContent("UpdateEmployerDetails", 'UpdateEmployer.HandleUpdateEmployerDocReady()', false, true, true, false, 'ServicesList.LoadServicesPage()');
                }
                else {
                    LoadPageContent("UpdateEmployerDetails", 'UpdateEmployer.HandleUpdateEmployerDocReady()', false, true);
                }
            }
            else {
                HideLoadingSpinner();
                AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        }
    },

    HandleUpdateEmployerDocReady: function () {
        SetHeaderTitle("Update Employer’s Details", "طلب تحديث بيانات صاحب العمل");
        BindEvents('#UpdateEmployerDetails_ReasonsDropDown', 'change', 'UpdateEmployer.HandleReasonsDropDownChanged()');
        BindEvents('#UpdateEmployerDetails_SaveBtn', 'click', 'UpdateEmployer.HandleSaveBtnClicked()');
        BindEvents('#UpdateEmployerDetails_UploadBtn', 'click', 'UpdateEmployer.HandleUploadDocBtnClicked()');

        $('#UpdateEmployerDetails_FirstName').on('input', function (e) {
            isArabic(UpdateEmployerDetails_FirstName);
        });
        $('#UpdateEmployerDetails_SecondName').on('input', function (e) {
            isArabic(UpdateEmployerDetails_SecondName);
        });
        $('#UpdateEmployerDetails_ThirdName').on('input', function (e) {
            isArabic(UpdateEmployerDetails_ThirdName);
        });
        $('#UpdateEmployerDetails_FourthName').on('input', function (e) {
            isArabic(UpdateEmployerDetails_FourthName);
        });

        $("#UpdateEmployerDetails_FaxNumber").mask("+XXX-X-XXXXXXX", { autoclear: false }).val("+971");
        $("#UpdateEmployerDetails_PhoneNumber").mask("+XXX-XX-XXXXXXX", { autoclear: false }).val("+971");
        $("#UpdateEmployerDetails_EntityMobile").mask("+XXX-XX-XXXXXXX", { autoclear: false }).val("+971");
        $("#UpdateEmployerDetails_LandLine").mask("+XXX-XX-XXXXXXX", { autoclear: false }).val("+971");
        $("#UpdateEmployerDetails_EntityEmiratesID").mask("784-XXXX-XXXXXXX-X");
    },

    HandleReasonsDropDownChanged: function () {

        var val = $("#UpdateEmployerDetails_ReasonsDropDown").val();
        var I_Type = "";
        var I_SubType = "";

        $(".UpdateEmployerContentClass").hide();

        UpdateEmployer.SelectedVal = val;

        //var URL = Globals.ServicesURI_Test + "view/requireddocuments/casetype/#Type#/caseSubType/#SubType#/language/#Lang#";
        var URL = "http://mgovws.gpssa.ae:8080/SPREST.svc/view/requireddocuments/language/#Lang#";

        try {
            $("#UpdateEmployerDetails_FirstName").val("");
            $("#UpdateEmployerDetails_SecondName").val("");
            $("#UpdateEmployerDetails_ThirdName").val("");
            $("#UpdateEmployerDetails_FourthName").val("");
            $("#UpdateEmployerDetails_EntityMobile").val("");
            $("#UpdateEmployerDetails_EntityMobile").val("+971");
            $("#UpdateEmployerDetails_EntityEmiratesID").val("");
            $("#UpdateEmployerDetails_LandLine").val("");
            $("#UpdateEmployerDetails_LandLine").val("971");
            $("#UpdateEmployerDetails_Email").val("");
            $("#UpdateEmployerDetails_EntityName").val("");
            $("#UpdateEmployerDetails_EntityNameAR").val("");
        } catch (e) {

        }

        if (val == -1) {
            CleanPage("UpdateEmployerDetails");
            return;
        }
        else if (val == "0") {

            $("#UpdateEmployerDetails_EntityName_TR").show();
            $("#UpdateEmployerDetails_EntityName_TR_Space").show();
            $("#UpdateEmployerDetails_EntityNameAR_TR").show();
            $("#UpdateEmployerDetails_EntityNameAR_TR_Space").show();
            try {
                $("#UpdateEmployerDetails_EntityName").val(UpdateEmployer.EmployerDetailsObj.EmployerName);
                $("#UpdateEmployerDetails_EntityNameAR").val(UpdateEmployer.EmployerDetailsObj.EmployerArabicName);
            } catch (e) {
                $("#UpdateEmployerDetails_EntityName").val("");
                $("#UpdateEmployerDetails_EntityNameAR").val("");
            }

            I_Type = "Update Employer";
            I_SubType = "Change Entity Name";
            //URL = URL.replace("#Type#", "Update Employer").replace("#SubType#", "Change Entity Name").replace("#Lang#", Language);
        }
        else if (val == "1") {

            $(".UpdateName").show();

            if (UpdateEmployer.EmployerDetailsObj.Contacts.length > 0) {
                for (var i = 0; i < UpdateEmployer.EmployerDetailsObj.Contacts.length; i++) {
                    if (UpdateEmployer.EmployerDetailsObj.Contacts[i].IsOwner == true) {
                        try {
                            $("#UpdateEmployerDetails_FirstName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].FirstName);
                            $("#UpdateEmployerDetails_SecondName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].SecondName);
                            $("#UpdateEmployerDetails_ThirdName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].ThirdName);
                            $("#UpdateEmployerDetails_FourthName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].FourthName);
                            //$("#UpdateEmployerDetails_EntityBirthDate").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].BirthDate);
                            $("#UpdateEmployerDetails_EntityMobile").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].Mobile);
                            $("#UpdateEmployerDetails_EntityEmiratesID").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].EmiratesId);
                            $("#UpdateEmployerDetails_LandLine").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].Landline);
                            $("#UpdateEmployerDetails_Email").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].Email);

                        } catch (e) {
                            $("#UpdateEmployerDetails_FirstName").val("");
                            $("#UpdateEmployerDetails_SecondName").val("");
                            $("#UpdateEmployerDetails_ThirdName").val("");
                            $("#UpdateEmployerDetails_FourthName").val("");
                            //$("#UpdateEmployerDetails_EntityBirthDate").val("");
                            $("#UpdateEmployerDetails_EntityMobile").val("");
                            $("#UpdateEmployerDetails_EntityMobile").val("+971");
                            $("#UpdateEmployerDetails_EntityEmiratesID").val("");
                            $("#UpdateEmployerDetails_LandLine").val("");
                            $("#UpdateEmployerDetails_LandLine").val("+971");
                            $("#UpdateEmployerDetails_Email").val("");
                        }
                    }
                }
            }
            else {
                $("#UpdateEmployerDetails_FirstName").val("");
                $("#UpdateEmployerDetails_SecondName").val("");
                $("#UpdateEmployerDetails_ThirdName").val("");
                $("#UpdateEmployerDetails_FourthName").val("");
                //$("#UpdateEmployerDetails_EntityBirthDate").val("");
                $("#UpdateEmployerDetails_EntityMobile").val("");
                $("#UpdateEmployerDetails_EntityMobile").val("+971");
                $("#UpdateEmployerDetails_EntityEmiratesID").val("");
                $("#UpdateEmployerDetails_LandLine").val("");
                $("#UpdateEmployerDetails_LandLine").val("+971");
                $("#UpdateEmployerDetails_Email").val("");
            }

            //URL = URL.replace("#Type#", "Update Employer").replace("#SubType#", "Change Entity Owner").replace("#Lang#", Language);
            I_Type = "Update Employer";
            I_SubType = "Change Entity Owner";
        }
        else if (val == "2") {
            $(".UpdateName").show();
            if (UpdateEmployer.EmployerDetailsObj.Contacts.length > 0) {
                for (var i = 0; i < UpdateEmployer.EmployerDetailsObj.Contacts.length; i++) {
                    if (UpdateEmployer.EmployerDetailsObj.Contacts[i].IsManager == true) {
                        try {
                            $("#UpdateEmployerDetails_FirstName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].FirstName);
                            $("#UpdateEmployerDetails_SecondName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].SecondName);
                            $("#UpdateEmployerDetails_ThirdName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].ThirdName);
                            $("#UpdateEmployerDetails_FourthName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].FourthName);
                            //   $("#UpdateEmployerDetails_EntityBirthDate").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].BirthDate);
                            $("#UpdateEmployerDetails_EntityMobile").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].Mobile);
                            $("#UpdateEmployerDetails_EntityEmiratesID").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].EmiratesId);
                            $("#UpdateEmployerDetails_LandLine").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].Landline);
                            $("#UpdateEmployerDetails_Email").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].Email);
                        } catch (e) {
                            $("#UpdateEmployerDetails_FirstName").val("");
                            $("#UpdateEmployerDetails_SecondName").val("");
                            $("#UpdateEmployerDetails_ThirdName").val("");
                            $("#UpdateEmployerDetails_FourthName").val("");
                            // $("#UpdateEmployerDetails_EntityBirthDate").val("");
                            $("#UpdateEmployerDetails_EntityMobile").val("");
                            $("#UpdateEmployerDetails_EntityMobile").val("+971");
                            $("#UpdateEmployerDetails_EntityEmiratesID").val("");
                            $("#UpdateEmployerDetails_LandLine").val("");
                            $("#UpdateEmployerDetails_LandLine").val("+971");
                            $("#UpdateEmployerDetails_Email").val("");
                        }
                    }
                }
            }
            else {
                $("#UpdateEmployerDetails_FirstName").val("");
                $("#UpdateEmployerDetails_SecondName").val("");
                $("#UpdateEmployerDetails_ThirdName").val("");
                $("#UpdateEmployerDetails_FourthName").val("");
                //$("#UpdateEmployerDetails_EntityBirthDate").val("");
                $("#UpdateEmployerDetails_EntityMobile").val("");
                $("#UpdateEmployerDetails_EntityMobile").val("+971");
                $("#UpdateEmployerDetails_EntityEmiratesID").val("");
                $("#UpdateEmployerDetails_LandLine").val("");
                $("#UpdateEmployerDetails_LandLine").val("+971");
                $("#UpdateEmployerDetails_Email").val("");
            }

            //URL = URL.replace("#Type#", "Update Employer").replace("#SubType#", "Admin Details").replace("#Lang#", Language);
            I_Type = "Update Employer";
            I_SubType = "Admin Details";
        }
        else if (val == "3") {
            //URL = URL.replace("#Type#", "Update Employer").replace("#SubType#", "Entity Cancellation").replace("#Lang#", Language);
            I_Type = "Update Employer";
            I_SubType = "Entity Cancellation";
        }
        else if (val == "4") {
            //URL = URL.replace("#Type#", "Update Employer").replace("#SubType#", "Admin Details").replace("#Lang#", Language);
            I_Type = "Update Employer";
            I_SubType = "Admin Details";
        }
        else if (val == "5") {

            //URL = URL.replace("#Type#", "Update Employer").replace("#SubType#", "Admin Details").replace("#Lang#", Language);
            I_Type = "Update Employer";
            I_SubType = "Admin Details";

            $("#UpdateEmployerDetails_EntityAddress1_TR").show();
            $("#UpdateEmployerDetails_EntityAddress1_TR_Space").show();
            $("#UpdateEmployerDetails_EntityAddress2_TR").show();
            $("#UpdateEmployerDetails_EntityAddress2_TR_Space").show();
            $("#UpdateEmployerDetails_EntityDistrict_TR").show();
            $("#UpdateEmployerDetails_EntityDistrict_TR_Space").show();
            $("#UpdateEmployerDetails_EntityEmirate_TR").show();
            $("#UpdateEmployerDetails_EntityEmirate_TR_Space").show();
            $("#UpdateEmployerDetails_PhoneNumber_TR").show();
            $("#UpdateEmployerDetails_PhoneNumber_TR_Space").show();
            $("#UpdateEmployerDetails_FaxNumber_TR").show();
            $("#UpdateEmployerDetails_FaxNumber_TR_Space").show();
            $("#UpdateEmployerDetails_POBox_TR").show();
            $("#UpdateEmployerDetails_POBox_TR_Space").show();
            $("#UpdateEmployerDetails_EntityEmirate").html(EmiratesHTML);

            try {
                $("#UpdateEmployerDetails_PhoneNumber").val(UpdateEmployer.EmployerDetailsObj.Phone);
                $("#UpdateEmployerDetails_FaxNumber").val(UpdateEmployer.EmployerDetailsObj.Fax);
                $("#UpdateEmployerDetails_POBox").val(UpdateEmployer.EmployerDetailsObj.POBox);

                if (UpdateEmployer.EmployerDetailsObj.Address.length > 0) {
                    $("#UpdateEmployerDetails_EntityAddress1").val(UpdateEmployer.EmployerDetailsObj.Address[0].AddressLine1);
                    $("#UpdateEmployerDetails_EntityAddress2").val(UpdateEmployer.EmployerDetailsObj.Address[0].AddressLine2);
                    $("#UpdateEmployerDetails_EntityDistrict").val(UpdateEmployer.EmployerDetailsObj.Address[0].District);
                    $("#UpdateEmployerDetails_EntityEmirate").val(UpdateEmployer.EmployerDetailsObj.Address[0].Emirate).trigger('change');
                }
                else {
                    $("#UpdateEmployerDetails_EntityAddress1").val("");
                    $("#UpdateEmployerDetails_EntityAddress2").val("");
                    $("#UpdateEmployerDetails_EntityDistrict").val("");
                    $("#UpdateEmployerDetails_EntityEmirate").val("-1").trigger('change');
                }
            } catch (e) {

                $("#UpdateEmployerDetails_EntityAddress1").val("");
                $("#UpdateEmployerDetails_EntityAddress2").val("");
                $("#UpdateEmployerDetails_EntityDistrict").val("");
                $("#UpdateEmployerDetails_EntityEmirate").val("-1").trigger('change');
            }
        }
        else if (val == "6") {
            $(".UpdateName").show();
            if (UpdateEmployer.EmployerDetailsObj.Contacts.length > 0) {
                for (var i = 0; i < UpdateEmployer.EmployerDetailsObj.Contacts.length; i++) {
                    if (UpdateEmployer.EmployerDetailsObj.Contacts[i].IsAuthorizedPerson == true) {
                        try {
                            $("#UpdateEmployerDetails_FirstName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].FirstName);
                            $("#UpdateEmployerDetails_SecondName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].SecondName);
                            $("#UpdateEmployerDetails_ThirdName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].ThirdName);
                            $("#UpdateEmployerDetails_FourthName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].FourthName);
                            //          $("#UpdateEmployerDetails_EntityBirthDate").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].BirthDate);
                            $("#UpdateEmployerDetails_EntityMobile").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].Mobile);
                            $("#UpdateEmployerDetails_EntityEmiratesID").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].EmiratesId);
                            $("#UpdateEmployerDetails_LandLine").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].Landline);
                            $("#UpdateEmployerDetails_Email").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].Email);
                        } catch (e) {
                            $("#UpdateEmployerDetails_FirstName").val("");
                            $("#UpdateEmployerDetails_SecondName").val("");
                            $("#UpdateEmployerDetails_ThirdName").val("");
                            $("#UpdateEmployerDetails_FourthName").val("");
                            //        $("#UpdateEmployerDetails_EntityBirthDate").val("");
                            $("#UpdateEmployerDetails_EntityMobile").val("+971");
                            $("#UpdateEmployerDetails_EntityMobile").val("");
                            $("#UpdateEmployerDetails_EntityEmiratesID").val("");
                            $("#UpdateEmployerDetails_LandLine").val("");
                            $("#UpdateEmployerDetails_LandLine").val("+971");
                            $("#UpdateEmployerDetails_Email").val("");
                        }
                    }
                }
            }
            else {
                $("#UpdateEmployerDetails_FirstName").val("");
                $("#UpdateEmployerDetails_SecondName").val("");
                $("#UpdateEmployerDetails_ThirdName").val("");
                $("#UpdateEmployerDetails_FourthName").val("");
                //$("#UpdateEmployerDetails_EntityBirthDate").val("");
                $("#UpdateEmployerDetails_EntityMobile").val("");
                $("#UpdateEmployerDetails_EntityMobile").val("+971");
                $("#UpdateEmployerDetails_EntityEmiratesID").val("");
                $("#UpdateEmployerDetails_LandLine").val("");
                $("#UpdateEmployerDetails_LandLine").val("+971");
                $("#UpdateEmployerDetails_Email").val("");
            }

            //URL = URL.replace("#Type#", "Update Employer").replace("#SubType#", "Change in Authorized Persons").replace("#Lang#", Language);
            I_Type = "Update Employer";
            I_SubType = "Change in Authorized Persons";
        }
        Log(URL);
        var data = {
            Type: I_Type,
            SubType: I_SubType
        };

        var ReqObj = JSON.stringify(data);
        URL = URL.replace("#Lang#", Language);
        CallWebService(URL, "UpdateEmployer.HandleRequiredDocCallSuccess(response)", "", ReqObj, "POST", true, true);
    },

    HandleRequiredDocCallSuccess: function (response) {
        $("#UpdateEmployerDetails_RequiredDoc").html("");
        if (response != null) {
            var RespObj = response;//JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                var Temp = RespObj.RequiredDocuments.split(',');
                UpdateEmployer.RequiredDocLength = Temp.length;
                var AllHtml = "";
                for (var i = 0; i < Temp.length; i++) {
                    var HtmlTemp = $('#UpdateEmployerDetails_File_Temp').html().replace('$RequireDoc$', Temp[i]);
                    AllHtml += HtmlTemp;
                }
                $("#UpdateEmployerDetails_RequiredDoc").append(AllHtml);
            }
        }
    },

    HandleUploadDocBtnClicked: function () {

        if (UpdateEmployer.RequiredDocLength > 0) {

            //if (UpdateEmployer.RequiredDocLength <= UpdateEmployer.UploadedFilesCount) {
            //    return;
            //}

            var fileUpload = $("#UpdateEmployerDetails_File").get(0);
            var imagefile = fileUpload.files;
            if (imagefile.length < 1) {
                AlertFunction("Please select Documents to upload", "من فضلك اختر الملفات المطلوبة لرفعها", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if (!GetUploadedDocsSize("UpdateEmployerDetails_File")) {
                AlertFunction("Max Size for documents is 7 MB", "الحد الاقصى للملفات 7 م ب", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else {
                var uuid = guid();
                UploadFiles("UpdateEmployerDetails_File", uuid, "UpdateEmployer.HandleUploadDocSuccess(name)");
            }
        }
    },

    HandleSaveBtnClicked: function () {
        try {
            var CleanUpdateEmployerDetails_EntityMobile = FormatChar($("#UpdateEmployerDetails_EntityMobile").val());
            var CleanWithPlusEntityMobile = "+" + CleanUpdateEmployerDetails_EntityMobile;
            var CleanUpdateEmployerDetails_LandLine = FormatChar($("#UpdateEmployerDetails_LandLine").val());
            var CleanWithPlusLandLine = "+" + CleanUpdateEmployerDetails_LandLine;
            //alert("CleanWithPlusEntityMobile  " + CleanWithPlusEntityMobile);
            //alert("CleanWithPlusLandLine  " + CleanWithPlusLandLine);
            if (UpdateEmployer.SelectedVal == -1) {
                AlertFunction("Please select reason for update", "من فضلك اختر سبب التحديث", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if (UpdateEmployer.SelectedVal == 0) {
                UpdateEmployer.EmployerDetailsObj.EmployerName = $("#UpdateEmployerDetails_EntityName").val();
                UpdateEmployer.EmployerDetailsObj.EmployerArabicName = $("#UpdateEmployerDetails_EntityNameAR").val();
            }
            else if (UpdateEmployer.SelectedVal == 1) {
                for (var i = 0; i < UpdateEmployer.EmployerDetailsObj.Contacts.length; i++) {
                    if (UpdateEmployer.EmployerDetailsObj.Contacts[i].IsOwner == true) {
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].FirstName = $("#UpdateEmployerDetails_FirstName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].SecondName = $("#UpdateEmployerDetails_SecondName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].ThirdName = $("#UpdateEmployerDetails_ThirdName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].FourthName = $("#UpdateEmployerDetails_FourthName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Mobile = CleanWithPlusEntityMobile;
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].EmiratesId = FormatChar($("#UpdateEmployerDetails_EntityEmiratesID").val());
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Landline = CleanWithPlusLandLine;
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Email = $("#UpdateEmployerDetails_Email").val();
                    }
                }
            }
            else if (UpdateEmployer.SelectedVal == 2) {
                for (var i = 0; i < UpdateEmployer.EmployerDetailsObj.Contacts.length; i++) {
                    if (UpdateEmployer.EmployerDetailsObj.Contacts[i].IsManager == true) {
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].FirstName = $("#UpdateEmployerDetails_FirstName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].SecondName = $("#UpdateEmployerDetails_SecondName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].ThirdName = $("#UpdateEmployerDetails_ThirdName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].FourthName = $("#UpdateEmployerDetails_FourthName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Mobile = CleanWithPlusEntityMobile;
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].EmiratesId = FormatChar($("#UpdateEmployerDetails_EntityEmiratesID").val());
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Landline = CleanWithPlusLandLine;
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Email = $("#UpdateEmployerDetails_Email").val();
                    }
                }
            }
            else if (UpdateEmployer.SelectedVal == 3)
            { }
            else if (UpdateEmployer.SelectedVal == 4)
            { }
            else if (UpdateEmployer.SelectedVal == 5) {
                UpdateEmployer.EmployerDetailsObj.Address[0].AddressLine1 = $("#UpdateEmployerDetails_EntityAddress1").val();
                UpdateEmployer.EmployerDetailsObj.Address[0].AddressLine2 = $("#UpdateEmployerDetails_EntityAddress2").val();
                UpdateEmployer.EmployerDetailsObj.Address[0].District = $("#UpdateEmployerDetails_EntityDistrict").val();
                UpdateEmployer.EmployerDetailsObj.Address[0].Emirate = $("#UpdateEmployerDetails_EntityEmirate").val();
                var CleanUpdateEmployerDetails_PhoneNumber = FormatChar($("#UpdateEmployerDetails_PhoneNumber").val());
                UpdateEmployer.EmployerDetailsObj.Phone = "+" + CleanUpdateEmployerDetails_PhoneNumber;
                //alert("UpdateEmployer.EmployerDetailsObj.Phone " + UpdateEmployer.EmployerDetailsObj.Phone);
                var CleanUpdateEmployerDetails_FaxNumber = FormatChar($("#UpdateEmployerDetails_FaxNumber").val());
                UpdateEmployer.EmployerDetailsObj.Fax = "+" + CleanUpdateEmployerDetails_FaxNumber;
                //alert("UpdateEmployer.EmployerDetailsObj.Fax " + UpdateEmployer.EmployerDetailsObj.Fax);
                UpdateEmployer.EmployerDetailsObj.POBox = $("#UpdateEmployerDetails_POBox").val();
            }
            else if (UpdateEmployer.SelectedVal == 6) {
                for (var i = 0; i < UpdateEmployer.EmployerDetailsObj.Contacts.length; i++) {
                    if (UpdateEmployer.EmployerDetailsObj.Contacts[i].IsAuthorizedPerson == true) {
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].FirstName = $("#UpdateEmployerDetails_FirstName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].SecondName = $("#UpdateEmployerDetails_SecondName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].ThirdName = $("#UpdateEmployerDetails_ThirdName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].FourthName = $("#UpdateEmployerDetails_FourthName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Mobile = CleanWithPlusEntityMobile;
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].EmiratesId = FormatChar($("#UpdateEmployerDetails_EntityEmiratesID").val());
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Landline = CleanWithPlusLandLine;
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Email = $("#UpdateEmployerDetails_Email").val();
                    }
                }
            }

            //if (UpdateEmployer.RequiredDocLength > UpdateEmployer.UploadedFilesCount) {
            //    AlertFunction("Please upload the required documents", "من فضلك ارفع الملفات المطلوبة", "Error", "خطأ", "OK", "موافق");
            //    return;
            //}

            if ($.trim($("#UpdateEmployerDetails_Email").val()) != "") {
                if (!ValidateEmail($("#UpdateEmployerDetails_Email").val())) {
                    AlertFunction("Please enter valid E-mail", "من فضلك ادخل البريد الإلكترونى صالح", "Error", "خطأ", "OK", "موافق");
                    return;
                }
            }

            var URL = Globals.ServicesURI_Test + "update/employer/language/#Lang#";
            URL = URL.replace("#Lang#", Language);

            if (isUndefinedOrNullOrBlank(UpdateEmployer.EmployerDetailsObj.ExpirationDate) || UpdateEmployer.EmployerDetailsObj.ExpirationDate == "01/01/2020") {
                UpdateEmployer.EmployerDetailsObj.ExpirationDate = "1577829600";//1-1-2020
            }

            UpdateEmployer.EmployerDetailsObj.ExpirationDate = ConvertDateToUNIX(UpdateEmployer.EmployerDetailsObj.ExpirationDate);

            var data = JSON.stringify(UpdateEmployer.EmployerDetailsObj);

            CallWebService(URL, 'UpdateEmployer.HandleUpdateCallSuccess(response)', "", data, "POST", true, true, true, 120000);

        } catch (e) {
        }
    },

    HandleUploadDocSuccess: function (name) {
        try {
            UpdateEmployer.UploadedFilesName[UpdateEmployer.UploadedFilesCount] = name;
            UpdateEmployer.EmployerDetailsObj.Header.AttachedFiles[UpdateEmployer.UploadedFilesCount] = name;
            UpdateEmployer.UploadedFilesCount += 1;
        } catch (e) {
        }
    },

    HandleUpdateCallSuccess: function (response) {
        if (response != null) {
            if (response.Message.Code == 0) {
                AlertFunction("Case Number : " + response.CaseNumber, "رقم الحالة : " + response.CaseNumber, "Success", "نجاح", "OK", "موافق");
                UpdateEmployer.SelectedVal = 0;
                UpdateEmployer.RequiredDocLength = 0;
                UpdateEmployer.UploadedFilesName = new Array();
                UpdateEmployer.UploadedFilesCount = 0;
                $("#UpdateEmployerDetails_RequiredDoc").html("");
                CleanPage("UpdateEmployerDetails");
            }
            else {
                AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        }
    },

    OpenUploadFile: function () {
        $("#UpdateEmployerDetails_File").css("display", "block");
        $("#UpdateEmployerDetails_File").css("visibility", "visible");
        setTimeout(function () {
            $("#UpdateEmployerDetails_File").trigger("click");
        }, 300);
        $("#UpdateEmployerDetails_File").css("display", "none");
        $("#UpdateEmployerDetails_File").css("visibility", "hidden");
    }
};

function clearCache() {
    navigator.camera.cleanup();
}
