﻿var NearestOffice = {

    NearestGPSSAOfficeResp: null,
    MostRecentOffice: 0,

    LoadNearestOfficeContent: function () {
        if (navigator.geolocation) {
            ShowLoadingSpinner();
            var options = { timeout: 5000 };
            navigator.geolocation.getCurrentPosition(NearestOffice.GetPositionSuccess, NearestOffice.GetPositionError, options);
        }
        else {
            AlertFunction("Geolocation is not supported by this browser.", "Geolocation is not supported by this browser.", "Error", "خطأ", "OK", "موافق");
            localStorage.setItem("LatLang", "0#0");
        }
    },

    HandleGetOfficesCallSuccess: function (response) {
        try {
            if (response != null) {
                var RespObj = response;//JSON.parse(response);

                if (RespObj.Message.Code == 0) {
                    localStorage.setItem("GPSSAOffices" + Language, JSON.stringify(response));
                    NearestOffice.NearestGPSSAOfficeResp = RespObj;
                    GPSSAOfficeResponse = NearestOffice.NearestGPSSAOfficeResp;
                    LoadPageContent("NearestOffice", 'NearestOffice.HandleNearestOfficeDocReady()', false, false);
                }
                else {
                    GPSSAOfficeResponseGotten = false;
                    AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
                }
            }
            else {
            }
        } catch (e) {
        }
    },

    HandleGetOfficesCallFailure: function () {
        GPSSAOfficeResponseGotten = false;
    },

    HandleNearestOfficeDocReady: function () {
        try {

            SetHeaderTitle("nearest GPSSA office", "أقرب مكتب للهيئة");
            document.getElementById('ATMframe').src = null;
            $("#ATMframe").height(DeviceHeight - 150);

            for (var i = 0; i < NearestOffice.NearestGPSSAOfficeResp.GPSSAOffices.length; i++) {
                NearestOffice.NearestGPSSAOfficeResp.GPSSAOffices[i].DistInKM = NearestOffice.calculateDistance(NearestOffice.NearestGPSSAOfficeResp.GPSSAOffices[i].Latitude, NearestOffice.NearestGPSSAOfficeResp.GPSSAOffices[i].longitude);
            }

            var SortedArr = NearestOffice.NearestGPSSAOfficeResp.GPSSAOffices.sort(function (a, b) {
                if (a.DistInKM > b.DistInKM) {
                    return 1;
                }
                if (a.DistInKM < b.DistInKM) {
                    return -1;
                }
                return 0;
            });

            localStorage.setItem("PosObject", JSON.stringify(SortedArr[0]));
            document.getElementById('ATMframe').src = "Templates/Map.html";
            HideLoadingSpinner();

        } catch (e) {

        }
    },

    GetPositionError: function (err) {

        var msg;
        if (Language = 'en')
            msg = "Sorry, we could not get your current location accurately";
        else
            msg = "عفوا, لم نستطع تحديد مكانك الحالي بدقة";
        AlertFunction(msg, msg, "Error", "خطأ", "OK", "موافق");
        HideLoadingSpinner();
        localStorage.setItem("LatLang", "0#0");

        if (GPSSAOfficeResponseGotten == false) {

            if (localStorage.getItem("GPSSAOffices" + Language) == null || localStorage.getItem("GPSSAOffices" + Language) == undefined) {
                var URL = Globals.ServicesURI_Test + "get/office/language/#Lang#";
                URL = URL.replace("#Lang#", Language);

                CallWebService(URL, 'NearestOffice.HandleGetOfficesCallSuccess(response)', "NearestOffice.HandleGetOfficesCallFailure()", "", "", true, false);
            }
            else {
                var response = localStorage.getItem("GPSSAOffices" + Language);
                NearestOffice.NearestGPSSAOfficeResp = JSON.parse(response);

                NearestOffice.MostRecentOffice = Number(NearestOffice.NearestGPSSAOfficeResp.GPSSAOffices[0].Modified);

                for (var i = 0; i < NearestOffice.NearestGPSSAOfficeResp.GPSSAOffices.length; i++) {
                    var Temp = Number(NearestOffice.NearestGPSSAOfficeResp.GPSSAOffices[i].Modified);

                    if (NearestOffice.MostRecentOffice > Temp) {
                        NearestOffice.MostRecentOffice = Temp;
                    }
                }

                var URL = Globals.ServicesURI_Test + "validate/list/module/gpssaoffices/lastupdate/#LastUpdate#/language/#Lang#";
                URL = URL.replace("#LastUpdate#", NearestOffice.MostRecentOffice).replace("#Lang#", Language);
                CallWebService(URL, "NearestOffice.HandleValidateOfficesCallSuccess(response)", "NearestOffice.HandleValidateOfficesCallFailure()", "", "", true, false, false);
            }

        } else {

            NearestOffice.NearestGPSSAOfficeResp = GPSSAOfficeResponse;
            LoadPageContent("NearestOffice", 'NearestOffice.HandleNearestOfficeDocReady()', true, true);
        }
        ShowLoadingSpinner()
    },

    HandleValidateOfficesCallSuccess: function (response)
    {
        if (response != null) {
            var RespObj = response;//JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                if (RespObj.IsValid == true) {
                    LoadPageContent("NearestOffice", 'NearestOffice.HandleNearestOfficeDocReady()', true, true);
                }
                else {
                    NearestOffice.NearestGPSSAOfficeResp = null;
                    localStorage.removeItem("GPSSAOffices" + Language);
                    var URL = Globals.ServicesURI_Test + "get/office/language/#Lang#";
                    URL = URL.replace("#Lang#", Language);
                    CallWebService(URL, 'NearestOffice.HandleGetOfficesCallSuccess(response)', "NearestOffice.HandleGetOfficesCallFailure()", "", "", true, false);
                }
            }
        }
    },

    HandleValidateOfficesCallFailure: function ()
    {
        var Resp = localStorage.getItem("GPSSAOffices" + Language);
        NearestOffice.NearestGPSSAOfficeResp = JSON.parse(Resp);
        LoadPageContent("NearestOffice", 'NearestOffice.HandleNearestOfficeDocReady()', false, true);
    },

    GetPositionSuccess: function (pos) {
        try {

            Globals.MyLongitude = pos.coords.longitude;
            Globals.MyLatitude = pos.coords.latitude;
            localStorage.setItem("LatLang", Globals.MyLatitude + "#" + Globals.MyLongitude);

            if (GPSSAOfficeResponseGotten == false) {
                if (localStorage.getItem("GPSSAOffices" + Language) == null || localStorage.getItem("GPSSAOffices" + Language) == undefined) {
                    var URL = Globals.ServicesURI_Test + "get/office/language/#Lang#";
                    URL = URL.replace("#Lang#", Language);

                    CallWebService(URL, 'NearestOffice.HandleGetOfficesCallSuccess(response)', "NearestOffice.HandleGetOfficesCallFailure()", "", "", true, false);
                }
                else {
                    var response = localStorage.getItem("GPSSAOffices" + Language);
                    NearestOffice.NearestGPSSAOfficeResp = JSON.parse(response);

                    NearestOffice.MostRecentOffice = Number(NearestOffice.NearestGPSSAOfficeResp.GPSSAOffices[0].Modified);

                    for (var i = 0; i < NearestOffice.NearestGPSSAOfficeResp.GPSSAOffices.length; i++) {
                        var Temp = Number(NearestOffice.NearestGPSSAOfficeResp.GPSSAOffices[i].Modified);

                        if (NearestOffice.MostRecentOffice > Temp) {
                            NearestOffice.MostRecentOffice = Temp;
                        }
                    }

                    var URL = Globals.ServicesURI_Test + "validate/list/module/gpssaoffices/lastupdate/#LastUpdate#/language/#Lang#";
                    URL = URL.replace("#LastUpdate#", NearestOffice.MostRecentOffice).replace("#Lang#", Language);
                    CallWebService(URL, "NearestOffice.HandleValidateOfficesCallSuccess(response)", "NearestOffice.HandleValidateOfficesCallFailure()", "", "", true, false, false);
                }

            } else {

                NearestOffice.NearestGPSSAOfficeResp = GPSSAOfficeResponse;
                LoadPageContent("NearestOffice", 'NearestOffice.HandleNearestOfficeDocReady()', true, true);
            }
            ShowLoadingSpinner()
        } catch (e) {
        }
    },

    calculateDistance: function (lat2, lon2) {
        try {

            var lat1 = Globals.MyLatitude;
            var lon1 = Globals.MyLongitude;

            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad(lat2 - lat1);  // deg2rad below
            var dLon = deg2rad(lon2 - lon1);
            var a =
              Math.sin(dLat / 2) * Math.sin(dLat / 2) +
              Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
              Math.sin(dLon / 2) * Math.sin(dLon / 2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var d = R * c; // Distance in km
            return d;
        }
        catch (error) {
        }
    }
};

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}